<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 29/04/13
 * Time: 12:09 PM
 *
 */

namespace Tests\Resources\Models;

/**
 * Class PeopleQuery
 *
 * A Test query class with find methods
 * @package Tests\Resources\Models
 */
class PeopleQuery
{
    protected $data;

    public function __construct()
    {
        $this->data = array();

        $this->data[] = new Person();
        $this->data[0]->setName('Carlos');
        $this->data[] = new Person();
        $this->data[1]->setName('Maria');
        $this->data[] = new Person();
        $this->data[2]->setName('Pedro');
        $this->data[] = new Person();
        $this->data[3]->setName('Maria');
    }

    public function find()
    {
        return $this->data;
    }

    public function findByName($name)
    {
        $res = array();
        foreach ($this->data as $p) {

            if ($name == $p->getName()) {
                $res[] = $p;
            }
        }

        return $res;
    }

    public function findOne()
    {
        return $this->data[0];
    }

    public function findOneByName($name)
    {
        $res = null;
        foreach ($this->data as $p) {

            if ($name == $p->getName()) {
                $res = $p;
                break;
            }
        }

        return $res;
    }

    public function findOneByBirthday($birthday)
    {
        $res = null;
        foreach ($this->data as $p) {

            if ($birthday == $p->getBirthday()) {
                $res = $p;
                break;
            }
        }

        return $res;
    }
}