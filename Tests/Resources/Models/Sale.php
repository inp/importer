<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 11/04/13
 * Time: 04:16 PM
 *
 */

namespace Tests\Resources\Models;


/**
 * Class Sale
 *
 * Test Sale model
 * @package Tests\Models\Resources
 */
class Sale
{
    protected $quantity;

    /**
     * @var Address
     */
    protected $address;

    /**
     * @param \Tests\Resources\Models\Address $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return \Tests\Resources\Models\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    public function getQuantity()
    {
        return $this->quantity;
    }

    public function toArray()
    {

        return [
            'quantity' => $this->quantity,
            'address' => $this->address != null ? $this->address->toArray() : []
        ];
    }
}