<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 11/04/13
 * Time: 04:19 PM
 *
 */

namespace Tests\Resources\Models;

/**
 * Class TelephoneCompany
 *
 * Test TelephoneCompany model.
 * @package Tests\Resources\Models
 */
class TelephoneCompany
{
    protected $nit;

    public function setNit($nit)
    {
        $this->nit = $nit;
    }

    public function getNit()
    {
        return $this->nit;
    }
}