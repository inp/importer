<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 11/04/13
 * Time: 04:15 PM
 *
 */

namespace Tests\Resources\Models;

use Importer\Util\ImportedDrawing;

/**
 * Class Person
 *
 * Test Person model
 * @package Tests\Resources\Models
 */
class Person
{
    protected $name;
    protected $birthday;


    /**
     * @var Address
     */
    protected $address;

    /**
     * @var Telephone
     */
    protected $telephone;

    /**
     * @var Sale
     */
    protected $sale;

    /**
     * @var string
     */
    protected $photo;

    /**
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param string $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    /**
     * @param ImportedDrawing $photo
     */
    public function setImportedPhoto(ImportedDrawing $photo)
    {
        $tempDir = sys_get_temp_dir();
        $photo->saveToPath($tempDir);
        $this->photo = $photo->getFilename();
    }

    /**
     * @param \Tests\Resources\Models\Sale $sale
     */
    public function setSale($sale)
    {
        $this->sale = $sale;
    }

    /**
     * @return \Tests\Resources\Models\Sale
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * @param \Tests\Resources\Models\Address $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return \Tests\Resources\Models\Address
     */
    public function getAddress()
    {
        return $this->address;
    }

    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @param \Tests\Resources\Models\Telephone $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return \Tests\Resources\Models\Telephone
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    public function toArray()
    {
        return [
            'name' => $this->name,
            'birthday' => $this->birthday,
            'photo' => $this->photo,
            'address' => $this->address != null ? $this->address->toArray() : [],
            'telephone' => $this->telephone != null ? $this->telephone->toArray() : [],
            'sale' => $this->sale != null ? $this->sale->toArray() : []
        ];
    }
}