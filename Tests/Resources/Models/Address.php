<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 11/04/13
 * Time: 04:17 PM
 *
 */

namespace Tests\Resources\Models;


/**
 * Class Address
 *
 * Test Address Model
 * @package Tests\Resources\Models
 */
class Address
{
    protected $address;
    protected $zip;
    protected $city;
    protected $state;

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setState($state)
    {
        $this->state = $state;
    }

    public function getState()
    {
        return $this->state;
    }

    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function toArray()
    {

        return [
            'address' => $this->address,
            'city' => $this->city,
            'state' => $this->state,
            'zip' => $this->zip
        ];
    }
}