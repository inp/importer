<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 22/04/13
 * Time: 05:25 PM
 *
 */

namespace Tests\Resources\Models;

use Tests\Resources\Models\Person;

class MultiSalePerson extends Person
{
    protected $sale = array();

    public function setSale($sale)
    {
        $this->sale = $sale;
    }

    public function getSale()
    {
        return $this->sale;
    }

}