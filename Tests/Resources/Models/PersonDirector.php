<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles
 * Date: 15/04/13
 * Time: 02:39 PM
 *
 */

namespace Tests\Resources\Models;

use Exception;
use Importer\Builder\GenericBuilder;
use Tests\Resources\Models\Address;
use Tests\Resources\Models\Person;
use Tests\Resources\Models\Sale;
use Tests\Resources\Models\Telephone;
use Tests\Resources\Models\TelephoneCompany;

/**
 * Class PersonDirector
 *
 * The Director class for Building Person objects.
 * @package Builder
 */
class PersonDirector
{
    /**
     * @var BuilderInterface The Builder used to create the Person objects.
     */
    protected $builder;

    public function __construct()
    {
        $this->builder = new GenericBuilder('Tests\Resources\Models\Person');
    }

    /**
     * Method to construct a person and assigning a list of data to its parts.
     * @param $values array An associative array mapping an attribute of the built object and its value.
     * @return Person A built Person object.
     * @throws \Exception
     */
    public function constructPerson($values)
    {

        try {
            // Person ready to be built.
            $this->builder->create();

            $this->builder->buildPart('address', new Address());
            $this->builder->buildPart('sale', new Sale())->buildPart('sale.address', new Address());
            $this->builder->buildPart('telephone', new Telephone())->buildPart(
                'telephone.company',
                new TelephoneCompany()
            );

            // Assigning values.
            foreach ($values as $path => $value) {

                $this->builder->buildPart($path, $value);
            }

            return $this->builder->get();

        } catch (Exception $e) {

            throw new Exception('Person object could not be built.', 0, $e);
        }
    }
}