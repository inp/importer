<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 19/04/13
 * Time: 09:21 AM
 *
 */

namespace Tests\Importer\Processor;


use Importer\Factory\ServiceContainerFactory;
use PHPExcel;
use Importer\Processor\PHPExcelWorksheetProcessor;
use PHPUnit\Framework\TestCase;

class PHPExcelWorksheetProcessorTest extends TestCase
{

    /**
     * @var PHPExcelWorksheetProcessor
     */
    public static $pewpt;

    /**
     * @var PHPExcel Test Object.
     */
    public static $peot;

    public static function setUpBeforeClass(): void
    {
        $container = ServiceContainerFactory::create(
            [
                'path' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
                    DIRECTORY_SEPARATOR . 'Yaml' . DIRECTORY_SEPARATOR,
                'file' => 'test_services.yml'
            ]
        );

        self::$peot = \PHPExcel_IOFactory::load(
            __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
            DIRECTORY_SEPARATOR . 'Spreadsheets' . DIRECTORY_SEPARATOR . 'Test1.xlsx'
        );

        self::$pewpt = new PHPExcelWorksheetProcessor($container->get(
            'processor_cell_is_erasable_predicate'
        ), $container->get('processor_header_finder'));
    }

    public function validTestRangesBeforeProvider()
    {
        return [
            [
                0,
                [
                    'highest' => [
                        'row' => 1,
                        'col' => 'A'
                    ],
                    'lowest' => [
                        'row' => 8,
                        'col' => 'P'
                    ]
                ]
            ]
        ];
    }

    public function validTestValuesWithoutEmptyCellsProvider()
    {
        return [
            [
                0,
                [
                    'A1' => 'Name',
                    'B1' => 'Birthday',
                    'C1' => 'Telephone',
                    'D1' => 'Telephone Company',
                    'E1' => 'Address',
                    'F1' => 'Zip',
                    'G1' => 'City',
                    'H1' => 'State',
                    'I1' => 'Photo',
                    'J1' => 'Sale',
                    'K1' => 'Sale Address',
                    'L1' => 'Sale Zip',
                    'M1' => 'Sale City',
                    'N1' => 'Sale State',
                    'A2' => 'Pedro',
                    'B2' => '1980-03-12',
                    'C2' => '3445467',
                    'D2' => 'ETB',
                    'E2' => 'cra 12 No 34 -56',
                    'F2' => '12341',
                    'G2' => 'Bogota',
                    'H2' => 'N/A',
                    'I2' => '',
                    'J2' => ' $  2,345,678.00 ',
                    'K2' => 'cra 21 No 34 -5',
                    'L2' => '12341',
                    'M2' => 'Bogota',
                    'N2' => 'N/A'
                ]
            ]
        ];
    }

    public function validTestDTODataProvider()
    {
        return [
            [
                0,
                [
                    'highest' => ['row' => 1, 'col' => 'A'],
                    'lowest' => ['row' => 6, 'col' => 'N']
                ],
                [
                    'Name',
                    'Birthday',
                    'Telephone',
                    'Telephone Company',
                    'Address',
                    'Zip',
                    'City',
                    'State',
                    'Photo',
                    'Sale',
                    'Sale Address',
                    'Sale Zip',
                    'Sale City',
                    'Sale State'
                ]
            ]
        ];
    }

    public function testPHPExcelWorksheetProcessorCreate()
    {
        $this->assertNotNull(self::$pewpt, 'The PHPExcelWorksheetProcessor service could not be created');
    }

    /**
     * Testing the findRange method before and after doing any deletion.
     * @dataProvider validTestRangesBeforeProvider
     */
    public function testPHPExcelWorksheetProcessorFindRangeBefore($sheetIdx, $testRange)
    {

        $worksheet = self::$peot->getSheet($sheetIdx);
        $range = self::$pewpt->findRange($worksheet);
        $this->assertEquals($testRange, $range, 'The found data range is incorrect.');
    }

    /**
     * Testing the removeEmptyCells method .
     * @dataProvider validTestValuesWithoutEmptyCellsProvider
     * @depends      testPHPExcelWorksheetProcessorFindRangeBefore
     */
    public function testPHPExcelWorksheetProcessorRemoveEmptyCells($sheetIdx, $testValues)
    {

        $worksheet = self::$peot->getSheet($sheetIdx);
        $range = self::$pewpt->findRange($worksheet);
        self::$pewpt->removeEmptyCells($worksheet, $range);

        foreach ($testValues as $key => $testValue) {

            $this->assertTrue($worksheet->cellExists($key), 'A data range cell got deleted: ' . $key);

            $this->assertEquals(
                $testValue,
                $worksheet->getCell($key)->getFormattedValue(),
                'The cell value is incorrect at .' . $key
            );
        }
    }

    /**
     * Testing the process to get a DTO from this Processor.
     * @param $sheetIdx
     * @param $testRange
     * @param $testHeaders
     * @dataProvider validTestDTODataProvider
     */
    public function testPHPExcelWorksheetProcessorProcess($sheetIdx, $testRange, $testHeaders)
    {
        $worksheet = self::$peot->getSheet($sheetIdx);
        $dto = self::$pewpt->process($worksheet);

        $this->assertEquals($worksheet, $dto->getSheet(), "The worksheets' content don't match.");
        $this->assertEquals($testRange, $dto->getDataRange(), 'The data ranges dont match.');
        $this->assertEquals($testHeaders, $dto->getHeaderIdentifiers(), 'The data ranges dont match.');
    }
}