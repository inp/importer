<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 6/05/13
 * Time: 03:51 PM
 *
 */

namespace Tests\Importer\Factory;


use Importer\Builder\ReferenceBuilder;
use Importer\Factory\InstanceTypeBuilderFactory;
use Importer\Finder\ClassMethodFinder;
use Importer\Mapper\BasicIdentifier;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyAccess\PropertyAccess;

class InstanceTypeBuilderFactoryTest extends TestCase
{

    public function validTestFactoryParametersProvider()
    {

        $identifier = new BasicIdentifier('column_builder');
        $identifier->setAttribute('builder', 'SoftReference');
        $identifier->setAttribute('finder', ['PeopleQuery', 'findByBirthday', ['$title$']]);


        // Fake identifier values
        $mockIdentifierMap = [

            ['builder', 'HardReference'],
            ['finder', ['PeopleQuery', 'findByName', ['$data$']]],
            [
                'column_builder',
                $identifier
            ],
        ];

        $mockHasAttributeMap = [

            ['column_builder', true],
        ];


        $mockIdentifier = $this->getMockBuilder('Importer\Mapper\BasicIdentifier')->disableOriginalConstructor()->setMethods(
            ['getAttribute', 'hasAttribute']
        )->getMock();

        $mockIdentifier->expects($this->any())->method('getAttribute')->with(
            $this->anything()
        )->will($this->returnValueMap($mockIdentifierMap));

        $mockIdentifier->expects($this->any())->method('hasAttribute')->with(
            $this->anything()
        )->will($this->returnValueMap($mockHasAttributeMap));

        $mockContainer = $this->getMockBuilder(
            'Symfony\Component\DependencyInjection\ContainerBuilder'
        )->disableOriginalConstructor()
            ->getMock();

        $accessor = PropertyAccess::createPropertyAccessor();

        $expectedBuilder = [
            new ReferenceBuilder()
        ];

        $finder = new ClassMethodFinder($mockIdentifierMap[1][1][0], $mockIdentifierMap[1][1][1], false);
        $finder->setContainer($mockContainer);
        $expectedBuilder[0]->setType($mockIdentifierMap[0][1]);
        $expectedBuilder[0]->setFinder($finder);

        $columnBuilder = new ReferenceBuilder();
        $finder = new ClassMethodFinder('PeopleQuery', 'findByBirthday', false);
        $finder->setContainer($mockContainer);
        $columnBuilder->setType('SoftReference');
        $columnBuilder->setFinder($finder);
        $columnBuilder->setContainer($mockContainer);
        $columnBuilder->setDataAccessor($accessor);
        $expectedBuilder[0]->setColumnBuilder($columnBuilder);
        $expectedBuilder[0]->setContainer($mockContainer);
        $expectedBuilder[0]->setDataAccessor($accessor);

        return [
            [
                [
                    'id' => $mockIdentifier,
                    'container' => $mockContainer,
                    'accessor' => $accessor
                ],
                $expectedBuilder[0]
            ],
        ];
    }

    /**
     * Testing if the InstanceTypeBuilderFactory's create method creates the expected Builder
     * @param $params
     * @param $expected
     * @dataProvider validTestFactoryParametersProvider
     */
    public function testInstanceTypeBuilderFactoryCreate($params, $expectedBuilder)
    {

        $fact = new InstanceTypeBuilderFactory();
        $builder = $fact->create($params);
        $this->assertEquals($expectedBuilder, $builder, 'The builder is not equal to the expected type.');
    }

}