<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 9/04/13
 * Time: 11:11 AM
 *
 */

namespace Tests\Importer\Factory;

use InvalidArgumentException;
use Importer\Factory\ServiceContainerFactory;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

class ServiceContainerFactoryTest extends TestCase
{

    public function validTestCaseProvider()
    {

        return array(array('excel_importer', 'Importer\Importer\ExcelImporter'));
    }

    /**
     * Testing if the factory's container instance creates the right Importer instances.
     * @param $importerName string The name of the desired Importer.
     * @param $importerClassType string The name of the expected class for the instances of the Importer.
     * @dataProvider validTestCaseProvider
     */
    public function testServiceContainerFactoryCreate($importerName, $importerClassType)
    {
        $container = ServiceContainerFactory::create(
            array(
                'path' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
                    DIRECTORY_SEPARATOR . 'Yaml',
                'file' => 'test_services.yml'
            )
        );

        $container->compile();

        $this->assertNotNull($container, 'Factory could not load Container object.');
        $importer = $container->get($importerName);
        $this->assertNotNull($importer, 'Factory could not load ExcelImporter object.');
        $this->assertEquals($importerClassType, get_class($importer), 'The instance is not valid.');

    }

    /**
     * Testing if ServiceContainerFactory create() method throws an exception.
     * @depends testServiceContainerFactoryCreate
     */
    public function testServiceContainerFactoryCreateFail()
    {

        try {

            $container = ServiceContainerFactory::create();
            $importer = $container->get('yaml_importer');

        } catch (ServiceNotFoundException $expected) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('An Exception was expected.');
    }

}