<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian robles Jimenez
 * Date: 26/04/13
 * Time: 03:55 PM
 *
 */

namespace Tests\Importer\Formatter;

use Importer\Formatter\DateFormatter;
use PHPUnit\Framework\TestCase;

class DateFormatterTest extends TestCase
{
    /**
     * @var DateFormatter
     */
    public static $dft;

    public static function setUpBeforeClass(): void
    {
        self::$dft = new DateFormatter();
    }

    public function validDateProvider()
    {

        return [
            ['1980-09-23', '23/09/1980'],
            ['2000-12-30', '30/12/2000'],
            ['1994-10-01', '01/10/1994'],
        ];
    }

    /**
     * Testing the format method with a set of input.
     * @dataProvider validDateProvider
     */
    public function testDateFormatterFormat($inputDate, $expectedDate)
    {
        $formattedDate = self::$dft->format($inputDate);
        $this->assertEquals($expectedDate, $formattedDate, 'The date was not formatted correctly.');
    }
}