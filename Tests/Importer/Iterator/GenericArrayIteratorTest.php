<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 16/04/13
 * Time: 05:02 PM
 *
 */

namespace Tests\Importer\Iterator;

use Importer\Iterator\GenericArrayIterator;
use PHPUnit\Framework\TestCase;

class GenericArrayIteratorTest extends TestCase
{
    public function validPropertyListProvider()
    {
        return [
            [
                ['a', 1, 123234343, true, null, null, 87, -45456456.5656, "dsdasd$$$", null, false]
            ],
            [
                range(-100, 1000)
            ]
        ];
    }

    /**
     * Testing the list traversing.
     * @dataProvider validPropertyListProvider
     * @param $list
     */
    public function testIndexedPropertyIteratorTraversing($list)
    {
        $it = new GenericArrayIterator($list);

        while ($it->valid()) {

            $this->assertEquals($list[$it->key()], $it->current(), "The lists's values don't match.");
            $it->next();
        }
    }
}