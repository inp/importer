<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 8/04/13
 * Time: 12:41 PM
 *
 */

namespace Tests\Importer\Util;

use PHPUnit\Framework\TestCase;
use Importer\Util\ExcelImporterUtil;

class ExcelImporterUtilTest extends TestCase
{

    public function validTestCellsProvider()
    {
        // Test cells
        return [
            ['A', '1'],
            ['ABC', '123'],
            ['Z', '234234343454354'],
            ['T', '234'],
            ['WREDDRRDRWRDTWWRDW', '343434545340005453']
        ];
    }

    public function validTestRangesProvider()
    {
        // Test ranges
        return [
            ['B', '2', 'BASASSDDW', '200'],
            ['A', '27678', 'FFFABC', '200000000000'],
            ['ZASDEF', '1', 'ZZZZBBBASA', '2'],
            ['A', '1', 'B', '2']
        ];
    }

    public function validCellRangeProvider()
    {
        //Test cells and ranges names
        return [
            ['A1', 'A1:A1'],
            ['H14', 'H1:H23456'],
            ['F14', 'B1:F14'],
            ['B3', 'B2:T200'],
        ];
    }

    /**
     * Method for testing stringToCell's performance.
     * @dataProvider validTestCellsProvider
     */
    public function testExcelImporterUtilStringToCell($testCol, $testRow)
    {

        $cell = ExcelImporterUtil::stringToCell($testCol . $testRow);
        $this->assertArrayHasKey('row', $cell, "The cell array has not the key 'row'");
        $this->assertArrayHasKey('col', $cell, "The cell array has not the key 'col'");
        $this->assertEquals($testCol, $cell['col'], "The 'col' field is not correct.");
        $this->assertEquals($testRow, $cell['row'], "The 'row' field is not correct.");
    }

    /**
     * Method for testing stringToRange's performance.
     * @dataProvider validTestRangesProvider
     * @depends      testExcelImporterUtilStringToCell
     */
    public function testExcelImporterUtilStringToRange($highestCol, $highestRow, $lowestCol, $lowestRow)
    {
        $separator = ':';


        $testRange = $highestCol . $highestRow . $separator . $lowestCol . $lowestRow;
        $range = ExcelImporterUtil::stringToRange($testRange, $separator);

        $this->assertArrayHasKey('highest', $range, "The range array has not the key 'highest'");
        $this->assertArrayHasKey('lowest', $range, "The range array has not the key 'lowest'");
        $this->assertArrayHasKey('row', $range['highest'], "The range array has not the key 'highest' -> 'row'");
        $this->assertArrayHasKey('col', $range['highest'], "The range array has not the key 'highest' -> 'col'");
        $this->assertArrayHasKey('row', $range['lowest'], "The range array has not the key 'lowest' -> 'row'");
        $this->assertArrayHasKey('col', $range['lowest'], "The range array has not the key 'lowest' -> 'col'");

        $testCells['highest'] = ExcelImporterUtil::stringToCell($highestCol . $highestRow);
        $testCells['lowest'] = ExcelImporterUtil::stringToCell($lowestCol . $lowestRow);

        $this->assertEquals(
            $testCells['highest']['col'],
            $range['highest']['col'],
            "The 'highest' -> 'col' field is not correct."
        );
        $this->assertEquals(
            $testCells['highest']['row'],
            $range['highest']['row'],
            "The 'highest' -> 'row' field is not correct."
        );
        $this->assertEquals(
            $testCells['lowest']['col'],
            $range['lowest']['col'],
            "The 'lowest' -> 'col' field is not correct."
        );
        $this->assertEquals(
            $testCells['lowest']['col'],
            $range['lowest']['col'],
            "The 'lowest' -> 'col' field is not correct."
        );
    }
}