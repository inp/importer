<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 8/04/13
 * Time: 10:05 AM
 *
 */

namespace Tests\Importer\Predicate;

use Importer\Predicate\CellFormatIsValidPredicate;
use PHPUnit\Framework\TestCase;

class CellFormatIsValidPredicateTest extends TestCase
{

    /**
     * @var CellFormatIsValidPredicate Test object for the CellFormatIsValidPredicate
     */
    protected static $cellFormatIsValidTestObj = null;

    public static function setUpBeforeClass(): void
    {
        self::$cellFormatIsValidTestObj = new CellFormatIsValidPredicate();
    }

    /**
     * @return array A set of valid test subjects
     */
    public function validTestSubjectsProvider()
    {

        return [
            ['A1'],
            ['B23'],
            ['ZASDEFG30000004000'],
            ['BKBAAAAAA9']
        ];
    }

    /**
     * @return array A set of invalid test subjects
     */
    public function invalidTestSubjectsProvider()
    {

        return [
            ['A'],
            ['2345545454'],
            ['0.0000006'],
            ['ZASDEFG03000004000'],
            ['8AS8GGHcy%&%$%&'],
            ['23ASDADD45'],
            ['asd34']
        ];
    }

    /**
     * This function tests the object could be created successfully.
     */
    public function testCellFormatIsValidPredicateCreation()
    {
        $this->assertNotNull(self::$cellFormatIsValidTestObj, 'Could not create CellIsValidPredicate object.');
    }

    /**
     * Testing CellFormatIsValidPredicate performance.
     * @dataProvider validTestSubjectsProvider
     */
    public function testCellFormatIsValidPredicate($validSubject)
    {

        $this->assertTrue(
            self::$cellFormatIsValidTestObj->evaluate($validSubject),
            'Incorrect predicate evaluation with test
                       value: ' . $validSubject
        );
    }

    /**
     * Testing if CellFormatIsValidPredicate evaulates to false on invalid test cases.
     * @dataProvider invalidTestSubjectsProvider
     */
    public function testCellFormatIsValidPredicateFail($invalidSubject)
    {
        $this->assertFalse(
            self::$cellFormatIsValidTestObj->evaluate($invalidSubject),
            'Incorrect predicate evaluation with test
                       value: ' . $invalidSubject
        );

    }
}