<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 8/04/13
 * Time: 04:02 PM
 *
 */

namespace Tests\Importer\Predicate;


use PHPExcel_Cell;
use Importer\Predicate\CellIsEmptyPredicate;
use PHPUnit\Framework\TestCase;

class CellIsErasablePredicateTest extends TestCase
{
    /**
     * @var CellIsEmptyPredicate
     */
    protected static $ciep;

    public static function setUpBeforeClass(): void
    {
        self::$ciep = new CellIsEmptyPredicate();
    }


    /**
     * Testing the CellIsEmptyPredicate object creation
     * @return CellIsEmptyPredicate
     */
    public function testCellIsEmptyPredicateCreation()
    {
        $this->assertNotNull(self::$ciep, 'Could not create CellIsEmptyPredicate object.');
    }

    /**
     * @depends testCellIsEmptyPredicateCreation
     */
    public function validEmptyCellsProvider()
    {

        return [
            [new PHPExcel_Cell("", null, new \PHPExcel_Worksheet())],
            [new PHPExcel_Cell(null, null, new \PHPExcel_Worksheet())],
            [new PHPExcel_Cell('      ' . "\t" . '    ', null, new \PHPExcel_Worksheet())],
            [new PHPExcel_Cell('     ' . "\t" . '  ' . "\t", null, new \PHPExcel_Worksheet())]
        ];
    }


    /**
     * Testing the CellIsEmptyPredicate
     * @dataProvider validEmptyCellsProvider
     */
    public function testCellIsEmptyPredicate(PHPExcel_Cell $cell)
    {

        $this->assertTrue(
            self::$ciep->evaluate($cell),
            'Incorrect predicate evaluation with test
                                  value: ' . addcslashes($cell->getValue(), self::$ciep->getBlankChars())
        );
    }
}