<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 5/04/13
 * Time: 12:08 PM
 *
 */

namespace Tests\Importer\Predicate;

use Exception;
use Importer\Predicate\FileExistsPredicate;
use Importer\Predicate\FileIsReadablePredicate;
use Importer\Predicate\FileIsValidPredicate;
use PHPUnit\Framework\TestCase;
use PHPUnit_Framework_TestCase;

class FileIsValidPredicateTest extends TestCase
{

    /**
     * Testing the FileExistsPredicate object creation
     * @return FileExistsPredicate
     */
    public function testFileExistsPredicateCreation()
    {

        $tfep = new FileExistsPredicate();
        $this->assertNotNull($tfep, 'Could not create FileExistsPredicate object.');

        return $tfep;
    }

    /**
     * Testing the FileIsReadablePredicate object creation
     * @return FileIsReadablePredicate
     */
    public function testFileIsReadablePredicateCreation()
    {

        $tfirp = new FileIsReadablePredicate();
        $this->assertNotNull($tfirp, 'Could not create FileIsReadablePredicate object.');

        return $tfirp;
    }

    /**
     * Testing the FileIsValidPredicate object creation
     * @return FileIsValidPredicate
     */
    public function testFileIsValidPredicateCreation()
    {

        $tfivp = new FileIsValidPredicate();
        $this->assertNotNull($tfivp, 'Could not create FileIsValidPredicate object.');

        return $tfivp;
    }

    /**
     * Testing if the FileExistsPredicate throws an Exception
     * @param FileExistsPredicate $tfep
     * @depends testFileExistsPredicateCreation
     *
     */
    public function testFileExistsPredicateFail(FileExistsPredicate $tfep)
    {

        $this->expectExceptionMessage('File doesn\'t exist');

        $tfep->evaluate('../Resources/TestTrolololololo.xlsx');
    }

    /**
     * Testing the FileExistsPredicate performance
     * @depends testFileExistsPredicateCreation
     */
    public function testFileExistsPredicate(FileExistsPredicate $tfep)
    {

        $this->assertTrue(
            $tfep->evaluate('C:\xampp\htdocs\Importer\Tests\Resources\Spreadsheets\Test1.xlsx'),
            'The FileExistsPredicate is incorrect.'
        );
    }

    /**
     * Testing the FileIsReadablePredicate performance
     * @depends testFileIsReadablePredicateCreation
     */
    public function testFileIsReadablePredicate(FileIsReadablePredicate $tfirp)
    {

        $this->assertTrue(
            $tfirp->evaluate('C:\xampp\htdocs\Importer\Tests\Resources\Spreadsheets\Test1.xlsx'),
            'The FileIsReadablePredicate is incorrect.'
        );
    }

    /**
     * Testing if the FileIsValidPredicate throws an Exception
     * @expectedException Exception
     * @depends testFileIsValidPredicateCreation
     */
    public function testFileIsValidPredicateFail(FileIsValidPredicate $tfivp)
    {

        $tfivp->evaluate('C:\xampp\htdocs\Importer\Tests\Resources\Spreadsheets\Test1.txt');
    }

    /**
     * Testing the FileIsValidPredicate performance
     * @depends testFileIsValidPredicateCreation
     */
    public function testFileIsValidPredicate(FileIsValidPredicate $tfivp)
    {

        $this->assertTrue(
            $tfivp->evaluate('C:\xampp\htdocs\Importer\Tests\Resources\Spreadsheets\Test1.xlsx'),
            'The FileIsValidPredicate is incorrect.'
        );
    }
}
