<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 11/04/13
 * Time: 10:00 AM
 *
 */

namespace Tests\Importer\Finder;


use Importer\Finder\WorksheetHeaderFinder;
use PHPUnit\Framework\TestCase;

class WorksheetHeaderFinderTest extends TestCase
{
    public static $whtf;
    public static $testWorkSheets;

    public static function setUpBeforeClass(): void
    {
        self::$whtf = new WorksheetHeaderFinder();
        self::$testWorkSheets = [new \PHPExcel_Worksheet()];

        self::$testWorkSheets[0]->getCell('A1')->setValue('Nombres');
        self::$testWorkSheets[0]->getCell('B1')->setValue('Apellidos');
        self::$testWorkSheets[0]->getCell('C1')->setValue('Telefono');
        self::$testWorkSheets[0]->getCell('D1')->setValue('Celular');
        self::$testWorkSheets[0]->getCell('E1')->setValue('Direccion');
        self::$testWorkSheets[0]->getCell('F1')->setValue('Cedula');
    }

    public function validTestHeaderRowsProvider()
    {
        return [
            [
                0,
                [
                    'Nombres',
                    'Apellidos',
                    'Telefono',
                    'Celular',
                    'Direccion',
                    'Cedula'
                ]
            ]
        ];
    }

    /**
     * Tests WorkSheetHeaderFinder creation
     */
    public function testWorksheetHeaderFinderCreate()
    {
        $this->assertNotNull(self::$whtf, 'Could not create WorkSheetHeaderFinder object.');
    }

    /**
     * Testing the performance of the findElements method.
     * @depends      testWorksheetHeaderFinderCreate
     * @dataProvider validTestHeaderRowsProvider
     * @param $worksheetIdx int The index of the desired test worksheet,
     * @param $rowHeader array The content of the test header row.
     */
    public function testWorksheetHeaderFinderFindElements($worksheetIdx, $rowHeader)
    {
        $this->assertEquals(
            self::$whtf->findElements(self::$testWorkSheets[$worksheetIdx]),
            $rowHeader,
            'The findElements method has failed extracting the header data.'
        );
    }
}