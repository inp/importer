<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 29/04/13
 * Time: 10:14 AM
 *
 */

namespace Tests\Importer\Finder;

use Importer\Finder\ClassMethodFinder;
use PHPUnit\Framework\TestCase;
use Tests\Resources\Models\PeopleQuery;

define('DS', DIRECTORY_SEPARATOR);

require_once(__DIR__ . DS . '..' . DS . '..' . DS . 'Resources' . DS . 'Models' . DS . 'Person.php');
require_once(__DIR__ . DS . '..' . DS . '..' . DS . 'Resources' . DS . 'Models' . DS . 'PeopleQuery.php');

class ClassMethodFinderTest extends TestCase
{

    public function validClassMethodResultsProvider()
    {

        $testClasses = ['Tests\Resources\Models\PeopleQuery'];
        $testMethods = [['find', 'findOneByName', 'findOne', 'findByName']];
        $testArgs = [[[null], ['Pedro'], [null], ['Maria']]];
        $testObjects = [new PeopleQuery()];
        $testResults = [
            [
                $testObjects[0]->find(),
                [$testObjects[0]->findOneByName('Pedro')],
                [$testObjects[0]->findOne()],
                $testObjects[0]->findByName('Maria')
            ],
        ];

        $t = min(count($testClasses), count($testMethods), count($testArgs), count($testResults));
        $testCases = [];
        for ($i = 0; $i < $t; ++$i) {

            $testCases[] = [$testClasses[$i], $testMethods[$i], $testArgs[$i], $testResults[$i]];
        }

        return $testCases;
    }

    /**
     * Testing the findElements Method
     * @dataProvider validClassMethodResultsProvider
     */
    public function testClassMethodFinderFindElements($testClassName, $testMethods, $testArgs, $testResult)
    {

        $t = count($testMethods);
        for ($i = 0; $i < $t; ++$i) {

            $cmft = new ClassMethodFinder($testClassName, $testMethods[$i], false);
            $p = $cmft->findElements($testArgs[$i]);

            $this->assertEquals($testResult[$i], $p, 'The findElements method did not find the expected objects.');
        }
    }
}