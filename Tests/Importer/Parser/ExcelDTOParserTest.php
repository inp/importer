<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 19/04/13
 * Time: 01:53 PM
 *
 */

namespace Tests\Importer\Parser;

require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Person.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Address.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Sale.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Telephone.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'TelephoneCompany.php');

use Importer\Factory\ServiceContainerFactory;
use Importer\Parser\ExcelDTOParser;
use PHPUnit\Framework\TestCase;

class ExcelDTOParserTest extends TestCase
{

    /**
     * @var ExcelDTOParser
     */
    public static $edpt;

    public static function setUpBeforeClass(): void
    {
        $container = ServiceContainerFactory::create(
            [
                'path' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
                    DIRECTORY_SEPARATOR . 'Yaml',
                'file' => 'test_services.yml'
            ]
        );

        $mapper = $container->get('mapper');
        $director = $container->get('director');
        $director->setContainer($container);

        self::$edpt = new ExcelDTOParser($mapper, $director);
    }

    public function testExcelDTOParserTestCreate()
    {
        $this->assertNotNull(self::$edpt, 'The  ExcelDTOParser object could not be created.');
    }

}