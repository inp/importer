<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 17/07/13
 * Time: 04:15 PM
 *
 */

namespace Tests\Importer\Exception;

use Importer\Exception\TranslatedImporterException;
use PHPUnit\Framework\TestCase;

/**
 * Class TranslatedImporterExceptionTest
 *
 * Testing the "Newly" created TranslatedImporterException, and (maybe )its subclasses.
 *
 * @package Tests\Importer\Exception
 */
class TranslatedImporterExceptionTest extends TestCase
{

    public static function setUpBeforeClass(): void
    {

        TranslatedImporterException::setUpTranslator('es');

        TranslatedImporterException::addResource(
            'xlf',
            __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
            DIRECTORY_SEPARATOR . 'Catalogue' . DIRECTORY_SEPARATOR . 'messages.xml',
            'es'
        );
    }

    public function translatedImporterExceptionGetTranslatedMessageProvider()
    {

        return [

            [
                'Importer\Exception\TranslatedImporterException',
                'Error loading the file.',
                'Error al tratar de cargar el archivo.'
            ],
            [
                'Importer\Builder\Exception\BuilderCreationException',
                'Error while trying to create an object.',
                'Ocurri&oacute; un error mientras se creaba el objeto.'
            ],
            [
                'Importer\Formatter\Exception\FormatterNewFormatCouldNotBeAppliedException',
                'The current format could not be applied : %format%',
                'No se pudo aplicar el formato : DateFormat',
                ['%format%' => 'DateFormat']
            ],
            [
                'Importer\Builder\Exception\BuilderProductPartCouldNotBeBuiltException',
                'Error trying to build the part : %part% => %value%.',
                'Error al tratar de construir la parte del producto : attr1 => 3.1459.',
                ['%part%' => 'attr1', '%value%' => '3.1459']
            ]
        ];
    }

    /**
     * Testing the success of the getTranslatedMessage method.
     *
     * @dataProvider translatedImporterExceptionGetTranslatedMessageProvider
     * @throws \Importer\Exception\TranslatedImporterException
     */
    public function testTranslatedImporterExceptionGetTranslatedMessageSuccess($className, $given, $expected, $placeHolders = null)
    {
        try {

            throw new $className($given, 0, null, null, $placeHolders);
        } catch (TranslatedImporterException $tie) {

            $this->assertEquals(
                $expected,
                $tie->getTranslatedMessage(),
                'The translated message is incorrect.'
            );
            return;
        }

        $this->fail('The TranslatedImporterException could not be thrown or captured.');
    }

}