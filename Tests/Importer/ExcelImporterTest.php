<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 5/04/13
 * Time: 12:08 PM
 *
 */

namespace Tests\Importer;

    require_once __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .'..'. DIRECTORY_SEPARATOR . 'vendor/autoload.php';
    require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
        DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Person.php');
    require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
        DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Address.php');
    require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
        DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Sale.php');
    require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
        DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Telephone.php');
    require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
        DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'TelephoneCompany.php');

    use Importer\Factory\ServiceContainerFactory;
    use Importer\Importer\ExcelImporter;
    use PHPUnit_Framework_TestCase;
    use Exception;
    use Tests\Resources\Models\Address;
    use Tests\Resources\Models\Person;
    use Tests\Resources\Models\Sale;
    use Tests\Resources\Models\Telephone;
    use Tests\Resources\Models\TelephoneCompany;

    class ExcelImporterTest extends PHPUnit_Framework_TestCase
    {
        /**
         * @var ExcelImporter The ExcelImporter service.
         */
        public static $importerService;

        public static function setUpBeforeClass()
        {

            $container = ServiceContainerFactory::create(
                array(
                    'path' => __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
                        DIRECTORY_SEPARATOR . 'Yaml',
                    'file' => 'test_services.yml'
                )
            );

            $container->get('director')->setContainer($container);

            self::$importerService = $container->get('excel_importer');
        }

        public function validTestModelsProvider()
        {

            $person = array();
            $telephone = array();
            $telephoneCompany = array();
            $address = array();
            $saleAddress = array();
            $sale = array();
            $person[0] = new Person();
            $telephone[0] = new Telephone();
            $telephoneCompany[0] = new TelephoneCompany();
            $address[0] = new Address();
            $saleAddress[0] = new Address();
            $sale[0] = new Sale();

            $saleAddress[0]->setAddress('cra 21 No 34 -5');
            $saleAddress[0]->setZip('12341');
            $saleAddress[0]->setCity('Bogota');
            $saleAddress[0]->setState('N/A');

            $address[0]->setAddress('cra 12 No 34 -56');
            $address[0]->setCity('Bogota');
            $address[0]->setState('N/A');
            $address[0]->setZip('12341');

            $sale[0]->setAddress($saleAddress[0]);
            $sale[0]->setQuantity('$  2345678.00');

            $telephoneCompany[0]->setNit('ETB');

            $telephone[0]->setNumber('3445467');
            $telephone[0]->setCompany($telephoneCompany[0]);

            $person[0]->setName('Pedro');
            $person[0]->setBirthday('12/03/1980');
            $person[0]->setTelephone($telephone[0]);
            $person[0]->setAddress($address[0]);
            $person[0]->setSale($sale[0]);
            $person[0]->setPhoto('image11.jpeg');

            ///////////////////////////////////

            $person[1] = new Person();
            $telephone[1] = new Telephone();
            $telephoneCompany[1] = new TelephoneCompany();
            $address[1] = new Address();
            $saleAddress[1] = new Address();
            $sale[1] = new Sale();

            $saleAddress[1]->setAddress('cra 30 No 14 -3');
            $saleAddress[1]->setZip('12342');
            $saleAddress[1]->setCity('Bogota');
            $saleAddress[1]->setState('N/A');

            $address[1]->setAddress('cra 23 No 40 -34');
            $address[1]->setCity('Bogota');
            $address[1]->setState('N/A');
            $address[1]->setZip('12342');

            $sale[1]->setAddress($saleAddress[1]);
            $sale[1]->setQuantity('$  1546765.00');

            $telephoneCompany[1]->setNit('Claro');

            $telephone[1]->setNumber('2343456');
            $telephone[1]->setCompany($telephoneCompany[1]);

            $person[1]->setName('Karen');
            $person[1]->setBirthday('20/08/1990');
            $person[1]->setTelephone($telephone[1]);
            $person[1]->setAddress($address[1]);
            $person[1]->setSale($sale[1]);

            ///////////////////////////////////

            $person[2] = new Person();
            $telephone[2] = new Telephone();
            $telephoneCompany[2] = new TelephoneCompany();
            $address[2] = new Address();
            $saleAddress[2] = new Address();
            $sale[2] = new Sale();

            $saleAddress[2]->setAddress('cll 12 No 3 - 7');
            $saleAddress[2]->setZip('12343');
            $saleAddress[2]->setCity('Bogota');
            $saleAddress[2]->setState('N/A');

            $address[2]->setAddress('cll 123 No 34 - 70');
            $address[2]->setCity('Bogota');
            $address[2]->setState('N/A');
            $address[2]->setZip('12343');

            $sale[2]->setAddress($saleAddress[2]);
            $sale[2]->setQuantity('$  547651.00');

            $telephoneCompany[2]->setNit('Claro');

            $telephone[2]->setNumber('5676789');
            $telephone[2]->setCompany($telephoneCompany[2]);

            $person[2]->setName('Luis');
            $person[2]->setBirthday('03/12/1987');
            $person[2]->setTelephone($telephone[2]);
            $person[2]->setAddress($address[2]);
            $person[2]->setSale($sale[2]);
            $person[2]->setPhoto('image22.gif');

            ///////////////////////////////////

            $person[3] = new Person();
            $telephone[3] = new Telephone();
            $telephoneCompany[3] = new TelephoneCompany();
            $address[3] = new Address();
            $saleAddress[3] = new Address();
            $sale[3] = new Sale();

            $saleAddress[3]->setAddress('cll 76 No 15 - 14');
            $saleAddress[3]->setZip('12344');
            $saleAddress[3]->setCity('Bogota');
            $saleAddress[3]->setState('N/A');

            $address[3]->setAddress('cll 76 No 5 - 04');
            $address[3]->setCity('Bogota');
            $address[3]->setState('N/A');
            $address[3]->setZip('12344');

            $sale[3]->setAddress($saleAddress[3]);
            $sale[3]->setQuantity('$  34543.00');

            $telephoneCompany[3]->setNit('ETB');

            $telephone[3]->setNumber('7564012');
            $telephone[3]->setCompany($telephoneCompany[3]);

            $person[3]->setName('Miguel');
            $person[3]->setBirthday('30/01/1987');
            $person[3]->setTelephone($telephone[3]);
            $person[3]->setAddress($address[3]);
            $person[3]->setSale($sale[3]);

            ///////////////////////////////////

            $person[4] = new Person();
            $telephone[4] = new Telephone();
            $telephoneCompany[4] = new TelephoneCompany();
            $address[4] = new Address();
            $saleAddress[4] = new Address();
            $sale[4] = new Sale();

            $saleAddress[4]->setAddress('cra 6 No 28 - 13');
            $saleAddress[4]->setZip('12345');
            $saleAddress[4]->setCity('Bogota');
            $saleAddress[4]->setState('N/A');

            $address[4]->setAddress('cra 63 No 80 - 15');
            $address[4]->setCity('Bogota');
            $address[4]->setState('N/A');
            $address[4]->setZip('12345');

            $sale[4]->setAddress($saleAddress[4]);
            $sale[4]->setQuantity('$  2345678.00');

            $telephoneCompany[4]->setNit('ETB');

            $telephone[4]->setNumber('4556712');
            $telephone[4]->setCompany($telephoneCompany[4]);

            $person[4]->setName('Angela');
            $person[4]->setBirthday('06/04/1980');
            $person[4]->setTelephone($telephone[4]);
            $person[4]->setAddress($address[4]);
            $person[4]->setSale($sale[4]);

            return array(array($person));
        }

        /**
         * Testing the ExcelImporter service generated by .
         */
        public function testExcelImporterServiceCreate()
        {

            $this->assertNotNull(self::$importerService, 'The ExcelImporter service could not be created.');
        }

        /**
         * Testing if the ExcelImporter read() method throws an Exception
         * @depends testExcelImporterServiceCreate
         * @expectedException Exception
         * @expectedExceptionMessage Error loading the file.
         */
        public function testExcelImporterServiceReadFail()
        {
            self::$importerService->read(
                __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
                    DIRECTORY_SEPARATOR . 'Spreadsheets' . DIRECTORY_SEPARATOR . 'Test33.xlsx'
            );
        }

        /**
         * Testing the ExcelImporter read method.
         * @depends testExcelImporterServiceCreate
         */
        public function testExcelImporterServiceRead()
        {

            self::$importerService->read(
                __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
                    DIRECTORY_SEPARATOR . 'Spreadsheets' . DIRECTORY_SEPARATOR . 'Test1.xlsx'
            );

            $this->assertNotNull(
                self::$importerService->getPHPExcelObject(),
                'The PHPExcel object could not be created'
            );
            $this->assertNotNull(self::$importerService->getPHPExcelObject()->getSheetByName('Test1'));
            $this->assertNull(self::$importerService->getPHPExcelObject()->getSheetByName('Hoja100000'));
        }

        /**
         * Testing a full read, process, parse sequence.
         * @depends testExcelImporterServiceRead
         * @dataProvider validTestModelsProvider
         */
        public function testExcelImporterAll($expectedModel)
        {
            self::$importerService->process();
            self::$importerService->parse();
            $model = self::$importerService->getModel();

            $t = count($expectedModel);

            for ($i = 0; $i < $t; ++$i) {
                $this->assertEquals($expectedModel[$i], $model[0][$i]['person'], 'The expected and actual model are not equal.');

            }

        }
    }