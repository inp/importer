<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 2/05/13
 * Time: 12:45 PM
 *
 */

namespace Tests\Importer\Builder;

require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Person.php');

use Importer\Builder\ReferenceBuilder;
use Exception;
use Importer\Formatter\DateFormatter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Tests\Resources\Models\Person;

class ReferenceBuilderTest extends TestCase
{
    /**
     * @var ReferenceBuilder
     */
    public $rbt;

    public function setUp(): void
    {

        // People
        $people = [];
        $people[0] = new Person();
        $people[0]->setName('Carla');

        $people[1] = new Person();
        $people[1]->setName('Gabriel');

        // Formatter
        $formatter = [];
        $formatter[0] = new DateFormatter();

        // Fake Finder object
        $mockFinder = $this->getMockBuilder('Importer\Finder\ClassMethodFinder')->disableOriginalConstructor()->setMethods(
            ['findElements']
        )->getMock();


        // Fake Found objects
        $mockFoundObjectsMap = [
            [['Carla'], [$people[0]]],
            [['Pepe'], []],
            [['Mickey'], []],
        ];

        $mockFinder->expects($this->any())->method('findElements')->with(
            $this->anything()
        )->will($this->returnValueMap($mockFoundObjectsMap));

        $mockContainer = new ContainerBuilder();
        $mockContainer->set('person', $people[1]);
        $mockContainer->set('date_formatter', $formatter[0]);

        $this->rbt = new ReferenceBuilder();
        $this->rbt->setContainer($mockContainer);
        $this->rbt->setFinder($mockFinder);
        $this->rbt->setDataAccessor(PropertyAccess::createPropertyAccessor());

    }

    public function testReferenceBuilderCreation()
    {
        $this->assertNotNull($this->rbt, 'The ReferenceBuilder object could not be created.');
    }

    public function validTestParamsAndObjectsProvider()
    {
        // People
        $people = [];
        $people[0] = new Person();
        $people[0]->setName('Carla');
        $people[1] = new Person();
        $people[1]->setName('Gabriel');

        // Fake Identifier objects
        $mockIdentifiers = [];
        $mockIdentifiers[0] = $this->getMockBuilder('Importer\Mapper\BasicIdentifier')->disableOriginalConstructor()->setMethods(
            ['getAttribute']
        )->getMock();

        $mockIdentifiers[0]->expects($this->any())->method('getAttribute')->with(
            $this->equalTo('identifier')
        )->will($this->returnValue('person'));

        return [
            //array(array('Carla'), ReferenceBuilder::SOFT_REFERENCE, $mockIdentifiers[0], $people[0]),
            [['Carla'], ReferenceBuilder::HARD_REFERENCE, $mockIdentifiers[0], $people[1]],
            [['Carla'], ReferenceBuilder::SOFT_REFERENCE, $mockIdentifiers[0], $people[0]],
            [['Gabriel'], ReferenceBuilder::HARD_REFERENCE, $mockIdentifiers[0], $people[1]],
            [['Gabriel'], ReferenceBuilder::SOFT_REFERENCE, $mockIdentifiers[0], $people[1]],
            [['Pepe'], ReferenceBuilder::HARD_REFERENCE, $mockIdentifiers[0], $people[1]],
            [['Pepe'], ReferenceBuilder::SOFT_REFERENCE, $mockIdentifiers[0], $people[1]],
        ];
    }

    public function invalidTestParamsProvider()
    {
        // Fake Identifier objects
        $mockIdentifiers = [];
        $mockIdentifiers[0] = $this->getMockBuilder('Importer\Mapper\BasicIdentifier')->disableOriginalConstructor()->setMethods(
            ['getAttribute']
        )->getMock();

        $mockIdentifiers[0]->expects($this->any())->method('getAttribute')->with(
            $this->equalTo('identifier')
        )->will($this->returnValue('participant'));

        return [
            [['Carla'], ReferenceBuilder::HARD_REFERENCE, $mockIdentifiers[0]],
            [['Mickey'], ReferenceBuilder::SOFT_REFERENCE, $mockIdentifiers[0]],
            [['Mickey'], ReferenceBuilder::HARD_REFERENCE, $mockIdentifiers[0]]
        ];
    }

    public function validBuildParamsProvider()
    {
        // People
        $people = [];
        $people[0] = new Person();
        $people[0]->setName('Carla');
        $people[0]->setBirthday('23/09/1980');
        $people[1] = new Person();
        $people[1]->setName('Gabriel');
        $people[1]->setBirthday('25/08/1970');

        // Fake identifier values
        $mockIdentifierMap = [

            ['identifier', 'person'],
            ['formatter', 'date_formatter'],
            ['mapping', 'birthday']
        ];

        $mockIdentifierExistsMap = [

            ['formatter', true],

        ];

        // Fake Identifier objects
        $mockIdentifiers = [];
        $mockIdentifiers[0] = $this->getMockBuilder('Importer\Mapper\BasicIdentifier')->disableOriginalConstructor()->setMethods(
            ['getAttribute', 'hasAttribute']
        )->getMock();

        $mockIdentifiers[0]->expects($this->any())->method('getAttribute')->with(
            $this->anything()
        )->will($this->returnValueMap($mockIdentifierMap));

        $mockIdentifiers[0]->expects($this->any())->method('hasAttribute')->with(
            $this->anything()
        )->will($this->returnValueMap($mockIdentifierExistsMap));

        return [
            [['Carla'], ReferenceBuilder::SOFT_REFERENCE, $mockIdentifiers[0], '1980-09-23', $people[0]],
            [['Carla'], ReferenceBuilder::HARD_REFERENCE, $mockIdentifiers[0], '1970-08-25', $people[1]],
            [['Gabriel'], ReferenceBuilder::SOFT_REFERENCE, $mockIdentifiers[0], '1970-08-25', $people[1]],
            [['Gabriel'], ReferenceBuilder::HARD_REFERENCE, $mockIdentifiers[0], '1970-08-25', $people[1]],

        ];
    }

    /**
     * Testing the ReferenceBuilder create() method for a series of test parameters with various reference types.
     * @dataProvider validTestParamsAndObjectsProvider
     * @param $testParams
     * @param $refType
     * @param $expectedObject
     */
    public function testServiceBuilderCreate($testParams, $refType, $serviceId, $expectedObject)
    {
        $this->rbt->setType($refType);
        $this->rbt->setFinderParams($testParams);
        $this->rbt->create($serviceId);
        $this->assertEquals($this->rbt->get(), $expectedObject, 'The created and expected objects are not equal.');
    }

    /**
     * Testing how the ReferenceBuilder create() method handles exceptions.
     * @dataProvider invalidTestParamsProvider
     * @param $testParams
     * @param $refType
     */
    public function testServiceBuilderCreateFail($testParams, $refType, $serviceId)
    {
        try {

            $this->rbt->setType($refType);
            $this->rbt->setFinderParams($testParams);
            $this->rbt->create($serviceId);

        } catch (Exception $e) {

            return;
        }

        $this->fail('The create method did not handle exceptions correctly.');
    }

    /**
     * Testing if the build methods from this builder construct the service correctly.
     * @dataProvider validBuildParamsProvider
     */
    public function testServiceBuilderBuild($testParams, $refType, $serviceId, $testData, $expectedObject)
    {
        $this->rbt->setType($refType);
        $this->rbt->setFinderParams($testParams);
        $this->rbt->create($serviceId);
        $this->rbt->buildData($testData);
        $this->rbt->buildToContainer();

        $this->assertEquals($this->rbt->get(), $expectedObject, 'The built and expected objects are not equal.');
    }
}