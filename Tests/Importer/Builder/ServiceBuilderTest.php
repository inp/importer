<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 2/05/13
 * Time: 06:09 PM
 *
 */

namespace Tests\Importer\Builder;

use Importer\Builder\ServiceBuilder;
use Exception;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Tests\Resources\Models\Person;

require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Person.php');


class ServiceBuilderTest extends TestCase
{
    /**
     * @var ServiceBuilder
     */
    public $sbt;

    public function setUp(): void
    {

        // People
        $people = [];
        $people[0] = new Person();
        $people[0]->setName('Carla');
        $people[1] = new Person();
        $people[1]->setName('Gabriel');

        // Fake Finder object
        $mockFinder = $this->getMockBuilder('Finder\ClassMethodFinder')->disableOriginalConstructor()->setMethods(
            ['findElements']
        )->getMock();


        // Fake Found objects
        $mockFoundObjectsMap = [
            [['Carla'], [$people[0]]],
            [['Pepe'], []],
            [['Mickey'], []],
        ];

        $mockFinder->expects($this->any())->method('findElements')->with(
            $this->anything()
        )->will($this->returnValueMap($mockFoundObjectsMap));

        // Fake container
        $mockContainer = $this->getMockBuilder(
            'Symfony\Component\DependencyInjection\ContainerBuilder'
        )->setMethods(
            ['get']
        )->getMock();

        //Fake container services
        $mockContainerServicesMap = [
            ['person', $people[1]]
        ];

        $mockContainer->expects($this->any())->method('get')->with(
            $this->anything()
        )->will($this->returnValueMap($mockContainerServicesMap));

        $this->sbt = new ServiceBuilder();
        $this->sbt->setContainer($mockContainer);
        $this->sbt->setFinder($mockFinder);
        $this->sbt->setDataAccessor(PropertyAccess::createPropertyAccessor());
    }

    public function testServiceBuilderCreation()
    {
        $this->assertNotNull($this->sbt, 'The ServiceBuilder object could not be created.');
    }

    public function validTestParamsAndObjectsProvider()
    {
        // People
        $people = [];
        $people[0] = new Person();
        $people[0]->setName('Carla');
        $people[1] = new Person();
        $people[1]->setName('Gabriel');

        // Fake Identifier objects
        $mockIdentifiers = [];
        $mockIdentifiers[0] = $this->getMockBuilder('Mapper\BasicIdentifier')->disableOriginalConstructor()->setMethods(
            ['getAttribute', 'hasAttribute']
        )->getMock();

        $mockIdentifiers[0]->expects($this->any())->method('getAttribute')->with(
            $this->equalTo('identifier')
        )->will($this->returnValue('person'));

        $mockIdentifiers[0]->expects($this->any())->method('hasAttribute')->with(
            $this->equalTo('identifier')
        )->will($this->returnValue(true));

        return [
            [['Carla'], $mockIdentifiers[0], $people[0]],
            [['Carla'], $mockIdentifiers[0], $people[0]],
        ];
    }

    public function invalidTestParamsProvider()
    {

        // Fake Identifier objects
        $mockIdentifiers = [];
        $mockIdentifiers[0] = $this->getMockBuilder('Mapper\BasicIdentifier')->disableOriginalConstructor()->setMethods(
            ['getAttribute', 'hasAttribute']
        )->getMock();

        $mockIdentifiers[0]->expects($this->any())->method('getAttribute')->with(
            $this->equalTo('identifier')
        )->will($this->returnValue('person'));

        $mockIdentifiers[0]->expects($this->any())->method('hasAttribute')->with(
            $this->equalTo('identifier')
        )->will($this->returnValue(true));

        return [
            [['Pepe'], $mockIdentifiers[0]],
            [['Mickey'], $mockIdentifiers[0]],
            [['Mickey'], $mockIdentifiers[0]]
        ];
    }

    /**
     * Testing the ServiceBuilder create() method for a series of test parameters with various reference types.
     * @dataProvider validTestParamsAndObjectsProvider
     * @param $testParams
     * @param $expectedObject
     */
    public function testServiceBuilderCreate($testParams, $testId, $expectedObject)
    {
        $this->sbt->setFinderParams($testParams);
        $this->sbt->create($testId);
        $this->assertEquals($this->sbt->get(), $expectedObject, 'The created and expected objects are not equal.');
    }

    /**
     * Testing how the ServiceBuilder create() method handles exceptions.
     * @dataProvider invalidTestParamsProvider
     * @param $testParams
     */
    public function testServiceBuilderCreateFail($testParams, $testId)
    {

        try {

            $this->sbt->setFinderParams($testParams);
            $this->sbt->create($testId);

        } catch (Exception $e) {

            $this->assertTrue(true);
            return;
        }

        $this->fail('The create method did not handle exceptions correctly.');
    }
}