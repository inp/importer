<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 15/04/13
 * Time: 04:35 PM
 *
 */

namespace Tests\Importer\Builder;

use PHPUnit\Framework\TestCase;
use Tests\Resources\Models\PersonDirector;
use Importer\Builder\GenericBuilder;
use Symfony\Component\PropertyAccess\PropertyAccess;

require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Person.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Address.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Sale.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Telephone.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'TelephoneCompany.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'PersonDirector.php');

class GenericBuilderTest extends TestCase
{
    /**
     * @var GenericBuilder
     */
    public static $gbt;

    public static function setUpBeforeClass() : void
    {
        self::$gbt = new GenericBuilder('Tests\Resources\Models\Person');
    }

    public function validTestValuesProvider()
    {
        return array(
            array(
                array(
                    'name' => 'Luis',
                    'birthday' => '1986-03-23',
                    'address.address' => 'Cra 23C No 45D - 78',
                    'address.zip' => '00000',
                    'address.city' => 'Bogota',
                    'address.state' => '¿Que?',
                    'telephone.number' => '3456734',
                    'telephone.company.nit' => '132433434334332',
                    'sale.quantity' => '$1\'234.645,00',
                    'sale.address.address' => 'Cra 98 No 125F - 67',
                    'sale.address.zip' => 'No se',
                    'sale.address.city' => 'Bogota',
                    'sale.address.state' => '¿Cundinamarca es un estado?'
                ),
                array(
                    'name' => 'Pepe',
                    'birthday' => '1987-10-06',
                    'address.address' => 'Cra 123 No 145 - 8',
                    'address.zip' => '00000',
                    'address.city' => 'Bogota',
                    'address.state' => '¿Que?',
                    'telephone.number' => '6456923',
                    'telephone.company.nit' => '145454545454',
                    'sale.quantity' => '$234.645,00',
                    'sale.address.address' => 'Cra 12 No 37B - 67',
                    'sale.address.zip' => 'No se',
                    'sale.address.city' => 'Bogota',
                    'sale.address.state' => '¿Cundinamarca es un estado?'
                ),
                array(
                    'name' => 'Karol',
                    'birthday' => '1980-10-13',
                    'address.address' => 'Cra 19 No 85B - 20',
                    'address.zip' => '00000',
                    'address.city' => 'Bogota',
                    'address.state' => '¿Que?',
                    'telephone.number' => '4567912',
                    'telephone.company.nit' => '132433434334332',
                    'sale.quantity' => '$2\'854.645,00',
                    'sale.address.address' => 'Cll 161 No 20 - 7',
                    'sale.address.zip' => 'No se',
                    'sale.address.city' => 'Bogota',
                    'sale.address.state' => '¿Cundinamarca es un estado?'
                ),
            )
        );
    }

    /**
     * Testing the product object creation.
     */
    public function testGenericBuilderCreate()
    {
        self::$gbt->create();
        $this->assertNotNull(self::$gbt->get(), 'The Builder could not create the object.');
    }

    /**
     * Testing the PersonDirector constructPerson method.
     * @depends testGenericBuilderCreate
     * @dataProvider validTestValuesProvider
     */
    public function testPersonDirectorConstructPerson($testValues)
    {
        // The director only needs the path -> value mapping to assign data.
        $personDirector = new PersonDirector();
        $person = $personDirector->constructPerson($testValues);

        // Using a PropertyAccessor to assert that values are stored in the right place.
        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($testValues as $path => $value) {

            $this->assertEquals(
                $accessor->getValue($person, $path),
                $value,
                'The PersonConstructor could not assign the data correctly.'
            );
        }
    }
}