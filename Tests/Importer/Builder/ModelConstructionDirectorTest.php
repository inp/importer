<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 6/05/13
 * Time: 05:45 PM
 *
 */

namespace Tests\Importer\Builder {

    use Importer\Builder\ModelConstructionDirector;
    use Symfony\Component\DependencyInjection\ContainerBuilder;
    use PHPUnit\Framework\TestCase;

    class ModelConstructionDirectorTest extends TestCase
    {
        /**
         * @var ModelConstructionDirector
         */
        public static $mcdt;

        public static function setUpBeforeClass() : void
        {
            self::$mcdt = new ModelConstructionDirector();
        }

        /**
         * Testing the ModelConstructionDirector creation.
         */
        public function testModelConstructionDirectorCreate()
        {

            $this->assertNotNull(self::$mcdt, 'The ModelConstructionDirector could not be created.');
        }

    }
}