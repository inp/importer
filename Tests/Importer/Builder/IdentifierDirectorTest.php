<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 26/04/13
 * Time: 12:27 PM
 *
 */

namespace Tests\Importer\Builder;

use Importer\Builder\IdentifierDirector;
use Importer\Mapper\BasicIdentifier;
use PHPUnit\Framework\TestCase;

class IdentifierDirectorTest extends TestCase
{
    /**
     * @var IdentifierDirector
     */
    public static $idt;

    public static function setUpBeforeClass(): void
    {
        self::$idt = new IdentifierDirector();
    }

    public function validTestDataProvider()
    {
        $identifiers = [];

        $identifiers[0] = new BasicIdentifier('Person');
        $identifiers[0]->setAttribute('mapping', 'id');
        $identifiers[0]->setAttribute('builder', 'SoftReference');
        $identifiers[0]->setAttribute('identifier', 'person');
        $identifiers[0]->setAttribute('finder', ['PersonQuery', 'findById']);

        $identifiers[2] = new BasicIdentifier('column_builder');
        $identifiers[2]->setAttribute('builder', 'SoftReference');
        $identifiers[2]->setAttribute('finder', ['FieldQuery', 'findFieldByName']);

        $identifiers[1] = new BasicIdentifier('Field1235');
        $identifiers[1]->setAttribute('mapping', 'personField.name');
        $identifiers[1]->setAttribute(
            'column_builder',
            $identifiers[2]
        );
        $identifiers[1]->setAttribute('builder', 'HardReference');
        $identifiers[1]->setAttribute('finder', ['FieldQuery', 'findByName']);


        return [
            [
                [
                    'Person' =>
                        [
                            'regex' => '/Id/',
                            'mapping' => 'id',
                            'type' => 'SoftReference',
                            'finder' => [
                                'PersonQuery',
                                'findById'
                            ],
                            'identifier' => 'person'
                        ]
                ],
                $identifiers[0]
            ],
            [
                [
                    'Field1235' =>
                        [
                            'regex' => '/^Field[0-9]+/',
                            'mapping' => 'personField.name',
                            'type' => 'HardReference',
                            'columnBuilder' => [
                                'type' => 'SoftReference',
                                'finder' => ['FieldQuery', 'findFieldByName']
                            ],
                            'finder' => [
                                'FieldQuery',
                                'findByName'
                            ],
                        ]
                ],
                $identifiers[1]
            ],
        ];
    }

    /**
     * Testing the IdentifierDirector object creation.
     */
    public function testIdentifierDirectorTestCreate()
    {
        $this->assertNotNull(self::$idt, 'The IdentifierDirector object could not be created.');
    }

    /**
     * Testing the construct method.
     * @param $testMap
     * @param $expectedId
     * @dataProvider validTestDataProvider
     */
    public function testIdentifierDirectorConstruct($testMap, $expectedId)
    {
        $id = self::$idt->construct($testMap);
        $this->assertTrue($id instanceof BasicIdentifier, 'The constructed object is not a valid Identifier.');
        $this->assertEquals($expectedId, $id, 'The Identifier object is different from the expected.');
    }

}