<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 25/04/13
 * Time: 03:06 PM
 *
 */

namespace Tests\Importer\Mapper;

use Exception;
use Importer\Mapper\BasicIdentifier;
use Importer\Mapper\ConfigurationMapper;
use PHPUnit\Framework\TestCase;

class ConfigurationMapperTest extends TestCase
{
    /**
     * @var ConfigurationMapper
     */
    public static $cmt;

    public static function setUpBeforeClass(): void
    {

        self::$cmt = new ConfigurationMapper();
    }

    public function validTestMockObjectsProvider()
    {
        // Fake identifiers
        $testMockIds = ['Id', 'Name', 'Email', 'Field1234'];

        $testMockSource = 'file.yaml';

        // Fake map
        $testMockMap = [
            'Id' => [
                'regex' => '/Id/',
                'mapping' => 'Id',
                'type' => 'SoftReference',
                'finder' => ['PersonQuery', 'findById'],
                'identifier' => 'person',
                'dataType' => 'drawing'
            ],
            'Name' => [
                'regex' => '/Name/',
                'mapping' => 'Name',
                'type' => 'HardReference',
                'finder' => ['PersonQuery', 'findByName'],
                'identifier' => 'person'
            ],
            'Email' => [
                'regex' => '/Email/',
                'mapping' => 'email',
                'type' => 'SoftReference',
                'finder' => ['PersonQuery', 'findById'],
                'identifier' => 'person'
            ],
            'Field' => [
                'regex' => '/^Field[0-9]+/',
                'mapping' => 'Id',
                'type' => 'SoftReference',
                'finder' => ['PersonQuery', 'findById'],
                'identifier' => 'person'
            ],
        ];

        $testMockIdMap = [

            [[$testMockIds[0] => $testMockMap['Id']], new BasicIdentifier($testMockIds[0])],
            [[$testMockIds[1] => $testMockMap['Name']], new BasicIdentifier($testMockIds[1])],
            [[$testMockIds[2] => $testMockMap['Email']], new BasicIdentifier($testMockIds[2])],
            [[$testMockIds[3] => $testMockMap['Field']], new BasicIdentifier($testMockIds[3])]
        ];

        // Fake parser
        $testMockParser = $this->getMockBuilder('Importer\Parser\YamlFileParser')->disableOriginalConstructor()->getMock();
        $testMockParser->expects($this->any())->method('parse')->with($this->anything())->will(
            $this->returnValue($testMockMap)
        );

        // Fake Director
        $testMockDirector = $this->getMockBuilder('Importer\Builder\IdentifierDirector')->disableOriginalConstructor()->getMock();
        $testMockDirector->expects($this->any())->method('construct')->with($this->anything())->will(
            $this->returnValueMap($testMockIdMap)
        );

        // Test results
        $testResults = [
            'Id' => [$testMockIdMap[0][1]],
            'Name' => [$testMockIdMap[1][1]],
            'Email' => [$testMockIdMap[2][1]],
            'Field1234' => [$testMockIdMap[3][1]],

        ];

        return [
            [$testMockParser, $testMockSource, $testMockDirector, $testMockIds, $testResults]
        ];

    }

    /**
     * Testing the ConfigurationMapper object creation.
     */
    public function testConfigurationMapperTestCreate()
    {
        $this->assertNotNull(self::$cmt, 'The ConfigurationMapper object could not be created.');
    }

    /**
     * Tests the getPaths method using mock objects to simulate behaviour.
     * @dataProvider validTestMockObjectsProvider
     */
    public function testConfigurationMapperGetPaths(
        $testMockParser,
        $testMockSource,
        $testMockDirector,
        $testMockIds,
        $testResults
    )
    {

        self::$cmt->setParser($testMockParser);
        self::$cmt->setMapSource($testMockSource);
        self::$cmt->setIdentifierDirector($testMockDirector);

        $paths = self::$cmt->getPaths($testMockIds);

        foreach ($paths as $key => $path) {

            $t2 = count($path);
            for ($j = 0; $j < $t2; ++$j) {

                $this->assertTrue(
                    $path[$j] instanceof BasicIdentifier,
                    'The object is not a valid identifier object.'
                );

                $this->assertEquals($testResults[$key], $path, "The Identifier objects don't match.");

            }

        }

        $this->assertEquals(count($testMockIds), count($paths), "The id list and path list sizes don't match");
    }

    /**
     * Testing if The getPaths method with strict mapping throws the requested exception.
     * @depends  testConfigurationMapperGetPaths
     */
    public function testConfigurationMapperStrictMappingFail()
    {
        $someIds = ['Birthday', 'name', '123Field'];
        $goodIds = ['Field4'];
        try { // This should fail
            self::$cmt->setStrictMapping(true);
            self::$cmt->getPaths(array_merge($someIds, $goodIds));
            self::$cmt->setStrictMapping(true);

        } catch (Exception $e) {

            $this->assertEquals(
                $e->getMessage(),
                'Some Ids could not be mapped : %ids%.',
                'The expected exception is not correct.'
            );

            return;
        }

        $this->fail('The getPaths method failed to throw an Exception.');
    }
}