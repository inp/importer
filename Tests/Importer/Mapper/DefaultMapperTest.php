<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 11/04/13
 * Time: 03:19 PM
 *
 */

namespace Tests\Importer\Mapper;

use Importer\Mapper\DefaultMapper;
use Importer\Parser\YamlFileParser;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;

require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Person.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Address.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Sale.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'Telephone.php');
require_once(__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
    DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . 'TelephoneCompany.php');

class DefaultMapperTest extends TestCase
{
    /**
     * @var DefaultMapper
     */
    public static $dmt;

    public static function setUpBeforeClass(): void
    {
        self::$dmt = new DefaultMapper(new YamlFileParser(), __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
            DIRECTORY_SEPARATOR . 'Yaml' . DIRECTORY_SEPARATOR . 'test_map.yml', true);
    }

    public function validIdentifierMapProvider()
    {
        return [
            [
                [
                    'Name',
                    'Birthday',
                    'Address',
                    'Zip',
                    'City',
                    'State',
                    'Telephone',
                    'Telephone Company',
                    'Sale',
                    'Sale Address',
                    'Sale Zip',
                    'Sale City',
                    'Sale State'
                ],
                [
                    'Name' => 'name',
                    'Birthday' => 'birthday',
                    'Address' => 'address.address',
                    'Zip' => 'address.zip',
                    'City' => 'address.city',
                    'State' => 'address.state',
                    'Telephone' => 'telephone.number',
                    'Telephone Company' => 'telephone.company.nit',
                    'Sale' => 'sale.quantity',
                    'Sale Address' => 'sale.address.address',
                    'Sale Zip' => 'sale.address.zip',
                    'Sale City' => 'sale.address.city',
                    'Sale State' => 'sale.address.state'
                ]
            ]
        ];
    }

    /**
     * Data provider for testing the DefaultMapper MultiMap capabilities.
     * @return array
     */
    public function validIdentifierMultiMapProvider()
    {
        return [
            [
                [
                    'Name',
                    'Birthday',
                    'Address',
                    'Zip',
                    'City',
                    'State',
                    'Telephone',
                    'Telephone Company',
                    'Sale',
                    'Address',
                    'Zip',
                    'City',
                    'State'
                ],
                [
                    'Name' => ['name'],
                    'Birthday' => ['birthday'],
                    'Address' => ['address.address', 'sale.address.address'],
                    'Zip' => ['address.zip', 'sale.address.zip'],
                    'City' => ['address.city', 'sale.address.city'],
                    'State' => ['address.state', 'sale.address.state'],
                    'Telephone' => ['telephone.number'],
                    'Telephone Company' => ['telephone.company.nit'],
                    'Sale' => ['sale.quantity'],
                ]
            ]
        ];
    }

    /**
     * This provides a set of test values that should be set (or get) to certain property retrieved from the Mapper.
     * @return array A set of test values for the Property Access.
     */
    public function validTestValuesProvider()
    {
        return [
            [
                [
                    'name' => 'Luis',
                    'birthday' => '1986-03-23',
                    'address.address' => 'Cra 23C No 45D - 78',
                    'address.zip' => '00000',
                    'address.city' => 'Bogota',
                    'address.state' => '¿Que?',
                    'telephone.number' => '3456734',
                    'telephone.company.nit' => '132433434334332',
                    'sale.quantity' => '$1\'234.645,00',
                    'sale.address.address' => 'Cra 98 No 125F - 67',
                    'sale.address.zip' => 'No se',
                    'sale.address.city' => 'Bogota',
                    'sale.address.state' => '¿Cundinamarca es un estado?'
                ]
            ]
        ];
    }

    /**
     * Testing if the DefaultMapper object could be created successfully.
     */
    public function testDefaultMapperCreation()
    {
        $this->assertNotNull(self::$dmt, 'Could not create DefaultMapper object.');
    }

    /**
     * Tests the behavior of the getPaths method.
     * @depends      testDefaultMapperCreation
     * @dataProvider validIdentifierMapProvider
     */
    public function testDefaultMapperGetPaths($identifiers, $expectedMap)
    {
        $properties = self::$dmt->getPaths($identifiers);

        // Map sizes evaluation
        $t = count($properties);
        $this->assertEquals($t, count($expectedMap), 'Map sizes doesn\'t match.');

        // Key and value evaluation
        foreach ($properties as $key => $value) {

            $this->assertTrue(array_key_exists($key, $expectedMap), 'Property key doesn\'t exist in expected map.');
            $this->assertEquals($expectedMap[$key], $value, 'Map values aren\'t equal.');
        }

        return $properties;
    }
}