<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 12/04/13
 * Time: 05:56 PM
 *
 */

namespace Tests\Importer\Mapper;

use Importer\Mapper\OrderedMapper;
use Importer\Parser\YamlFileParser;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;

class OrderedMapperTest extends TestCase
{
    /**
     * @var OrderedMapper
     */
    public static $omt;

    public static function setUpBeforeClass(): void
    {
        self::$omt = new OrderedMapper(new YamlFileParser(), __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Resources' .
            DIRECTORY_SEPARATOR . 'Yaml' . DIRECTORY_SEPARATOR . 'test_map2.yml', true);
    }

    /**
     * Data provider for testing the OrderedMapper capabilities.
     * @return array
     */
    public function validIdentifierProvider()
    {
        return [
            [
                [
                    'Name',
                    'Birthday',
                    'Address',
                    'Zip',
                    'City',
                    'State',
                    'Telephone',
                    'Telephone Company',
                    'Sale',
                    'Address',
                    'Zip',
                    'City',
                    'State'
                ],
                [
                    'name',
                    'birthday',
                    'address.address',
                    'address.zip',
                    'address.city',
                    'address.state',
                    'telephone.number',
                    'telephone.company.nit',
                    'sale.quantity',
                    'sale.address.address',
                    'sale.address.zip',
                    'sale.address.city',
                    'sale.address.state'
                ]
            ]
        ];
    }

    /**
     * This provides a set of test values that should be set (or get) to certain property retrieved from the Mapper.
     * @return array A set of test values for the Property Access.
     */
    public function validTestValuesProvider()
    {
        return [
            [
                [
                    'name' => 'Luis',
                    'birthday' => '1986-03-23',
                    'address.address' => 'Cra 23C No 45D - 78',
                    'address.zip' => '00000',
                    'address.city' => 'Bogota',
                    'address.state' => '¿Que?',
                    'telephone.number' => '3456734',
                    'telephone.company.nit' => '132433434334332',
                    'sale.quantity' => '$1\'234.645,00',
                    'sale.address.address' => 'Cra 98 No 125F - 67',
                    'sale.address.zip' => 'No se',
                    'sale.address.city' => 'Bogota',
                    'sale.address.state' => '¿Cundinamarca es un estado?'
                ],
                [
                    'name' => 'Pepe',
                    'birthday' => '1987-10-06',
                    'address.address' => 'Cra 123 No 145 - 8',
                    'address.zip' => '00000',
                    'address.city' => 'Bogota',
                    'address.state' => '¿Que?',
                    'telephone.number' => '6456923',
                    'telephone.company.nit' => '145454545454',
                    'sale.quantity' => '$234.645,00',
                    'sale.address.address' => 'Cra 12 No 37B - 67',
                    'sale.address.zip' => 'No se',
                    'sale.address.city' => 'Bogota',
                    'sale.address.state' => '¿Cundinamarca es un estado?'
                ],
                [
                    'name' => 'Karol',
                    'birthday' => '1980-10-13',
                    'address.address' => 'Cra 19 No 85B - 20',
                    'address.zip' => '00000',
                    'address.city' => 'Bogota',
                    'address.state' => '¿Que?',
                    'telephone.number' => '4567912',
                    'telephone.company.nit' => '132433434334332',
                    'sale.quantity' => '$2\'854.645,00',
                    'sale.address.address' => 'Cll 161 No 20 - 7',
                    'sale.address.zip' => 'No se',
                    'sale.address.city' => 'Bogota',
                    'sale.address.state' => '¿Cundinamarca es un estado?'
                ],
            ]
        ];
    }

    /**
     * Testing if the OrderedMapper object could be created successfully.
     */
    public function testOrderedMapperCreation()
    {
        $this->assertNotNull(self::$omt, 'Could not create OrderedMapper object.');
    }

    /**
     * Tests the behavior of the getPaths method.
     * @depends      testOrderedMapperCreation
     * @dataProvider validIdentifierProvider
     */
    public function testOrderedMapperGetPaths($identifiers, $expectedPaths)
    {
        $properties = self::$omt->getPaths($identifiers);

        // Map sizes evaluation
        $t = count($properties);
        $this->assertEquals($t, count($expectedPaths), 'Path list sizes doesn\'t match.');

        // Key and value evaluation
        foreach ($properties as $idx => $value) {

            $this->assertEquals($expectedPaths[$idx], $value, 'Path lists values aren\'t equal.');
        }

        return $properties;
    }
}