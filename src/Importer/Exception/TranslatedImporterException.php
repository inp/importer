<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 16/07/13
 * Time: 04:10 PM
 *
 */

namespace Importer\Exception;

use Symfony\Component\Translation\Loader\LoaderInterface;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class TranslatedImporterException
 *
 * An Exception intended to be translated. Each object
 *
 * @package Importer\Exception
 */
class TranslatedImporterException extends BaseImporterException
{

    /**
     * @var Translator current translator object, it must be ready for use.
     */
    protected static $translator;

    /**
     * @var array An associative array with placeholders for the translated message.
     */
    protected $placeHolders;

    public function __construct($message = "", $code = 0, $previous = null, $params = null, $placeHolders = null)
    {

        parent::__construct($message, $code, $previous, $params);
        $this->placeHolders = !is_array($placeHolders) ? array() : $placeHolders;
    }

    /**
     * Starts the translator
     */
    public static function setUpTranslator($locale = 'es', $messageSelector = null)
    {

        self::$translator = new Translator($locale, $messageSelector);
        self::$translator->addLoader('xlf', new XliffFileLoader());
        self::$translator->addResource(
            'xlf',
            __DIR__ . DIRECTORY_SEPARATOR . $locale . DIRECTORY_SEPARATOR . 'messages.xml',
            $locale
        );
    }

    /**
     * Returns the current translator.
     * @return Translator
     */
    public static function getTranslator()
    {

        return self::$translator;
    }

    /**
     * Adds a resource for the translator
     * @param $format
     * @param $resource
     * @param $locale
     * @param null $domain
     */
    public static function addResource($format, $resource, $locale, $domain = null)
    {

        self::$translator->addResource($format, $resource, $locale, $domain);
    }


    public static function addLoader($format, LoaderInterface $loader)
    {

        self::$translator->addLoader($format, $loader);
    }

    /**
     * Returns a translated version of the Exception's message.
     * @return string
     */
    public function getTranslatedMessage()
    {

        return self::$translator->trans($this->getMessage(), $this->placeHolders);
    }

}