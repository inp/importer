<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 10:21 AM
 *
 */

namespace Importer\Exception;

/**
 * Class BaseImporterException
 *
 * Base Exception class for the Importer, intended to have additional information.
 *
 * @package Importer\Exception
 */
class BaseImporterException extends \Exception
{

    /**
     * @var mixed Container for extra information about an Importer Exception.
     */
    protected $params;

    public function __construct($message = "", $code = 0, $previous = null, $params = null)
    {

        parent::__construct($message, $code, $previous);
        $this->params = $params;
    }

    /**
     * Returns the additional parameters.
     * @return mixed|null
     */
    public function getParams()
    {

        return $this->params;
    }
}