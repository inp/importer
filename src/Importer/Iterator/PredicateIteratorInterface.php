<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 16/04/13
 * Time: 04:28 PM
 *
 */

namespace Importer\Iterator;

use Importer\Predicate\AbstractPredicate;

/**
 * Class PredicateIteratorInterface
 *
 * An special interface extending IteratorInterface to provide a Predicate based filter for choosing which elements
 * should be traversed.
 * @package Iterator
 */
interface PredicateIteratorInterface extends IteratorInterface
{
    /**
     * Method for setting up the predicate filter.
     * @param AbstractPredicate $filter The filter predicate to be used
     * @return mixed
     */
    public function setPredicate(AbstractPredicate $filter);
}