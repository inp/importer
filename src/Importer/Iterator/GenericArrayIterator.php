<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 16/04/13
 * Time: 04:37 PM
 *
 */

namespace Importer\Iterator;

/**
 * Class GenericArrayIterator
 *
 * An Iterator for an array based container
 * @package Iterator
 */
class GenericArrayIterator implements IteratorInterface
{
    protected $list;

    public function __construct(&$array)
    {
        $this->list = &$array;
        $this->reset();
    }

    public function reset()
    {
        reset($this->list);
    }

    public function current()
    {
        return current($this->list);
    }

    public function key()
    {
        return key($this->list);
    }

    public function next()
    {
        next($this->list);
    }

    public function valid()
    {
        return array_key_exists($this->key(), $this->list);
    }

}