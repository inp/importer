<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 16/04/13
 * Time: 04:19 PM
 *
 */

namespace Importer\Iterator;

/**
 * Class IteratorInterface
 *
 * Common Iterator interface for traversing containers transparently. It has a PHP alike interface, but it's independent.
 * @package Iterator
 */
interface IteratorInterface
{
    public function reset();

    public function current();

    public function key();

    public function next();

    public function valid();
}