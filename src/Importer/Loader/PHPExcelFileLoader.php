<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 18/04/13
 * Time: 09:44 AM
 *
 */

namespace Importer\Loader;

use Exception;
use Importer\Loader\Exception\LoaderSourceLoadingException;
use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Reader_CSV;
use Importer\Predicate\AbstractPredicate;

/**
 * Class PHPExcelFileLoader
 *
 * The loading strategy based on PHPExcel for Excel file loading.
 * @package Loader
 */
class PHPExcelFileLoader implements LoaderInterface
{
    /**
     * @var PHPExcel The loaded resource.
     */
    protected $resource;

    /**
     * @var AbstractPredicate The file predicate used to validate the input file.
     */
    protected $fileIsValidPredicate;

    /**
     * Method for loading an Excel source file into a PHPExcel object.
     * @param $source string The requested resource's URL.
     * @throws LoaderSourceLoadingException If the file could not be successfully loaded via the IOFactory
     */
    public function load($source)
    {
        try {

            $this->fileIsValidPredicate->evaluate($source);

            $pathinfo = pathinfo($source);

            // To explicitly load the CSV reader when needed
            if ((isset($pathinfo['extension'])) && $pathinfo['extension'] == 'csv') {

                $reader = new PHPExcel_Reader_CSV();
                $this->resource = $reader->load($source);
            } else {

                $this->resource = PHPExcel_IOFactory::load($source);
            }

        } catch (Exception $e) {

            throw new LoaderSourceLoadingException('An error occurred while trying to load the file.', $e->getCode(
            ), $e);
        }
    }

    public function unload()
    {
        $loader = null;
    }

    /**
     * @return PHPExcel The loaded resource.
     */
    public function getResource()
    {
        return $this->resource;
    }

    public function setFilePredicate(AbstractPredicate $filePredicate)
    {
        $this->fileIsValidPredicate = $filePredicate;
    }
}