<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 11:46 AM
 *
 */

namespace Importer\Loader\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class LoaderSourceLoadingException
 *
 * Exception for the Loader's source loading.
 *
 * @package Importer\Importer\Exception
 */
class LoaderSourceLoadingException extends TranslatedImporterException
{

}