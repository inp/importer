<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 18/04/13
 * Time: 09:38 AM
 *
 */

namespace Importer\Loader;

/**
 * Class LoaderInterface
 *
 * Common interface for the Importer's input sources' loading strategies.
 * @package Loader
 */
interface LoaderInterface
{

    public function load($source);

    public function unload();

    public function getResource();
}