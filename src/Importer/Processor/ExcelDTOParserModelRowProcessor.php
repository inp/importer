<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 2/07/13
 * Time: 11:35 AM
 *
 */

namespace Importer\Processor;

use Exception;
use Importer\Processor\Exception\ExcelDTOParserModelRowProcessingException;

/**
 * Class ExcelDTOParserModelRowProcessor
 *
 * Processor class for additional processing over a ExcelDTOParser parsed row.
 *
 * @package Importer\Processor
 */
class ExcelDTOParserModelRowProcessor implements ProcessorInterface
{

    /**
     * @var ProcessorInterface A processing action for each cell.
     */
    protected $cellProcessor;

    public function __construct($cellProcessor = null){

        $this->cellProcessor = $cellProcessor;
    }

    /**
     * The method for processing a result into extracted data.
     * @param $row array Array with object models
     * @return array Processed row.
     */
    public function process($row)
    {

        try {

            foreach ($row as $obj) {

                $this->cellProcessor->process($obj);
            }

            return $row;
        } catch (Exception $e) {

            throw new ExcelDTOParserModelRowProcessingException('Error processing the model row.', $e->getCode(), $e);
        }
    }

    /**
     * @param \Importer\Processor\ProcessorInterface $cellProcessor
     */
    public function setCellProcessor($cellProcessor)
    {
        $this->cellProcessor = $cellProcessor;
    }

    /**
     * @return \Importer\Processor\ProcessorInterface
     */
    public function getCellProcessor()
    {
        return $this->cellProcessor;
    }
}