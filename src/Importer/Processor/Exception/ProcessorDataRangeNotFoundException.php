<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 03:07 PM
 *
 */

namespace Importer\Processor\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class ProcessorDataRangeNotFoundException
 *
 * Exception in case a data range
 *
 * @package Importer\Processor\Exception
 */
class ProcessorDataRangeNotFoundException extends TranslatedImporterException
{

}