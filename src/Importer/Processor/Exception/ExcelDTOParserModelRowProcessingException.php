<?php
/**
 * INP All rights reserved..
 * User: Juan Sebastian Robles Jimenez
 * Date: 2/07/13
 * Time: 02:34 PM
 *
 */

namespace Importer\Processor\Exception;

use Exception;
use Importer\Exception\TranslatedImporterException;

/**
 * Class ExcelDTOParserModelRowProcessingException
 *
 * Exception related to errors processing a row model.
 *
 * @package Importer\Processor\Exception
 */
class ExcelDTOParserModelRowProcessingException extends TranslatedImporterException
{

    public function ExcelDTOParserModelRowProcessingException($message, $code = 0, Exception $previous = null)
    {

        parent::__construct($message, $code, $previous);
    }

    public function __toString()
    {
        return $this->getMessage() . ' Code : ' . $this->getCode();
    }
}