<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 03:23 PM
 *
 */

namespace Importer\Processor\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class ProcessorCouldNotCleanSourceDataException
 *
 * Exception thrown in case the input data to be processed could not be cleaned.
 *
 * @package Importer\Importer\Exception
 */
class ProcessorCouldNotCleanSourceDataException extends TranslatedImporterException
{

}