<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 03:23 PM
 *
 */

namespace Importer\Processor\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class ProcessorCouldNotUnmergeRangeException
 *
 * Exception thrown if there is any issue trying to unmerge the merged cell
 * ranges from a worksheet.
 *
 * @package Importer\Importer\Exception
 */
class ProcessorCouldNotUnmergeRangeException extends TranslatedImporterException
{

}