<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 18/04/13
 * Time: 10:01 AM
 *
 */

namespace Importer\Processor;

/**
 * Class ProcessorInterface
 *
 * Common interface for the Importer's resources' processing strategies.
 * @package Processor
 */
interface ProcessorInterface
{
    /**
     * The method for processing a result into extracted data.
     * @param $toProcess Object to be processed.
     * @return mixed The results of the processing.
     */
    public function process($toProcess);
}