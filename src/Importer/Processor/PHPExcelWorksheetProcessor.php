<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 18/04/13
 * Time: 10:08 AM
 *
 */

namespace Importer\Processor;

use Importer\DTO\ExcelImporterDTO;
use Exception;
use Importer\Finder\FinderInterface;
use Importer\Predicate\AbstractPredicate;
use Importer\Processor\Exception\ProcessorCouldNotCleanSourceDataException;
use Importer\Processor\Exception\ProcessorDataRangeNotFoundException;
use Importer\Util\ExcelImporterUtil;
use PHPExcel_Worksheet;

/**
 * Class PHPExcelWorksheetProcessor
 *
 * A processor that takes a PHPExcel_Worksheet, and process it's contents to make it useful for a parser.
 * @package Processor
 */
class PHPExcelWorksheetProcessor implements ProcessorInterface
{
    /**
     * @var AbstractPredicate The predicate to define which cells should be deleted.
     */
    protected $isCellToRemove;

    /**
     * @var FinderInterface A Finder used to locate the attribute header in a worksheet.
     */
    protected $headerFinder;

    public function __construct(AbstractPredicate $isCellToRemove, FinderInterface $headerFinder)
    {

        $this->isCellToRemove = $isCellToRemove;
        $this->headerFinder = $headerFinder;
    }

    /**
     * The method for processing a result into extracted data.
     * @return mixed
     */
    public function process($worksheet)
    {
        $dataRange = $this->findRange($worksheet);
        $newDataRange = $this->removeEmptyCells($worksheet, $dataRange);
        $this->unmergeRanges($worksheet);

        // Now it has to read the sheet to find the identifiers
        $headerIdentifiers = $this->headerFinder->findElements($worksheet);

        // The new DTO is created and registered.
        return new ExcelImporterDTO($worksheet, $newDataRange, $headerIdentifiers);
    }

    /**
     * This method finds and returns an associative array with keys 'lowest' and 'highest', where both lowest and highest
     * have keys 'row' an 'col', referring to the rows and columns of thw highest and lowest cells of the data range of
     * this worksheet. This result may NOT be consistent with the state of the worksheet after a deletion operation.
     * @param \PHPExcel_Worksheet $sheet The worksheet its data range we want to find.
     * @return array An associative array with highest and lowest rows and columns from the data range of the sheet.
     * @throws \Exception
     */
    public function findRange(\PHPExcel_Worksheet $sheet)
    {
        try {

            // calculateWorksheetDataDimension() helps to get the data range, it returns a string in the format [start:
            // end] where start is the highest leftmost cell of the range and end is the lowest rightmost cell of the
            // range. According to tests, apparently PHPExcel method calculateWorksheetDataDimension takes ANYTHING
            // before the highest row and column as a data range (even white cells).
            $dataDimension = ExcelImporterUtil::stringToRange($sheet->calculateWorksheetDataDimension());

            return $dataDimension;

        } catch (Exception $e) {

            throw new ProcessorDataRangeNotFoundException('Error finding data range.', $e->getCode(), $e);
        }
    }

    /**
     * This method removes empty rows from a worksheet. PHPExcel moves rows up to cover empty rows, leaving empty rows
     * at the bottom.
     * @param \PHPExcel_Worksheet $sheet The worksheet.
     * @param $dataRange array The data range to be tested for white rows.
     * @throws \Exception
     * @return array An associative array with a new "custom" data range.
     */
    public function removeEmptyRows(\PHPExcel_Worksheet $sheet, $dataRange)
    {

        try {
            $fromColIdx = \PHPExcel_Cell::columnIndexFromString($dataRange['highest']['col']);
            $toColIdx = \PHPExcel_Cell::columnIndexFromString($dataRange['lowest']['col']);
            $fromRowIdx = $dataRange['highest']['row'];
            $toRowIdx = $dataRange['lowest']['row'];

            for ($i = $fromRowIdx; $i <= $toRowIdx; ++$i) {

                $rowIsEmpty = true;

                for ($j = $fromColIdx; $j <= $toColIdx && $rowIsEmpty; ++$j) {

                    $rowIsEmpty = $this->isCellToRemove->evaluate($sheet->getCellByColumnAndRow($j - 1, $i));
                }

                if ($rowIsEmpty) {

                    $sheet->removeRow($i);
                    --$i;
                    --$toRowIdx;
                }
            }

            // Returning a new "custom" data range.
            $newRange = $dataRange;
            $newRange['lowest']['row'] = $toRowIdx;
            return $newRange;
        } catch (Exception $e) {

            throw new ProcessorCouldNotCleanSourceDataException('Error while trying to remove empty rows.', $e->getCode(
            ), $e);
        }
    }

    /**
     * This method removes empty columns from a worksheet. PHPExcel "moves" columns to the left to cover empty columns
     * and leaving trailing empty columns.
     * @param \PHPExcel_Worksheet $sheet The worksheet.
     * @param $dataRange array The data range to be tested for white columns.
     * @throws \Exception
     * @return array An associative array with the "updated" range data.
     */
    public function removeEmptyColumns(\PHPExcel_Worksheet $sheet, $dataRange)
    {
        try {
            $fromColIdx = \PHPExcel_Cell::columnIndexFromString($dataRange['highest']['col']);
            $toColIdx = \PHPExcel_Cell::columnIndexFromString($dataRange['lowest']['col']);

            for ($i = $fromColIdx; $i <= $toColIdx; ++$i) {

                $colIsEmpty = true;

                for ($j = $dataRange['highest']['row']; $j <= $dataRange['lowest']['row'] && $colIsEmpty; ++$j) {

                    $colIsEmpty = $this->isCellToRemove->evaluate($sheet->getCellByColumnAndRow($i - 1, $j));
                }

                if ($colIsEmpty) {

                    $sheet->removeColumnByIndex($i - 1);
                    --$i;
                    --$toColIdx;
                }
            }

            // Returning a new "custom" data range.
            $newRange = $dataRange;
            $newRange['lowest']['col'] = $sheet->getCellByColumnAndRow(
                $toColIdx,
                $dataRange['highest']['row']
            )->stringFromColumnIndex($toColIdx - 1);
            return $newRange;
        } catch (Exception $e) {

            throw new ProcessorCouldNotCleanSourceDataException('Error while trying to remove empty columns.', $e->getCode(
            ), $e);
        }
    }

    /**
     * Method for removing empty cells from a worksheet in a given data range.
     * @param $sheet PHPExcel_Worksheet The worksheet  to "clean".
     * @param $dataRange array An associative array with the sheet's range to be scanned.
     * @throws ProcessorCouldNotCleanSourceDataException
     */
    public function removeEmptyCells(\PHPExcel_Worksheet $sheet, $dataRange)
    {
        try {

            $dataRange = $this->removeEmptyRows($sheet, $dataRange);
            $dataRange = $this->removeEmptyColumns($sheet, $dataRange);

            return $dataRange;
        } catch (Exception $e) {

            throw new ProcessorCouldNotCleanSourceDataException('Error while trying to remove empty cells.', $e->getCode(
            ), $e);
        }
    }

    /**
     * This method finds and separates merged cells, and takes the value of the
     * top-leftmost cell of the range and copies it into the remaining cells.
     * @param PHPExcel_Worksheet $worksheet
     * @throws ProcessorCouldNotUnmergeRangeException
     */
    public function unmergeRanges(\PHPExcel_Worksheet $worksheet)
    {
        $ranges = $worksheet->getMergeCells();

        try {

            foreach ($ranges as $range) {

                $worksheet->unmergeCells($range);

                $dataRange = explode(':', $range);
                if ($dataRange[0] == $dataRange[1]) {

                    continue;
                }

                $baseValue = $worksheet->getCell($dataRange[0])->getFormattedValue();

                $coordsIni = \PHPExcel_Cell::coordinateFromString($dataRange[0]);
                $coordsFin = \PHPExcel_Cell::coordinateFromString($dataRange[1]);

                $fromColIdx = \PHPExcel_Cell::columnIndexFromString($coordsIni[0]);
                $toColIdx = \PHPExcel_Cell::columnIndexFromString($coordsFin[0]);

                for ($i = $fromColIdx; $i <= $toColIdx; ++$i) {

                    for ($j = $coordsIni[1]; $j <= $coordsFin[1]; ++$j) {

                        $worksheet->getCellByColumnAndRow($i - 1, $j)->setValue($baseValue);
                    }
                }
            }
        } catch (Exception $e) {
            throw new ProcessorCouldNotUnmergeRangeException('Error while trying to unmerge cell ranges.', $e->getCode(
                ), $e);
        }
    }
}