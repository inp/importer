<?php
/**
 * Created by PhpStorm.
 * User: Juan Sebastian
 * Date: 04/09/2014
 * Time: 11:14 AM
 */

namespace Importer\Processor;

/**
 * Class PostParseModelProcessor
 *
 * This class handles a set of grouped data and calls a "callback" method from
 * a provided object
 * @package Importer\Processor
 */
class PostParseModelProcessor implements ProcessorInterface
{

    /**
     * @var string PropertyAccess path to the id field of a "grouping" object
     */
    protected $bucketIdPath;

    /**
     * @var string PropertyAccess path
     */
    protected $contentIdPath;

    /**
     * @var array An array containing a "Callable"
     */
    protected $processorDef;

    /** @var bool process even when some rows have errors */
    protected $processWithErrors;

    /**
     * @return bool
     */
    public function isProcessWithErrors()
    {
        return $this->processWithErrors;
    }

    /**
     * @param bool $processWithErrors
     */
    public function setProcessWithErrors($processWithErrors)
    {
        $this->processWithErrors = $processWithErrors;
    }

    /**
     * @return string
     */
    public function getBucketIdPath()
    {
        return $this->bucketIdPath;
    }

    /**
     * @param string $bucketIdPath
     */
    public function setBucketIdPath($bucketIdPath)
    {
        $this->bucketIdPath = $bucketIdPath;
    }

    /**
     * @return string
     */
    public function getContentIdPath()
    {
        return $this->contentIdPath;
    }

    /**
     * @param string $contentIdPath
     */
    public function setContentIdPath($contentIdPath)
    {
        $this->contentIdPath = $contentIdPath;
    }

    /**
     * @return array
     */
    public function getProcessorDef()
    {
        return $this->processorDef;
    }

    /**
     * @param array $processorDef
     */
    public function setProcessorDef($processorDef)
    {
        $this->processorDef = $processorDef;
    }

    public function __construct($processorDef, $bucketIdPath, $contentIdPath)
    {
        $this->processorDef = $processorDef;
        $this->bucketIdPath = $bucketIdPath;
        $this->contentIdPath = $contentIdPath;
    }

    /**
     * The method for processing a result into extracted data.
     * @param $toProcess Object to be processed.
     * @return mixed The results of the processing.
     */
    public function process($toProcess)
    {
        call_user_func_array($this->processorDef, [$toProcess]);
    }
}