<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 18/07/13
 * Time: 10:55 AM
 *
 */

namespace Importer\Processor;

/**
 * Class ExcelDTOParserCellPropelSaveProcessor
 *
 * A Processor for saving propel objects.
 *
 * @package Importer\Processor
 */
class ExcelDTOParserCellPropelSaveProcessor implements ProcessorInterface
{

    /**
     * @var array|string
     */
    protected $callback = 'save';

    /**
     * @return array|string
     */
    public function getCallback()
    {
        return $this->callback;
    }

    /**
     * @param array|string $callback
     */
    public function setCallback($callback)
    {
        $this->callback = $callback;
    }

    /**
     * The method for processing a result into extracted data.
     * @param $cell
     * @return mixed The results of the processing.
     */
    public function process($cell)
    {
        if ((is_string($this->callback)) && is_object($cell)) {

            if (method_exists($cell, $this->callback))
                $cell->{$this->callback}();

        } else if (is_array($this->callback)) {

            call_user_func_array($this->callback, [$cell]);
        }

        return $cell;
    }
}