<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 25/04/13
 * Time: 11:44 AM
 *
 */

namespace Importer\Formatter;

use Importer\Formatter\Exception\FormatterNewFormatCouldNotBeAppliedException;

/**
 * Class DateFormatter
 *
 * A Formatter that takes as an input a Date string and re-format it.
 * @package Formatter
 */
class DateFormatter extends AbstractFormatter
{
    public function __construct()
    {
        // Defining a set of input-output rules.
        $this->rules = array(
            '/(\d{4,})-(0[1-9]|\d{2})-(0[1-9]|\d{2})/' => '\3/\2/\1'
        );
    }

    /**
     * This method must be defined in each subclass for extracting the matching format from the input.
     * @param $input
     * @return mixed
     */
    protected function extractInputOutputFormat($input)
    {

        foreach ($this->rules as $key => $value) {

            if (preg_match($key, $input) === 1) {

                return array($key, $value);
            }

        }

        throw new FormatterNewFormatCouldNotBeAppliedException('%value% is not a valid value, some options are %options%.', E_WARNING, null, null, array('%value%' => $input, '%options%' => sprintf("\"%s\", \"%s\"", '1970-01-01', date('Y-m-d'))));
    }

    /**
     * This method must be defined in each subclass for giving the input a new format
     * @param $toFormat
     * @param $inputFormat
     * @param $outputFormat
     * @throws Exception\FormatterNewFormatCouldNotBeAppliedException
     * @return mixed
     */
    protected function formatFromInputToOutput($toFormat, $inputFormat, $outputFormat)
    {
        $formatted = preg_replace($inputFormat, $outputFormat, $toFormat);
        if (!isset($formatted)) {

            throw new FormatterNewFormatCouldNotBeAppliedException('Could not apply new format.', E_WARNING, null);
        }

        return $formatted;
    }
}