<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 16/07/13
 * Time: 11:06 AM
 *
 */

namespace Importer\Formatter\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class FormatterNewFormatCouldNotBeAppliedException
 *
 * Exception when an input format rule cannot be replaced by a new format rule.
 *
 * @package Importer\Formatter\Exception
 */
class FormatterNewFormatCouldNotBeAppliedException extends TranslatedImporterException
{

}