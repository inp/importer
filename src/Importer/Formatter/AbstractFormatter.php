<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 25/04/13
 * Time: 11:16 AM
 *
 */

namespace Importer\Formatter;


abstract class AbstractFormatter
{
    /**
     * @var mixed The ruleset for the formatter
     */
    protected $rules;

    /**
     * @var AbstractFormatter An optional extra formatter.
     */
    protected $formatter;

    /**
     * This function takes an input and perform a series of "nested" calls to perform a series of format tasks over a
     * given input (A Decorator fashion).
     * @param $toFormat mixed The input to be formatted.
     * @return mixed The formatted input.
     */
    public function format($toFormat)
    {
        if (!isset($this->formatter)) {

            return $this->formatInput($toFormat);
        } else {

            $formatted = $this->formatter->format($toFormat);
            return $this->formatInput($formatted);
        }
    }

    /**
     * This abstract method must be defined in each subclass for extracting the inputFormat -> outputFormat entry
     * @param $input
     * @return mixed
     */
    protected abstract function extractInputOutputFormat($input);


    /**
     * This abstract method must be defined in each subclass for giving the input a new format
     * @param $toFormat
     * @param $inputFormat
     * @param $outputFormat
     * @return mixed
     */
    protected abstract function formatFromInputToOutput($toFormat, $inputFormat, $outputFormat);

    /**
     * This method uses a Template schema based on abstract methods for formatting the input parameter.
     * @param $toFormat mixed The input to be formatted.
     * @return mixed The formatted result.
     */
    protected function formatInput($toFormat)
    {
        list($inputFormat, $outputFormat) = $this->extractInputOutputFormat($toFormat);
        $formatted = $this->formatFromInputToOutput($toFormat, $inputFormat, $outputFormat);

        return $formatted;
    }

    /**
     * Changes the rule schema for the formatter
     * @param $rules mixed The new rule set for
     */
    public function setRules($rules)
    {
        $this->rules = $rules;
    }

    public function setFormatter($formatter)
    {
        $this->formatter = $formatter;
    }
}