<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 24/04/13
 * Time: 05:36 PM
 *
 */

namespace Importer\Mapper;

/**
 * Class BasicIdentifier
 *
 * A Basic identifier with a series of attributes.
 * @package Mapper
 */
class BasicIdentifier implements IdentifierInterface
{
    /**
     * @var array The attribute map
     */
    protected $attributes;

    /**
     * @var mixed The mapped identifier
     */
    protected $identifier;

    public function __construct($id)
    {
        $this->identifier = $id;
        $this->attributes = [];
    }

    /**
     * @param $id mixed The identifier of an attribute for this identifier.
     * @return mixed Tha value associated with that attribute
     */
    public function getAttribute($id)
    {
        if ($this->hasAttribute($id)) {

            return $this->attributes[$id];
        }

        return null;
    }

    /**
     * @return mixed The identifier name.
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param $id mixed The key (attribute) we want to assign.
     * @param $value mixed The value to be assigned under the key.
     */
    public function setAttribute($id, $value)
    {
        $this->attributes[$id] = $value;
    }

    /**
     * @param $id mixed The attribute we want to query.
     * @return bool If the attribute exists for this identifier, returns true, else returns false.
     */
    public function hasAttribute($id)
    {
        return array_key_exists($id, $this->attributes);
    }

}