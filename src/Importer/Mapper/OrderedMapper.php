<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 12/04/13
 * Time: 04:42 PM
 *
 */

namespace Importer\Mapper;

use Exception;

/**
 * Class OrderedMapper
 *
 * A very simple Ordered Mapper, who takes a list of identifiers and returns an indexed array, where each position
 * has a route matching the route assigned for each identifier in its position.
 * @package Mapper
 */
class OrderedMapper extends DefaultMapper
{
    protected $indexedMap;
    protected $idx;

    public function __construct($parser = null, $mapSource = null, $strictMapping = false)
    {
        parent::__construct($parser, $mapSource, $strictMapping, true);
    }

    protected function addToFound(&$pathsFound, $path, $id)
    {
        $this->indexedMap[$this->idx++] = $path;
        parent::addToFound($pathsFound, $path, $id);
    }

    public function getPaths($toMap)
    {
        $this->idx = 0;
        $this->indexedMap = array();
        parent::getPaths($toMap);
        return $this->indexedMap;
    }

    public function setMultiMap($multiMap)
    {
        throw new Exception('This method is not implemented for this Mapper.');
    }
}