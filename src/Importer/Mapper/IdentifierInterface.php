<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 24/04/13
 * Time: 05:13 PM
 *
 */

namespace Importer\Mapper;

/**
 * Class IdentifierInterface
 *
 * A common interface for classes encapsulating an identifier and a mapped attribute (or attributes).
 * @package Mapper
 */
interface IdentifierInterface
{
    /**
     * @param $id mixed The identifier of an attribute for this identifier.
     * @return mixed Tha value associated with that attribute
     */
    public function getAttribute($id);

    /**
     * @return mixed The identifier name.
     */
    public function getIdentifier();

    /**
     * @param $id mixed The key (attribute) we want to assign.
     * @param $value mixed The value to be assigned under the key.
     */
    public function setAttribute($id, $value);

    /**
     * @param $id mixed The attribute we want to query.
     * @return bool If the attribute exists for this identifier, returns true, else returns false.
     */
    public function hasAttribute($id);
}