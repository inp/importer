<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 11/04/13
 * Time: 12:55 PM
 *
 */

namespace Importer\Mapper;

use Exception;
use Importer\Mapper\Exception\MapperMapNotCreatedException;
use Importer\Mapper\Exception\MapperPathsNotFoundException;
use Symfony\Component\Yaml\Yaml;

/**
 * Class DefaultMapper
 *
 * A simple mapper mapping an array of identifiers to map, using a provided mapper object implementing a parse() method
 * to read a path referring to a map source and returning an associative array whose keys are the provided identifiers
 * and each key refers to its path. Or returns an ex
 * @package Mapper
 */
class DefaultMapper implements MapperInterface
{
    /**
     * @var string The path to the map source.
     */
    protected $mapSource;

    /**
     * @var mixed The parser object to parse the map source.
     */
    protected $parser;

    /**
     * @var bool A flag to decide if this mapper instance has to map every identifier provided.
     */
    protected $strictMapping;

    /**
     * @var bool A flag to decide if this mapper instance returns a multimap (a key may hold more than one value).
     */
    protected $multiMap;

    /**
     * Default constructor
     * @param mixed $parser The parser to be used.
     * @param mixed $mapSource The path of the map source.
     * @param bool $strictMapping If this mapper MUST map every provided identifier.
     */
    public function __construct($parser = null, $mapSource = null, $strictMapping = false, $multiMap = false)
    {
        $this->parser = $parser;

        $this->setMapSource($mapSource);
        $this->strictMapping = $strictMapping;
        $this->multiMap = $multiMap;
    }

    /**
     * @param $parser mixed The parser object implementing the parse() object used to read the map source.
     */
    public function setParser($parser)
    {
        $this->parser = $parser;
    }

    /**
     * @param $source mixed An identifier to tell the parser where (or what) is the map source.
     */
    public function setMapSource($source)
    {
        //$this->mapSource = str_replace('$mapper.root_dir$', __DIR__, $source);
        $this->mapSource = $source;
    }

    /**
     * Method for adding a found path in the container under a given key.
     * @param $pathsFound array The pathsFound container passed BY REFERENCE.
     * @param $path string The path to be assigned.
     * @param $id string The container key under it stores the new path.
     */
    protected function addToFound(&$pathsFound, $path, $id)
    {
        if ($this->multiMap) {

            $pathsFound[$id][] = $path;
        } else {

            $pathsFound[$id] = $path;
        }
    }

    /**
     * Method for adding a key that could not be associated to a path by the Mapper.
     * @param $pathsNotFound array The pathsNotFound container passed BY REFERENCE.
     * @param $id string The identifier that could not be mapped.
     */
    protected function addToNotFound(&$pathsNotFound, $id)
    {
        $pathsNotFound[] = $id;
    }

    /**
     * @param $toMap mixed The set of elements to be mapped.
     * @return mixed A "map" associating each element of the set with its corresponding path.
     */
    public function getPaths($toMap)
    {
        try {
            // Associative array with the contents of the file.
            $map = null;

            $map = $this->parser->parse($this->mapSource);

            if (!is_array($map))
                throw new MapperMapNotCreatedException('Map file not found : %map%.', E_ERROR, null, null, ['%map%' => $this->mapSource]);

            $pathsNotFound = $this->strictMapping ? [] : null;
            $pathsFound = [];

            // Testing if we could map every path, by looking in the map if it has a key given by the current identifier.
            foreach ($toMap as $id) {

                // Testing if we need to create a new key entry for the foundPaths container. ONLY ONCE!
                if (!array_key_exists($id, $pathsFound))
                    $pathsFound[$id] = [];

                // Verifying if the we are not trying to give an identifier more values than it should.
                $currentMapSize = count($pathsFound[$id]);
                if (array_key_exists($id, $map) && (is_array($map[$id]) ? count($map[$id]) : 1 > $currentMapSize)) {

                    $this->addToFound(
                        $pathsFound,
                        $this->multiMap && is_array($map[$id]) ? $map[$id][$currentMapSize] : $map[$id],
                        $id
                    );
                } else {

                    if ($this->strictMapping) {

                        $this->addToNotFound($pathsNotFound, $id);
                    }
                }
            }
        } catch (Exception $e) {

            throw new Exception('An error ocurred while trying to map the paths.', 0, $e);
        }

        if ($this->strictMapping && count($pathsNotFound) > 0) {

            // If this mapper is using strict mapping, an Exception will be thrown reporting which ids could not be
            // mapped.
            throw new MapperPathsNotFoundException('Some identifiers could not be mapped.', E_WARNING, null, $pathsNotFound);
        }

        return $pathsFound;
    }

    /**
     * @param boolean $multiMap
     */
    public function setMultiMap($multiMap)
    {
        $this->multiMap = $multiMap;
    }

    /**
     * @return boolean
     */
    public function isMultiMap()
    {
        return $this->multiMap;
    }

    /**
     * @param boolean $strictMapping
     */
    public function setStrictMapping($strictMapping)
    {
        $this->strictMapping = $strictMapping;
    }

    /**
     * @return boolean
     */
    public function getStrictMapping()
    {
        return $this->strictMapping;
    }


}