<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 24/04/13
 * Time: 04:36 PM
 *
 */

namespace Importer\Mapper;

use Importer\Builder\DirectorInterface;
use Exception;
use Importer\Mapper\Exception\MapperIdMappingException;
use Importer\Mapper\Exception\MapperPathsNotFoundException;


/**
 * Class ConfigurationMapper
 *
 * A mapper for putting together and returning IdentifierInterface objects created from a list of identifiers. It also
 * associates every mapped attribute with a series of dependencies.
 * @package Mapper
 */
class ConfigurationMapper implements MapperInterface
{
    const MAP_ORDER = 0;
    const PATH_ORDER = 1;

    /**
     * @var string The path to the map source.
     */
    protected $mapSource;

    /**
     * @var mixed The parser object to parse the map source.
     */
    protected $parser;

    /**
     * @var bool A flag to decide if this mapper instance has to map every identifier provided.
     */
    protected $strictMapping;

    /**
     * @var DirectorInterface A Director for constructing identifiers
     */
    protected $director;

    /**
     * @var array Container for the found paths.
     */
    protected $pathsFound;

    /**
     * @var array Container for the not found paths.
     */
    protected $pathsNotFound;

    /**
     * @var array
     */
    protected $map;

    public function __construct($parser = null, $mapSource = null, $director = null, $strictMapping = false)
    {
        $this->parser = $parser;
        $this->mapSource = $mapSource;
        $this->director = $director;
        $this->strictMapping = $strictMapping;
    }

    /**
     * Loads the map data from the parser.
     */
    protected function loadMap()
    {

        $this->map = null;
        $this->map = $this->parser->parse($this->mapSource);
        if (!isset($this->map)) {

            throw new MapperPathsNotFoundException('The map could not be created : %map%.', E_WARNING, null, null, ['%map%' => $this->mapSource]);
        }

    }

    /**
     * @param $parser mixed The parser object implementing the parse() object used to read the map source.
     */
    public function setParser($parser)
    {
        $this->parser = $parser;
    }

    /**
     * @param $source mixed An identifier to tell the parser where (or what) is the map source.
     */
    public function setMapSource($source)
    {
        $this->mapSource = $source;
    }

    public function setIdentifierDirector($director)
    {
        $this->director = $director;
    }

    public function setStrictMapping($strict)
    {
        $this->strictMapping = $strict;
    }

    /**
     * Auxiliary method for building the path data structures.
     * @return array The new path containers
     */
    protected function getPathSets()
    {
        $pathLists = [
            [],
            $this->strictMapping ? [] : null
        ];

        return $pathLists;
    }

    /**
     * This method finds the map entries matching a regular expression test with the provided $id.
     * @param $id string The id to match.
     * @return array A set with the matching map entries for that key
     */
    protected function findCandidatesById($id)
    {
        $candidates = [];
        $cleanId = $this->cleanId($id);
        foreach ($this->map as $key => $entry) {

            if (preg_match($entry['regex'], $cleanId) === 1) {
                $entry['originalHeader'] = $key;

                // Creating a new mapping between the provided key and the matching map setting.
                $candidates[] = [$id => $entry];
            }
        }
        return $candidates;
    }

    /**
     * @param $id
     * @return string Normalized Id
     * @link http://myshadowself.com/coding/php-function-to-convert-accented-characters-to-their-non-accented-equivalant/
     */
    protected function cleanId($id)
    {

        $id = trim($id);
        $a = ['À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό', 'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή', 'ή'];
        $b = ['A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο', 'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η', 'η'];
        return str_replace($a, $b, $id);

    }

    /**
     * Adds (NOT SETS) an item to a list under certain key in container
     * @param $item mixed The item to be inserted.
     * @param $container mixed The container to update.
     * @param null $key The (optional) key under the item must be pushed.
     */
    protected function addTo($item, &$container, $key = null)
    {
        if ($key === null) {

            $container[] = $item;
        } else {

            if (!array_key_exists($key, $container)) {

                $container[$key] = [];
            }

            $container[$key][] = $item;
        }
    }

    /**
     * Method for mapping each id with its Identifier object
     * @param $toMap mixed Id list to map
     * @param $pathsFound mixed Created identifiers
     * @param $pathsNotFound mixed Identifiers that could not be mapped.
     * @throws Exception
     * @throws MapperPathsNotFoundException
     */
    protected function mapIds($toMap, &$pathsFound, &$pathsNotFound)
    {
        try {
            foreach ($toMap as $id) {

                $candidates = $this->findCandidatesById($id);

                // Filtering between mapped and not mapped identifiers
                if (empty($candidates)) {

                    if ($this->strictMapping) {

                        $this->addTo($id, $pathsNotFound);
                    }

                } else {

                    // Using the director we create each Identifier object for the id string.
                    foreach ($candidates as $candidate) {

                        $this->addTo($this->director->construct($candidate), $pathsFound, $id);
                    }

                }
            }

            // In case we are using strict mapping.
            if ($this->strictMapping && !empty($pathsNotFound)) {

                throw new MapperPathsNotFoundException('Some Ids could not be mapped : %ids%.', E_WARNING, null, $pathsNotFound, [
                    '%ids%' => implode(
                        ', ',
                        $pathsNotFound
                    )
                ]);
            }

        } catch (MapperIdMappingException $e) {

            throw new Exception('Error while trying to map the Ids.', $e->getCode(), $e);
        }
    }

    /**
     * Method for creating the Ids's map.
     * @param $toMap mixed The set of elements to be mapped.
     * @param int $order Which order should the keys of the output have, if the same order as the parsed map, or the
     * order in which the Id's came.
     * @return mixed A "map" associating each element of the set with its corresponding path.
     * @throws Exception
     * @throws MapperPathsNotFoundException
     */
    public function getPaths($toMap, $order = ConfigurationMapper::MAP_ORDER)
    {

        $this->loadMap();
        [$this->pathsFound, $this->pathsNotFound] = $this->getPathSets();
        $this->mapIds($toMap, $this->pathsFound, $this->pathsNotFound);

        if ($order == ConfigurationMapper::MAP_ORDER) {
            $this->sortFoundByMapRegex();
        }

        return $this->pathsFound;
    }

    /**
     * Returns the found paths.
     * @return array The array with mapped ids.
     */
    public function getPathsFound()
    {
        return $this->pathsFound;
    }

    /**
     * Returns the array with the ids that could not be mapped.
     * @return array Tha array with the id's that could not be mapped.
     */
    public function getPathsNotFound()
    {
        return $this->pathsNotFound;
    }

    public function getMap()
    {
        return $this->map;
    }

    /**
     * Method for re sorting the found path array based on the original map key order.
     * taken from http://lfhck.com/question/305636/sort-an-array-based-on-another-array
     * And modified to order indexes by the source map's regular expressions.
     */
    protected function sortFoundByMapRegex()
    {
        $ordered = [];
        foreach ($this->map as $key => $val) {
            foreach ($this->pathsFound as $key2 => $val2) {
                if (preg_match($val['regex'], $this->cleanId($key2)) === 1) {
                    $ordered[$key2] = $val2;
                    unset($this->pathsFound[$key2]);
                }
            }
        }

        $this->pathsFound = $ordered + $this->pathsFound;
    }
}