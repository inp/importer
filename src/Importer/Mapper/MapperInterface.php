<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 11/04/13
 * Time: 12:23 PM
 *
 */

namespace Importer\Mapper;

/**
 * Class MapperInterface
 *
 * Common interface for the Mapper classes.
 * @package Mapper
 */
interface MapperInterface
{
    /**
     * @param $parser mixed The parser object implementing the parse() object used to read the map source.
     */
    public function setParser($parser);

    /**
     * @param $source mixed An identifier to tell the parser where (or what) is the map source.
     */
    public function setMapSource($source);

    /**
     * @param $toMap mixed The set of elements to be mapped.
     * @return mixed A "map" associating each element of the set with its corresponding path.
     */
    public function getPaths($toMap);
}