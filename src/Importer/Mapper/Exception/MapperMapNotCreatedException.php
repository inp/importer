<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 04:52 PM
 *
 */

namespace Importer\Mapper\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class MapperMapNotCreatedException
 *
 * Exception when the Mapper can't create a map.
 *
 * @package Importer\Mapper\Exception
 */
class MapperMapNotCreatedException extends TranslatedImporterException
{
}