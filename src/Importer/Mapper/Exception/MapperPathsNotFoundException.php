<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 11/04/13
 * Time: 04:37 PM
 *
 */

namespace Importer\Mapper\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class MapperPathsNotFoundException
 *
 * Thrown when the Mapper cannot associate an identifier with a property.
 *
 * @package Mapper\Exception
 */
class MapperPathsNotFoundException extends TranslatedImporterException
{

    /**
     * @return mixed The set of paths to properties that could not be mapped.
     */
    public function getPathsNotFound()
    {
        return $this->getParams();
    }

    public function __toString()
    {
        return $this->getMessage() . ' : ' . join(' ', $this->getParams());
    }
}