<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 05:46 PM
 *
 */

namespace Importer\Mapper\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class MapperIdMappingException
 *
 * Exception for a generic error while mapping Id's.
 *
 * @package Importer\Mapper\Exception
 */
class MapperIdMappingException extends TranslatedImporterException
{

}