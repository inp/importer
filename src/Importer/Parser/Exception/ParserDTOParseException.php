<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 06:07 PM
 *
 */

namespace Importer\Parser\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class ParserDTOParseException
 *
 * Exception for Errors while parsing a DTO object.
 *
 * @package Importer\Parser\Exception
 */
class ParserDTOParseException extends TranslatedImporterException
{

}