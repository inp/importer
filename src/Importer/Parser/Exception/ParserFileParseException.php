<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 05:07 PM
 *
 */

namespace Importer\Parser\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class ParserFileParseException
 *
 * Exception when a map source file can't be parsed.
 *
 * @package Importer\Parser\Exception
 */
class ParserFileParseException extends TranslatedImporterException
{

}