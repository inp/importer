<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 06:01 PM
 *
 */

namespace Importer\Parser\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class ParserRowParseException
 *
 * Exception when an error happens while parsing a row.
 *
 * @package Importer\Parser\Exception
 */
class ParserRowParseException extends TranslatedImporterException
{

}