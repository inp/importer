<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 18/04/13
 * Time: 10:58 AM
 *
 */

namespace Importer\Parser;

/**
 * Class ParserInterface
 *
 * Common Parser interface to parsing an attribute set into a different model.
 * @package Parser
 */
interface ParserInterface
{
    /**
     * @param $toParse mixed The data to be parsed.
     * @return mixed The desired model representation.
     */
    public function parse($toParse);

    /**
     * @return array An array of object with information about potential errors.
     */
    public function getErrors();
}