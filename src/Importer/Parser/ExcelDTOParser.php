<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 18/04/13
 * Time: 11:02 AM
 *
 */

namespace Importer\Parser;

use Importer\Builder\DirectorInterface;
use Importer\Event\AbstractNotifier;
use Importer\Event\ImporterEvent;
use Importer\Parser\Exception\ParserDTOParseException;
use Importer\Parser\Exception\ParserRowParseException;
use Importer\Parser\ParserInterface;
use Importer\Builder\Exception\ModelConstructionDirectorObjectNotBuiltException;
use Importer\DTO\ExcelImporterDTO;
use Exception;
use Importer\Iterator\GenericArrayIterator;
use Importer\Mapper\MapperInterface;
use Importer\Processor\ProcessorInterface;
use Importer\Util\ImportedDrawing;
use PHPExcel_Worksheet;

/**
 * Class ExcelDTOParser
 *
 * A Parser for a ExcelDTO object
 * @package Parser
 */
class ExcelDTOParser extends AbstractNotifier implements ParserInterface
{
    /**
     * @var MapperInterface The Mapper used by this Parser.
     */
    protected $mapper;

    /**
     * @var DirectorInterface The Director used to construct the resulting model.
     */
    protected $director;

    /**
     * @var array Container for LineErrorDTO.
     */
    protected $lineError;

    /**
     * @var ProcessorInterface Processor object for parsed rows.
     */
    protected $parsedRowProcessor;

    /**
     * @var \PHPExcel_Worksheet_BaseDrawing[] drawings indexed by coordinate
     */
    private $drawingCollection;

    public function __construct(MapperInterface $mapper, DirectorInterface $director, ProcessorInterface $parsedRowProcessor = null)
    {
        $this->director = $director;
        $this->mapper = $mapper;
        $this->parsedRowProcessor = $parsedRowProcessor;
    }

    /**
     * This method parses one row form the provided Worksheet to create its model representation
     * @param int $i The row index
     * @param $pathMap IdentifierInterface[] An associative array with the mapped paths.
     * @param $workSheet PHPExcel_Worksheet The worksheet to be parsed.
     * @param $fromCol int The initial column index.
     * @param $toCol int The ending column index (inclusive).
     * @param $header
     * @return array
     * @throws ModelConstructionDirectorObjectNotBuiltException
     * @throws ParserRowParseException
     */
    protected function parseRowFromWorksheet($i, &$pathMap, $workSheet, $fromCol, $toCol, $header)
    {

        try {
            $params = [];
            $rowModel = [];

            $this->director->startNewObject();

            foreach ($pathMap as $key => $val) {

                if (array_key_exists($key, $header)) {

                    $params['title'] = $key;
                    $params['identifier'] = $val;
                    $params['data'] = null;

                    if ($val[0]->hasAttribute('data_type') && $val[0]->getAttribute('data_type') == 'drawing') {
                        $coordinate = \PHPExcel_Cell::stringFromColumnIndex(($fromCol - 1) + intval($header[$key])) . $i;
                        if (isset($this->drawingCollection[$coordinate]))
                            $params['data'] = new ImportedDrawing($this->drawingCollection[$coordinate]);

                    } else {
                        $params['data'] = $workSheet->getCellByColumnAndRow(($fromCol - 1) + intval($header[$key]), $i)->getFormattedValue();
                    }

                    try {

                        $cellModel = $this->director->construct($params);
                        if (!empty($cellModel) && !empty($cellModel[0]) && !empty($cellModel[1])) {
                            $rowModel[$cellModel[0]] = $cellModel[1];
                            $this->notifyEvent(new ImporterEvent($cellModel, 'row_model_built', ['row' => $i, 'params' => $params]));
                        }

                    } catch (ModelConstructionDirectorObjectNotBuiltException $mcdonbe) {

                        // If this model could not be created, it will be added to the current list's wrong cells.
                        if (!isset($this->lineError[$i])) {

                            $this->lineError[$i] = new ExcelDTOParserLineErrorDTO();
                            $this->lineError[$i]->setRowNumber($i);
                            $this->lineError[$i]->setWorkSheetName($workSheet->getTitle());
                            $this->lineError[$i]->addWrongCell($mcdonbe);

                        }

                        $this->notifyEvent(new ImporterEvent($this->lineError[$i], 'failed_row_model_build', ['row' => $i, 'params' => $params]));

                        // And the exception is thrown again to end this row processing
                        throw $mcdonbe;
                    }
                }
            }

            return $rowModel;
        } catch (ModelConstructionDirectorObjectNotBuiltException $mce) {

            throw $mce;
        } catch (Exception $e) {

            throw new ParserRowParseException('Error while trying to parse row %row%.', $e->getCode(), $e, $i, ['%row%' => $i]);
        } finally {

            $this->director->finishObject();

        }
    }

    /**
     * @param $dto ExcelImporterDTO The DTO to be parsed.
     * @return mixed The desired model representation.
     * @throws ParserDTOParseException
     */
    public function parse($dto)
    {
        try {
            $model = [];
            $this->lineError = [];

            // Fist get header, data range and the pathmap
            $header = $dto->getHeaderIdentifiers();
            $pathMap = $this->mapper->getPaths($header);
            $dataRange = $dto->getDataRange();
            $header = array_flip($header);

            // Second, build the parameters
            $fromColIdx = \PHPExcel_Cell::columnIndexFromString($dataRange['highest']['col']);
            $toColIdx = \PHPExcel_Cell::columnIndexFromString($dataRange['lowest']['col']);

            $fromRowIdx = $dataRange['highest']['row'] + 1;
            $toRowIdx = $dataRange['lowest']['row'];

            $this->setupDrawingCollection($dto->getSheet());
            // Third, parse each row.
            for ($i = $fromRowIdx; $i <= $toRowIdx; ++$i) {

                try {
                    $row = $this->parseRowFromWorksheet($i, $pathMap, $dto->getSheet(), $fromColIdx, $toColIdx, $header);
                    $model[] = (isset($this->parsedRowProcessor) ? $this->parsedRowProcessor->process($row) : $row);

                    $this->notifyEvent(new ImporterEvent($row, 'parsed_row', ['row' => $i, 'sheet' => $dto->getSheet(), 'header' => $header, 'fromCol' => $fromColIdx, 'toCol' => $toColIdx]));

                } catch (Exception $e) {

                    // If this model could not be created, it will be added to the current list's wrong rows.
                    if (!isset($this->lineError[$i])) {

                        $this->lineError[$i] = new ExcelDTOParserLineErrorDTO();
                        $this->lineError[$i]->setRowNumber($i);
                        $this->lineError[$i]->setWorkSheetName($dto->getSheet()->getTitle());
                    }

                    $this->lineError[$i]->setException($e);
                    $this->notifyEvent(new ImporterEvent($this->lineError[$i], 'failed_parsed_row', ['row' => $i, 'sheet' => $dto->getSheet(), 'header' => $header, 'fromCol' => $fromColIdx, 'toCol' => $toColIdx]));

                    continue;
                }
            }

            return $model;
        } catch (Exception $e) {

            throw new ParserDTOParseException('Error while trying to parse the DTO.', $e->getCode(), $e);
        }
    }

    /**
     * @return array A collection with information about potential errors.
     */
    public function getErrors()
    {
        return $this->lineError;
    }

    /**
     * Setups a collection indexed by the coordinates of the drawings for easy findings in the parsing stage
     * @param $workSheet \PHPExcel_Worksheet
     */
    private function setupDrawingCollection($workSheet)
    {
        $collection = $workSheet->getDrawingCollection();
        $this->drawingCollection = [];
        foreach ($collection as $drawing) {
            $coordinates = $drawing->getCoordinates();
            $this->drawingCollection[$coordinates] = $drawing;
        }

    }
}