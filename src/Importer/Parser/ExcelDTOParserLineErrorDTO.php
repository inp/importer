<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 18/06/13
 * Time: 03:04 PM
 *
 */

namespace Importer\Parser;

/**
 * Class ExcelDTOParserLineErrorDTO
 *
 * Class for storing the information about error trying to parse a worksheet line.
 *
 * @package Importer\Parser
 */
class ExcelDTOParserLineErrorDTO implements ParserErrorDTOInterface
{
    /**
     * @var int The number of the row that went "wrong".
     */
    protected $rowNumber;

    /**
     * @var string The Name of the Worksheet currently being parsed.
     */
    protected $workSheetName;

    /**
     * @var array Container for ModelConstructionDirectorObjectNotBuiltException objects
     */
    protected $wrongCells;

    /**
     * @var mixed Additional error information
     */
    protected $exception;

    /**
     * @param int $rowNumber
     */
    public function setRowNumber($rowNumber)
    {
        $this->rowNumber = $rowNumber;
    }

    /**
     * @return int
     */
    public function getRowNumber()
    {
        return $this->rowNumber;
    }

    /**
     * @param array $wrongCells
     */
    public function setWrongCells($wrongCells)
    {
        $this->wrongCells = $wrongCells;
    }

    /**
     * @return array
     */
    public function getWrongCells()
    {
        return $this->wrongCells;
    }

    /**
     * Method for adding a wrong cell to this error DTO.
     * @param $cell mixed The wrong cell.
     */
    public function addWrongCell($cell)
    {
        $cell->xdebug_message = null;
        $this->wrongCells[] = $cell;
    }

    /**
     * @param $exception mixed an Exception object or any other object with additional error information.
     */
    public function setException($exception)
    {
        $this->exception = $exception;
    }

    /**
     * @return mixed The object containing addition information about the error.
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param string $workSheetName
     */
    public function setWorkSheetName($workSheetName)
    {
        $this->workSheetName = $workSheetName;
    }

    /**
     * @return string
     */
    public function getWorkSheetName()
    {
        return $this->workSheetName;
    }

    /**
     * @return mixed A container with all the information in the DTO object.
     */
    public function getData()
    {
        return array(
            'row_number' => $this->rowNumber,
            'wrong_cells' => $this->wrongCells,
            'exception' => $this->exception
        );
    }
}