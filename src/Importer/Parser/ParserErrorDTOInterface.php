<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 18/06/13
 * Time: 02:56 PM
 *
 */

namespace Importer\Parser;

use Importer\DTO\ErrorDTOInterface;

/**
 * Class ParserErrorDTOInterface
 *
 * Common interface for cbjects with error information for the Parsers.
 *
 * @package Importer\Parser
 */
interface ParserErrorDTOInterface extends ErrorDTOInterface
{

}