<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 26/04/13
 * Time: 08:37 AM
 *
 */

namespace Importer\Parser;

use Exception;
use Importer\Parser\Exception\ParserFileParseException;
use Symfony\Component\Yaml\Parser;

/**
 * Class YamlFileParser
 *
 * A Yaml File Parser based on the Symfony Yaml Parser object
 * @package Parser
 */
class YamlFileParser implements ParserInterface
{

    protected $parser;

    public function __construct()
    {
        $this->parser = new Parser();
    }

    /**
     * @param $toParse string The file to be parsed.
     * @return mixed An associative array with file content.
     */
    public function parse($toParse)
    {
        try {

            return $this->parser->parse(file_get_contents($toParse));

        } catch (\Exception $e) {
            throw new ParserFileParseException('Error while trying to parse source yml file.', $e->getCode(
            ), $e, $toParse);
        }
    }

    public function getErrors()
    {
        return null;
    }
}