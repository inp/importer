<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 12/04/13
 * Time: 08:43 AM
 *
 */

namespace Importer\DTO;

use PHPExcel_Worksheet;

/**
 * Class ExcelImporterDTO
 *
 * A DTO for passing around data processed in the ExcelImporter.
 * @package DTO
 */
class ExcelImporterDTO implements DTOInterface
{
    /**
     * @var PHPExcel_WorkSheet The obtained worksheet.
     */
    protected $sheet;

    /**
     * @var array The data range over the worksheet where we want to look for data.
     */
    protected $dataRange;

    /**
     * @var array A list of identifier strings.
     */
    protected $headerIdentifiers;

    /**
     * A constructor with optional arguments for creating the DTO.
     * @param PHPExcel_Worksheet $sheet
     * @param null $dataRange
     * @param null $headerIdentifiers
     */
    public function __construct(PHPExcel_WorkSheet $sheet = null, $dataRange = null, $headerIdentifiers = null)
    {
        $this->sheet = $sheet;
        $this->dataRange = $dataRange;
        $this->headerIdentifiers = $headerIdentifiers;
    }

    /**
     * @param array $dataRange
     */
    public function setDataRange($dataRange)
    {
        $this->dataRange = $dataRange;
    }

    /**
     * @return array
     */
    public function getDataRange()
    {
        return $this->dataRange;
    }

    /**
     * @param array $headerIdentifiers
     */
    public function setHeaderIdentifiers($headerIdentifiers)
    {
        $this->headerIdentifiers = $headerIdentifiers;
    }

    /**
     * @return array
     */
    public function getHeaderIdentifiers()
    {
        return $this->headerIdentifiers;
    }

    /**
     * @param \PHPExcel_Worksheet $sheet
     */
    public function setSheet($sheet)
    {
        $this->sheet = $sheet;
    }

    /**
     * @return \PHPExcel_Worksheet
     */
    public function getSheet()
    {
        return $this->sheet;
    }

    /**
     * @return mixed A container with all the information in the DTO object.
     */
    public function getData()
    {
        return [
            'sheet' => $this->sheet,
            'data_range' => $this->dataRange,
            'header_identifiers' => $this->headerIdentifiers
        ];
    }
}