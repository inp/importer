<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 12/04/13
 * Time: 08:42 AM
 *
 */

namespace Importer\DTO;

/**
 * Class DTOInterface
 *
 * Common interface for DTOs
 * @package DTO
 */
interface DTOInterface
{
    /**
     * @return mixed A container with all the information in the DTO object.
     */
    public function getData();
}