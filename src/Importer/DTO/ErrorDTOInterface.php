<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 20/06/13
 * Time: 11:02 AM
 *
 */

namespace Importer\DTO;

/**
 * Class ErrorDTOInterface
 *
 * An extended DTO Interface for storing information about an error.
 *
 * @package Importer\DTO
 */
interface ErrorDTOInterface extends DTOInterface
{

}