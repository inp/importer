<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 5/04/13
 * Time: 04:15 PM
 *
 */

namespace Importer\Predicate;

use Importer\Predicate\Exception\PredicateInexistentFileException;

/**
 * Class FileExistsPredicate
 *
 * Simple predicate to define if a file exists based on its name. True if it does or returns an exception if it dosen´t.
 * @package Predicate
 */
class FileExistsPredicate extends AbstractPredicate
{

    protected function processAnswer($ans)
    {
        if (!$ans) {

            throw new PredicateInexistentFileException("File doesn't exist.", E_ERROR, null);
        }

        parent::processAnswer($ans);
    }

    protected function test($filename)
    {

        return file_exists($filename);
    }
}