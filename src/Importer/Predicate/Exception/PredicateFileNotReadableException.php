<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 12:16 PM
 *
 */

namespace Importer\Predicate\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class PredicateFileNotReadableException
 *
 * Exception thrown in case a file is not readable.
 *
 * @package Importer\Importer\Exception
 */
class PredicateFileNotReadableException extends TranslatedImporterException
{

}