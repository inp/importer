<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 01:02 PM
 *
 */

namespace Importer\Predicate\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class PredicateEvaluationFailedException
 *
 * Exception thrown in case the predicate's evaluation gives a negative result.
 *
 * @package Importer\Importer\Exception
 */
class PredicateEvaluationFailedException extends TranslatedImporterException
{

}