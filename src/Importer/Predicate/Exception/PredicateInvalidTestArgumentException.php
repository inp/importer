<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 12:52 PM
 *
 */

namespace Importer\Predicate\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class PredicateInvalidTestArgumentException
 *
 * Exception thrown in case the predicate evaluates a wrong type parameter.
 *
 * @package Importer\Importer\Exception
 */
class PredicateInvalidTestArgumentException extends TranslatedImporterException
{

}