<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 12:31 PM
 *
 */

namespace Importer\Predicate\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class PredicateInexistentFileException
 *
 * Exception thrown in case a file does not exist.
 *
 * @package Importer\Importer\Exception
 */
class PredicateInexistentFileException extends TranslatedImporterException
{

}