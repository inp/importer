<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 12:10 PM
 *
 */

namespace Importer\Predicate\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class PredicateInvalidFileException
 *
 * Exception thrown in case of an Invalid file.
 *
 * @package Importer\Importer\Exception
 */
class PredicateInvalidFileException extends TranslatedImporterException
{

}