<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 5/04/13
 * Time: 02:55 PM
 *
 */

namespace Importer\Predicate;

/**
 * Class AbstractPredicate
 *
 * Abstract definition for a Predicate evaluating if certain condition (or conditions) follow for a value. It has
 * a Composite structure, so many Predicate can be nested and evaluated one after (or before) other.
 *
 * @package Predicate
 */
abstract class AbstractPredicate
{
    /**
     * @var AbstractPredicate[]
     */
    protected $leafPredicates = [];
    protected $answer = null;

    /**
     * This method prepares the answer returned by this predicate and can be reimplemented to do any desired processing
     * over the set of answers comming from the test methods performed by this predicate and leaf predicates.
     * @param $ans mixed The answer of a simple test performed on a value
     */
    protected function processAnswer($ans)
    {

        if ($this->answer !== null) {

            $this->answer = ($this->answer && $ans);
        } else {
            $this->answer = $ans;
        }

    }

    /**
     * This abstract method must be implemented with the evaluation performed over the value.
     * @param $val mixed The value to be evaluated.
     * @return mixed The result of this evaluation over value
     */
    abstract protected function test($val);

    /**
     * Evaluates the value by this predicate and its children predicate, preparing and returning a result based on those
     * evaluations.
     * @param $val mixed The value to be evaluated by this predicate and nested predicates.
     * @return mixed A value representing the answer or aswers of the evaluation
     */
    public function evaluate($val)
    {

        // Remember to ALWAYS clean your answer first each time you use this method...or you could suffer unexpected
        // consecuences (unless you know exactly what are you doing :) )
        $this->answer = null;
        $this->processAnswer($this->test($val));

        foreach ($this->leafPredicates as $predicate) {

            $this->processAnswer($predicate->evaluate($val));
        }

        return $this->answer;
    }

    /**
     * Method to add LeafPredicates to this predicate
     * @param AbstractPredicate $p Leaf predicate to add
     */
    public function addPredicate(AbstractPredicate $p)
    {
        $this->leafPredicates[] = $p;
    }

}