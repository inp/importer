<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 8/04/13
 * Time: 03:49 PM
 *
 */

namespace Importer\Predicate;

/**
 * Class CellIsErasablePredicate
 *
 * A composite predicate to evaluate is a spreadsheet cell is a candidate to be erased.
 * @package Predicate
 */
class CellIsErasablePredicate extends AbstractPredicate
{

    /**
     * For now, the only criteria for removing a cell is a blank or empty content.
     */
    public function __construct()
    {

        $this->addPredicate(new CellIsEmptyPredicate());
    }

    /**
     * This method must be implemented with the evaluation performed over the value.
     * @param $val mixed The value to be evaluated.
     * @return mixed The result of this evaluation over value
     */
    protected function test($cell)
    {
        return true;
    }
}