<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 8/04/13
 * Time: 03:52 PM
 *
 */

namespace Importer\Predicate;

use Importer\Predicate\Exception\PredicateInvalidTestArgumentException;
use PHPExcel_Cell;

/**
 * Class CellIsEmptyPredicate
 *
 * A simple predicate to test if a spreadsheet cell is empty
 * @package Predicate
 */
class CellIsEmptyPredicate extends AbstractPredicate
{

    /**
     * @var string Regular Expression of characters considered "blanks" as seen in
     * http://stackoverflow.com/questions/4166896/trim-unicode-whitespace-in-php-5-2
     */
    protected $blankChars = '/^[\pZ\pC]+|[\pZ\pC]+$/u';

    /**
     * @param string $blanks An optional regular expression of character blanks
     */
    public function __construct($blanks = null)
    {
        if ($blanks != null) {

            $this->blankChars = $blanks;
        }
    }

    /**
     * This method returns true if a PHPExcel_Cell object represents an empty worksheet cell.
     * @param $val mixed The cell to be evaluated, must be a PHPExcel_Cell instance.
     * @return bool True is $cell represents an empty cell.
     */
    protected function test($cell)
    {
        if (!($cell instanceof PHPExcel_Cell)) {

            throw new PredicateInvalidTestArgumentException('Parameter should be a PHPExcel_Cell instance.', E_ERROR, null);
        }

        return $cell == null || $cell->getValue() == '' || preg_replace(
            $this->getBlankChars(),
            '',
            $cell->getValue()
        ) == '';
    }

    /**
     * Accessor to the blank char regular expression defined for this Predicate
     * @return null|string
     */
    public function getBlankChars()
    {

        return $this->blankChars;
    }
}