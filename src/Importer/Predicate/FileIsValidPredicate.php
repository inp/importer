<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 5/04/13
 * Time: 04:41 PM
 *
 */

namespace Importer\Predicate;

use Exception;
use Importer\Predicate\Exception\PredicateInvalidFileException;

/**
 * Class FileIsValidPredicate
 *
 * A composite validator to test if an input file is valid for reading by an Importer.
 * @package Predicate
 */
class FileIsValidPredicate extends AbstractPredicate
{
    /**
     * @param mixed $ans
     * @throws \Exception
     */
    protected function processAnswer($ans)
    {
        try {

            parent::processAnswer($ans);

        } catch (\Exception $e) {

            throw new PredicateInvalidFileException('Invalid file.', $e->getCode(), $e);
        }
    }

    function __construct()
    {
        $this->addPredicate(new FileExistsPredicate());
        $this->addPredicate(new FileIsReadablePredicate());
    }

    protected function test($val)
    {
        return true;
    }
}