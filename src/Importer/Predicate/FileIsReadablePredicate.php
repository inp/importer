<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 5/04/13
 * Time: 04:34 PM
 *
 */

namespace Importer\Predicate;

use Importer\Predicate\Exception\PredicateFileNotReadableException;

/**
 * Class FileIsReadablePredicate
 *
 * Simple predicate to test if a file is readable and legible. True if it does or returns an exception if it doesn't.
 * @package Predicate
 */
class FileIsReadablePredicate extends AbstractPredicate
{

    protected function processAnswer($ans)
    {
        if (!$ans) {

            throw new PredicateFileNotReadableException("File isn't readable.", E_ERROR, null);
        }

        parent::processAnswer($ans);
    }


    protected function test($filename)
    {
        return is_readable($filename);
    }
}