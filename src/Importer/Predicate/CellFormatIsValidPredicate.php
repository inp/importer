<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 8/04/13
 * Time: 09:44 AM
 *
 */

namespace Importer\Predicate;

use Importer\Predicate\Exception\PredicateEvaluationFailedException;

/**
 * Class CellIsValidPredicate
 *
 * A simple predicate to test if a string has a valid spreadsheet cell format.
 *
 * @package Predicate
 */
class CellFormatIsValidPredicate extends AbstractPredicate
{

    /**
     * @var string Default pattern for the regular expression (in PCRE format).
     */
    protected $regExp = '/^[A-Z]+[1-9]+[0-9]*$/';

    protected function processAnswer($ans)
    {
        // 1 means TRUE, 0 means FALSE
        if ($ans === 1 || $ans === 0) {

            parent::processAnswer(((bool)$ans));
        } else {

            throw new PredicateEvaluationFailedException("Regular expression evaluation error.", E_WARNING, null);
        }
    }

    /**
     * This constructor may accept a new PCRE pattern for the regular expression evaluating cell patterns.
     * @param string $regExp
     */
    function __construct($regExp = null)
    {

        if (isset($regExp)) {

            $this->regExp = $regExp;
        }
    }

    protected function test($val)
    {
        return preg_match($this->regExp, $val);
    }

    /**
     * An accessor to the regular expression string for this Predicate
     * @return null|string
     */
    public function getRegExp()
    {

        return $this->regExp;
    }


}