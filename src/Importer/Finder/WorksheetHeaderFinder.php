<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 11/04/13
 * Time: 09:04 AM
 *
 */

namespace Importer\Finder;

use Exception;
use Importer\Finder\Exception\FinderInvalidFinderSetException;
use PHPExcel_Worksheet;

/**
 * Class WorksheetHeaderFinder
 *
 * Finder class for looking in a worksheet for a row representing a "header", a set of "field names".
 * @package Finder
 */
class WorksheetHeaderFinder implements FinderInterface
{

    /**
     * @var int The row position where the "header" is located.
     */
    protected $rowPos;

    public function  __construct()
    {
        $this->rowPos = 1;
    }

    /**
     * @param PHPExcel_WorkSheet $worksheet The worksheet where we want to find the header.
     * @return array The header content.
     * @throws \Exception
     */
    public function findElements($worksheet = null)
    {
        if (!($worksheet instanceof PHPExcel_WorkSheet)) {

            throw new FinderInvalidFinderSetException('Parameter should be a PHPExcel_WorkSheet instance.', E_WARNING, null);
        }

        $header = array();
        $found = false;

        // First find the requested row, later extract its cells and finally store their names.
        foreach ($worksheet->getRowIterator() as $row) {

            if ($row->getRowIndex() == $this->rowPos) {

                $found = true;
                foreach ($row->getCellIterator() as $cell) {
                    if ($cell->getValue()) {
                        $header[] = $cell->getValue();
                    }
                }
            }

            if ($found) {

                break;
            }
        }

        return $header;
    }

    /**
     * Sets the row where the header can be found.
     * @param int $rowPos The position of the row.
     */
    public function setCriteria($rowPos = 1)
    {
        $this->rowPos = $rowPos;
    }

    /**
     * @return int The current header row position.
     */
    public function getCriteria()
    {
        return $this->rowPos;
    }

    /**
     * @return mixed The set of configuration options used to configure this
     * finder
     */
    public function getCallerInfo()
    {
        return [
            'row_pos' => $this->rowPos
        ];
    }
}