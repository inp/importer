<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 11/04/13
 * Time: 09:01 AM
 *
 */

namespace Importer\Finder;

/**
 * Class FinderInterface
 *
 * Common interface for Finder classes that looks for a set of elements matching a criteria.
 * @package Finder
 */
interface FinderInterface
{

    /**
     * @param $criteria mixed The criteria of the elements we are looking for
     */
    public function setCriteria($criteria = null);

    /**
     * @param $set mixed The element set where we want to search.
     * @return mixed The set of elements matching the criteria previously defined.
     */
    public function findElements($set = null);

    /**
     * @return mixed The set of configuration options used to configure this
     * finder
     */
    public function getCallerInfo();
}