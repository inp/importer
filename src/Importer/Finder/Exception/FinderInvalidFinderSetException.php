<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 07:08 PM
 *
 */

namespace Importer\Finder\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class FinderInvalidFinderSetException
 *
 * Exception for Invalid find method arguments.
 *
 * @package Importer\Finder\Exception
 */
class FinderInvalidFinderSetException extends TranslatedImporterException
{

}