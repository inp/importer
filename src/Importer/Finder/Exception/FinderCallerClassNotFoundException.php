<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 07:27 PM
 *
 */

namespace Importer\Finder\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class FinderCallerClassNotFoundException
 *
 * Exception for Invalid classes instances used for the Finder.
 *
 * @package Importer\Finder\Exception
 */
class FinderCallerClassNotFoundException extends TranslatedImporterException
{

}