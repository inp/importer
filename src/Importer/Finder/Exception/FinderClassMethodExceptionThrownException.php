<?php
/**
 * Created by PhpStorm.
 * User: juanrobles
 * Date: 12/09/17
 * Time: 11:49 AM
 */

namespace Importer\Finder\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class FinderClassMethodExceptionThrownException
 *
 * Exception for cases where the finder function throws an exception
 *
 * @package Importer\Finder\Exception
 */
class FinderClassMethodExceptionThrownException extends TranslatedImporterException
{

}