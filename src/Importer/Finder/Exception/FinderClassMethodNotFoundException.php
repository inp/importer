<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 08:01 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Importer\Finder\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class FinderClassMethodNotFoundException
 *
 * Exception for class methods that could not be found.
 *
 * @package Importer\Finder\Exception
 */
class FinderClassMethodNotFoundException extends TranslatedImporterException
{

}