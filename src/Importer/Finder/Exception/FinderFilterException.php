<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 07:38 PM
 *
 */

namespace Importer\Finder\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class FinderFilterException
 *
 * Exception for errors while trying to filter the result from a find process.
 *
 * @package Importer\Finder\Exception
 */
class FinderFilterException extends TranslatedImporterException
{

}