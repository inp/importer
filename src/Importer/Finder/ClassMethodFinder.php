<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 29/04/13
 * Time: 08:54 AM
 *
 */

namespace Importer\Finder;

use Exception;
use Importer\Finder\Exception\FinderCallerClassNotFoundException;
use Importer\Finder\Exception\FinderClassMethodExceptionThrownException;
use Importer\Finder\Exception\FinderClassMethodNotFoundException;
use Importer\Finder\Exception\FinderFilterException;
use Importer\Predicate\AbstractPredicate;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class ClassMethodFinder
 *
 * A Finder for retrieving objects based on certain criteria, using a "findBy" method provided by certain class.
 * @package Finder
 */
class ClassMethodFinder implements FinderInterface
{
    /**
     * @var string|object The name or instance of the class.
     */
    protected $class;

    /**
     * @var string The name of the "finder" method.
     */
    protected $method;

    /**
     * @var boolean Is the finder method static?
     */
    protected $isStatic;

    /**
     * @var AbstractPredicate The filter criteria "if any"
     */
    protected $criteria;

    /**
     * @var Container An optional container in case the finder object must be taken from a service container
     */
    protected $container;

    /**
     * @param $class
     * @param $method
     * @param bool $static
     * @param $container
     */

    public function __construct($class = null, $method = null, $static = true, $container = null)
    {
        $this->class = $class;
        $this->method = $method;
        $this->isStatic = $static;
        $this->container = $container;
    }

    /**
     * @param $criteria AbstractPredicate The criteria of the elements we are looking for
     */
    public function setCriteria($criteria = null)
    {
        $this->criteria = $criteria;
    }

    /**
     * @param $results array The result set to be filtered
     * @return array The filtered results
     * @throws FinderFilterException
     */
    protected function filterResults($results)
    {
        try {
            $results = is_array($results) ? $results : (isset($results) ? [$results] : []);
            $toFilter = [];

            if (isset($this->criteria)) {

                $t = count($results);
                for ($i = 0; $i < $t; ++$i) {

                    if ($this->criteria->evaluate($results{$i})) {

                        $toFilter[] = $results{$i};
                    }
                }

                return $toFilter;
            } else {

                return $results;
            }

        } catch (Exception $e) {

            throw new FinderFilterException('It was not possible to filter the Finder results.', $e->getCode(), $e);
        }
    }

    /**
     * Method for choosing the finder method invoker.
     * @return string|mixed The invoker object.
     * @throws \Exception
     */
    protected function getCaller()
    {
        try {

            $caller = $this->class;

            if (is_string($caller)) {

                // If it's a service
                if ($caller[0] == '@') {

                    return $this->container->get(substr($caller, 1));
                }

                // If it's an static method call or an instance method call
                if ($this->isStatic) {

                    return $caller;
                } else {

                    return new $caller();
                }

            } else {

                // Any other option (an instance object).
                return $caller;
            }

        } catch (Exception $e) {

            $myInfo = $this->getCallerInfo();

            throw new FinderCallerClassNotFoundException('Could not instantiate the caller : %caller%.', $e->getCode(),
                $e, null, ['%caller%' => $myInfo['class']]);
        }
    }

    /**
     * @param $set array An ordered list with the parameters provided to the "finder" method.
     * @return array The set of elements returned by the "finder" method. It will always be a container (an array) even
     * if its size is 0, 1 or greater.
     * @throws FinderClassMethodExceptionThrownException
     * @throws FinderClassMethodNotFoundException
     */
    public function findElements($set = null)
    {

        // Object given or class name given ? Static method call or instance method call?
        $caller = $this->getCaller();
        $myInfo = $this->getCallerInfo();

        if (!method_exists($caller, $this->method)) {

            throw new FinderClassMethodNotFoundException(
                'The method %finder_method% from the caller class %caller_class% could not be called.', E_ERROR, null,
                null, ['%finder_method%' => $myInfo['method'], '%caller_class%'  => $myInfo['class']]
            );
        }

        try {

            $results = call_user_func_array([$caller, $this->method], $set);
        } catch (Exception $e) {

            $msg = $e->getMessage();
            throw new FinderClassMethodExceptionThrownException(
                'The method %finder_method% from the caller class %caller_class% threw an exception : %msg%.', E_ERROR,
                null, null, ['%finder_method%' => $myInfo['method'], '%caller_class%'  => $myInfo['class'], '%msg%' => $msg]
            );
        }

        // Filtering the results
        return $this->filterResults($results);
    }

    public function getClass()
    {
        return $this->class;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function isStaticMethodCall()
    {
        return $this->isStatic;
    }

    /**
     * Method for getting information about this Finder.
     * @return array An associative array with this finder's info.
     */
    public function getCallerInfo()
    {
        return [
            'class'     => is_string($this->class) ? $this->class : get_class($this->class),
            'method'    => $this->method,
            'is_static' => $this->isStatic
        ];

    }

    public function setContainer($container)
    {
        $this->container = $container;
    }
}