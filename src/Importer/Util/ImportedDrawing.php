<?php
/**
 * INP All rights reserved.
 * User: Ivan
 * Date: 5/7/2015
 * Time: 6:22 PM
 */

namespace Importer\Util;

use Importer\Importer\Exception\ImporterProcessException;

/**
 * Class ImportedDrawing represents an imported \PHPExcel_Worksheet_BaseDrawing with utility methods
 * to save the image to a folder and get its filename
 * @package Importer\Util
 */
class ImportedDrawing
{

    /** @var  \PHPExcel_Worksheet_BaseDrawing */
    protected $drawing;

    function __construct($drawing)
    {
        $this->drawing = $drawing;
    }

    /**
     * @return \PHPExcel_Worksheet_BaseDrawing
     */
    public function getDrawing()
    {
        return $this->drawing;
    }

    /**
     * @return string the filename from PHPExcel indexed filename
     * @throws ImporterProcessException
     */
    public function getFilename()
    {
        if ($this->drawing instanceof \PHPExcel_Worksheet_Drawing) {

            return $this->drawing->getIndexedFilename();

        } else if ($this->drawing instanceof \PHPExcel_Worksheet_MemoryDrawing) {

            return $this->drawing->getIndexedFilename();
        } else throw new ImporterProcessException('Unknown Drawing Type');
    }

    /**
     * Saves the drawing as a file
     * @param $path string folder to save the drawing to
     * @param null $filename filename to save the drawing or null to use the indexedFilename of PHPExcel
     * @return null|string
     * @throws \Importer\Importer\Exception\ImporterProcessException
     */
    public function saveToPath($path, $filename = null)
    {

        if (!is_dir($path))
            throw new ImporterProcessException('The specified path to save the drawing is not a directory.');

        if (!is_writable($path))
            throw new ImporterProcessException('The specified path to save the drawing is not a writable.');

        $filename = $path . DIRECTORY_SEPARATOR . ($filename ?: $this->getFilename());
        if ($this->drawing instanceof \PHPExcel_Worksheet_Drawing) {

            @copy($this->drawing->getPath(), $filename);

        } else if ($this->drawing instanceof \PHPExcel_Worksheet_MemoryDrawing) {

            // we have a memory drawing (case xls)
            $image = $this->drawing->getImageResource();

            // save image to disk
            $renderingFunction = $this->drawing->getRenderingFunction();

            switch ($renderingFunction) {

                case \PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG:
                    imagejpeg($image, $filename);
                    break;

                case \PHPExcel_Worksheet_MemoryDrawing::RENDERING_GIF:
                    imagegif($image, $filename);
                    break;

                case \PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG:
                case \PHPExcel_Worksheet_MemoryDrawing::RENDERING_DEFAULT:
                    imagepng($image, $filename);
                    break;
            }
        } else {
            throw new ImporterProcessException('Unknown Drawing Type');
        }

        return $filename;
    }

}