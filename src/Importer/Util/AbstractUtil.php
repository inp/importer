<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 8/04/13
 * Time: 11:18 AM
 *
 */

namespace Importer\Util;

/**
 * Class AbstractUtil
 *
 * An interface defining a set of utility classes (methods) to help other classes.
 *
 * @package Util
 */
interface AbstractUtil
{

}