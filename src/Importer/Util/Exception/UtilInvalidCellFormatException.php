<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 02:55 PM
 *
 */

namespace Importer\Util\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class UtilInvalidCellFormatException
 *
 * Exception thrown in case the evaluation of a cell can't be done because of an invalid format.
 *
 * @package Importer\Importer\Exception
 */
class UtilInvalidCellFormatException extends TranslatedImporterException
{

}