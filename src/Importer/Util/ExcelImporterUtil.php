<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 8/04/13
 * Time: 09:37 AM
 *
 */

namespace Importer\Util;

use Importer\Predicate\AbstractPredicate;
use Importer\Predicate\CellFormatIsValidPredicate;
use Importer\Util\Exception\UtilInvalidCellFormatException;

/**
 * Class ExcelImporterUtil
 *
 * A set of utility functions for the ExcelImporter Class.
 *
 * @package Util
 */
class ExcelImporterUtil extends ImporterUtil
{
    public static function columnToNumber($col, $toIndex = false)
    {

        return base_convert($col, 26, 10);
    }

    /**
     * This utility function takes a string representing a cell (a string of uppercase letters followed by a number) and
     * returns an associative array. Key 'row' is the row number >=1 and 'col' is the column letter >= 'A'.
     * @param $str String The String to be parsed.
     * @return array An associative array with the values of the cell parsed.
     */
    public static function stringToCell($str, AbstractPredicate $cellStringPredicate = null)
    {

        if (!isset($cellStringPredicate)) {

            $cellStringPredicate = new CellFormatIsValidPredicate();
        }

        if (!$cellStringPredicate->evaluate($str)) {

            throw new UtilInvalidCellFormatException('Invalid cell format.', E_ERROR, null);
        }

        $cell = ['row' => '', 'col' => ''];

        // We extract the column / row values assuming they are well formed
        if (is_string($str) && ctype_alpha($str{0})) {

            $t = strlen($str);
            for ($i = 0; $i < $t; ++$i) {

                if (ctype_alpha($str{$i})) {

                    $cell['col'] .= $str{$i};

                } else {
                    if (ctype_digit($str{$i})) {

                        $cell['row'] .= $str{$i};
                    }
                }
            }
        }

        return $cell;
    }

    /**
     * Parses an string representing a range from a worksheet and returns an associative array with the highest and
     * lowest cells form a range.
     * @param string $str  The string to be parsed
     * @param string $defSep A separator (':' by default).
     * @return array an associative array with both highest and lowest cells of the range.
     */
    public static function stringToRange($str, $defSep = ':')
    {
        $rangeStr = explode($defSep, $str);

        $dataDimension['highest'] = ExcelImporterUtil::stringToCell($rangeStr[0]);
        $dataDimension['lowest'] = ExcelImporterUtil::stringToCell($rangeStr[1]);

        return $dataDimension;
    }

    /**
     * Method for testing if a cell belongs to a range.
     * @param $cell array|string A representation of a cell.
     * @param $range array|string A representation of a range.
     * @return bool True if cell $cell belongs to the cell range $range.
     */
    public static function isCellInRange($cell, $range)
    {

        $cellData = null;
        $rangeData = null;
        // Type conversion (if needed)
        if (is_string($cell)) {

            $cellData = self::stringToCell($cell);
        }

        if (is_string($range)) {

            $rangeData = self::stringToRange($range);
        }

        return ($cellData['row'] >= $rangeData['highest']['row'] && $cellData['row'] <= $rangeData['lowest']['row']) &&
            (base_convert($cellData['col'], 26, 10) >= base_convert($rangeData['highest']['col'], 26, 10) &&
                base_convert($cellData['col'], 26, 10) <= base_convert($rangeData['lowest']['col'], 26, 10));

    }
}