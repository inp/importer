<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 8/04/13
 * Time: 09:35 AM
 *
 */

namespace Importer\Util;

/**
 * Class ImporterUtil
 *
 * A set of general utility methods for a generic Importer.
 *
 * @package Util
 */
class ImporterUtil implements AbstractUtil
{

}