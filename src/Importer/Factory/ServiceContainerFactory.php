<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 9/04/13
 * Time: 10:11 AM
 *
 */

namespace Importer\Factory;

use Exception;
use Importer\Extension\RowScopeExtension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;


/**
 * Class ServiceContainerFactory
 *
 * A simple factory to help creating and accessing a service container.
 * @package Factory
 */
class ServiceContainerFactory implements FactoryInterface
{
    protected static $container;
    protected static $path;
    protected static $file;

    /**
     * @param $params array An associative array with keys 'path' and 'file' for storing the service definition path and
     * file.
     * @return Container An instance from the Dependency Injection Container.
     * @throws \Exception
     */
    public static function create($params = null)
    {

        try {

            // If necessary, it will load the configuration file
            if (isset($params)) {

                if (!self::$container || $params['path'] != self::$path || $params['file'] != self::$file) {

                    self::$path = $params['path'];
                    self::$file = $params['file'];

                    self::$container = new ContainerBuilder();
                    self::$container->registerExtension(new RowScopeExtension());

                    $loader = new YamlFileLoader(self::$container, new FileLocator(self::$path));
                    $loader->load(self::$file);
                }
            }

            // Returning the instance
            return self::$container;
        } catch (Exception $e) {

            throw $e;
        }
    }
}