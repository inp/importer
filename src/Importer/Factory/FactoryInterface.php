<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 9/04/13
 * Time: 10:08 AM
 *
 */

namespace Importer\Factory;

/**
 * Class FactoryInterface
 *
 * Common Interface to Object Factories.
 *
 * @package Factory
 */
interface FactoryInterface
{

    public static function create($params);
}