<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 29/04/13
 * Time: 01:45 PM
 *
 */

namespace Importer\Factory;

use Importer\Builder\ReferenceBuilder;
use Importer\Builder\ServiceBuilder;
use Exception;
use Importer\Factory\Exception\FactoryInvalidBuilderTypeException;
use Importer\Factory\Exception\FactoryProductNotCreatedException;
use Importer\Finder\ClassMethodFinder;
use Importer\Mapper\IdentifierInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class InstanceTypeBuilderFactory
 *
 * A Factory for creating the expected Builder for certain Data Type instance or reference. Provided an identifier.
 * @package Factory
 */
class InstanceTypeBuilderFactory implements FactoryInterface
{

    protected static function createBuilder($type)
    {
        $builder = null;

        switch ($type) {

            case 'SoftReference':
            case 'HardReference':
            case 'QueryOrBuild':
            case 'Build':
                $builder = new ReferenceBuilder();
                $builder->setType($type);
                break;

            case 'Service':
            case 'Query':
            case 'QueryOrIgnore':
            case 'ServiceOrIgnore':
                $builder = new ServiceBuilder();
                $builder->setType($type);
                break;

            default:
                throw new FactoryInvalidBuilderTypeException('The provided type %type% has not an associated Builder.',
                    E_WARNING, null, $type, ['%type%' => $type]);
        }

        return $builder;
    }

    /**
     * @param $params
     * @return ReferenceBuilder|ServiceBuilder|null
     * @throws FactoryProductNotCreatedException
     */
    public static function create($params)
    {
        try {


            $type = $params['id']->getAttribute('builder');

            $builder = self::createBuilder($type);

            if ($params['id']->hasAttribute('column_builder')) {

                $type = $params['id']->getAttribute('column_builder')->getAttribute('builder');
                $columnBuilder = self::createBuilder($type);
                $columnFinder = $params['id']->getAttribute('column_builder')->getAttribute('finder');
                $columnBuilder->setFinder(new ClassMethodFinder($columnFinder[0], $columnFinder[1], false,
                    $params['container']));

                $columnBuilder->setContainer($params['container']);
                $columnBuilder->setDataAccessor($params['accessor']);
                $builder->setColumnBuilder($columnBuilder);
            }
            $finder = $params['id']->getAttribute('finder');
            $builder->setFinder(new ClassMethodFinder($finder[0], $finder[1], false, $params['container']));
            $builder->setContainer($params['container']);
            $builder->setDataAccessor($params['accessor']);

            return $builder;
        } catch (Exception $e) {

            throw new FactoryProductNotCreatedException('The Builder could not be created.', $e->getCode(), $e);
        }
    }
}