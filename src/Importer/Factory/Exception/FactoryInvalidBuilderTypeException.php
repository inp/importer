<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 06:42 PM
 *
 */

namespace Importer\Factory\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class FactoryInvalidBuilderTypeException
 *
 * Exception when no Builder can be created for certain type.
 *
 * @package Importer\Factory\Exception
 */
class FactoryInvalidBuilderTypeException extends TranslatedImporterException
{

}