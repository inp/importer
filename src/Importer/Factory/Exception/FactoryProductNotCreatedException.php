<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 06:50 PM
 *
 */

namespace Importer\Factory\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class FactoryProductNotCreatedException
 *
 * Exception in case a Factory Product could not be created.
 *
 * @package Importer\Factory\Exception
 */
class FactoryProductNotCreatedException extends TranslatedImporterException
{

}