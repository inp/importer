<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 2/03/16
 * Time: 09:59 AM
 */

namespace Importer\Event;

/**
 * Class ImporterEvent
 *
 * Basic importer event definition
 *
 * @package Importer\Event
 */
class ImporterEvent
{

    /**
     * @var object Object related to this event
     */
    protected $subject;

    /**
     * @var string Event type
     */
    protected $type;

    /**
     * @var array Associative array with extra information about an event
     */
    protected $params;

    public function __construct($subject = null, $type, $params = null)
    {
        $this->subject = $subject;
        $this->type = $type;
        $this->params = $params;
    }

    /**
     * @return object
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param object $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }

}