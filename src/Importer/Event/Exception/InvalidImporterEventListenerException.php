<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/03/16
 * Time: 10:33 AM
 */

namespace Importer\Event\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class InvalidImporterEventListenerException
 *
 * Exception for listener related exceptions.
 *
 * @package Importer\Event\Exception
 */
class InvalidImporterEventListenerException extends TranslatedImporterException
{

}