<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 2/03/16
 * Time: 10:21 AM
 */

namespace Importer\Event\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class UnknownEventTypeException
 *
 * Exception to be launched in case of an unknown method inside a notifier
 *
 * @package Importer\Event\Exception
 */
class UnknownEventTypeException extends TranslatedImporterException
{

}