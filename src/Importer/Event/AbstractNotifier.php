<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 02/03/16
 * Time: 09:46 AM
 *
 */

namespace Importer\Event;

use ICanBoogie\Inflector;
use Importer\Event\Exception\InvalidImporterEventListenerException;
use Importer\Event\Exception\UnknownEventTypeException;

/**
 * Class AbstractNotifier
 *
 * Abstract class for basic Observer register and event notification
 *
 * @package Importer\Event
 */
abstract class AbstractNotifier
{

    /**
     * @var mixed A collection of listeners grouped by an event type, not forced
     * to implement an interface as long as they define the method for handling
     * it
     */
    protected $listeners = [];

    /**
     * Method for adding a listener to certain event type, this may include the
     * desired method.
     *
     * @param string $type
     * @param mixed $listener
     * @param string|null $handler
     */
    public function addListener($type, $listener, $handler = null)
    {

        if (!array_key_exists($type, $this->listeners)) {

            $this->listeners[$type] = [];
        }

        $this->listeners[$type][] = [
            'listener' => $listener,
            'handler' => $handler
        ];
    }

    /**
     * This method sends an ImporterEvent to each of the listeners, those
     * listening to the event type will handle the event according to their
     * interface.
     *
     * @param ImporterEvent $event An event object
     */
    public function notifyEvent(ImporterEvent $event)
    {

        $type = $event->getType();

        if (!array_key_exists($type, $this->listeners)) {
            return;
        }

        $listeners = $this->listeners[$type];

        foreach ($listeners as $idx => $listener) {

            $obj = $listener['listener'];
            $handler = empty($listener['handler']) ? ('on' . Inflector::get()->camelize($type)) : $listener['handler'];

            if (!((is_object($obj)) && method_exists($obj, $handler))) {

                throw new InvalidImporterEventListenerException('"%type%" has an invalid or unimplemented listener (%idx%)',
                    0, null, null, ['%type%' => $type, '%idx%' => $idx]);
            }

            // The method signature must handle two parameters, the event subject
            // and the extra parameter array
            call_user_func_array([$obj, $handler], [$event->getSubject(), $event->getParams()]);
        }
    }
}