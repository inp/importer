<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 15/04/13
 * Time: 01:04 PM
 *
 */

namespace Importer\Builder;

use Exception;
use Importer\Builder\Exception\BuilderProductPartCouldNotBeBuiltException;
use Importer\Builder\Exception\ObjectNotBuiltException;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class GenericBuilder
 *
 * A "generic" Builder, based on the PropertyAccess Bundle, able to build objects from an specific class, and building
 * it's attributes.The product class is required to implement the adequate getters and setters for the attributes to be
 * built.
 * @package Builder
 */
class GenericBuilder implements BuilderInterface
{
    /**
     * @var mixed "Generic" product to be created
     */
    protected $product;

    /**
     * @var string The product class.
     */
    protected $class;

    /**
     * @var PropertyAccessor The property accesor for getting/setting attributes.
     */
    protected $accesor;

    public function __construct($class = null)
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
        $this->class = $class;
    }

    /**
     * Creates a new instance of the product to be built. It also can be used to "build over" a partially built object.
     * @param mixed $partial A partially built object (optional). null to create a completely new product instance.
     * @throws \Exception
     */
    public function create($partial = null)
    {
        if (isset($partial)) {

            if (is_object($partial)) {

                $this->class = get_class($partial);
                $this->product = $partial;
            } else {

                throw new ObjectNotBuiltException('Invalid product type.', E_WARNING, null);
            }

        } else {

            $this->product = $this->getNewProduct();
        }
    }

    /**
     * Method for building the product's pieces.
     * @param $attr string The attribute to be built into the product (Property Access notation).
     * @param $val mixed The value of the attribute.
     * @return $this reference to this Builder (fluent interface).
     */
    public function buildPart($attr, $val)
    {
        try {

            $this->accessor->setValue($this->product, $attr, $val);
            return $this;
        } catch (Exception $e) {
            throw new BuilderProductPartCouldNotBeBuiltException('Error trying to build the part : %part% => %value%.', $e->getCode(
            ), $e, null, ['%part%' => $attr, '%value%' => $val]);
        }
    }

    /**
     * Gets the "finished" product.
     * @return mixed The product being built.
     */
    public function get()
    {
        return $this->product;
    }

    /**
     * Method for testing if certain attribute of the product has been built (it's not null).
     * @param $attr string The attribute to be tested (pProperty Access Notation).
     * @return bool True if the attribute is set.
     */
    public function isBuilt($attr)
    {
        $val = $this->accessor->getValue($this->product, $attr);
        return isset($val);
    }

    /**
     * @return string The product class name.
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param $class string The class name.
     */
    public function setClass($class)
    {
        $this->class = $class;
    }

    /**
     * Returns a new instance of the product class.
     * @return mixed The new Product
     */
    protected function getNewProduct()
    {
        return new $this->class();
    }
}