<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 17/06/13
 * Time: 09:58 AM
 *
 */

namespace Importer\Builder\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class ObjectNotBuiltException
 *
 * Exception related to errors trying to create an object, containing information about the error.
 *
 * @package Importer\Builder\Exception
 */
class ObjectNotBuiltException extends TranslatedImporterException
{

    public function __toString()
    {
        return 'The object could not be built ' . ' Code : ' . $this->getCode() . 'Detail : ' . $this->getMessage(
        ) . ' ' . $this->getParams();
    }

}