<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 06:26 PM
 *
 */

namespace Importer\Builder\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class BuilderUndefinedFinderParameterException
 *
 * Exception when a parameter for a Finder object can't be parsed
 *
 * @package Importer\Builder\Exception
 */
class BuilderUndefinedFinderParameterException extends TranslatedImporterException
{

}