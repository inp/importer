<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 16/07/13
 * Time: 11:39 AM
 *
 */

namespace Importer\Builder\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class BuilderObjectDataCouldNotBeAssignedException
 *
 * Exception for data that cannot be assigned to a built object.
 *
 * @package Importer\Builder\Exception
 */
class BuilderObjectDataCouldNotBeAssignedException extends TranslatedImporterException
{

}