<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 16/07/13
 * Time: 10:59 AM
 *
 */

namespace Importer\Builder\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class BuilderServiceContainerException
 *
 * Exception for errors while trying to register a service within a builder.
 *
 * @package Importer\Builder\Exception
 */
class BuilderServiceContainerException extends TranslatedImporterException
{

}