<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 17/06/13
 * Time: 10:49 AM
 *
 */

namespace Importer\Builder\Exception;

/**
 * Class ModelConstructionDirectorObjectNotBuiltException
 *
 * An ObjectNotBuiltException with additional information about the "unbuilt" object:
 * Wrong Id,
 *
 * @package Importer\Builder\Exception
 */
class ModelConstructionDirectorObjectNotBuiltException extends ObjectNotBuiltException
{

    /**
     * @var array Candidate Identifiers for an object
     */
    protected $candidateIdentifiers;

    /**
     * @var mixed Data for the object.
     */
    protected $data;

    /**
     * @var string A name/title for the object to be created.
     */
    protected $title;

    /**
     * Method for setting the candidate identifiers.
     * @param $candidateIdentifiers array The list of candidate identifiers.
     */
    public function setCandidateIdentifiers($candidateIdentifiers)
    {
        $this->candidateIdentifiers = $candidateIdentifiers;
    }

    /**
     * @return array The array of Identifier candidates.
     */
    public function getCandidateIdentifiers()
    {
        return $this->candidateIdentifiers;
    }

    /**
     * Method for setting the data proposed fo the new object.
     * @param $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed The data.
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string The assigned title.
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function __toString()
    {
        return 'The Model Construction Object could not be created : ' . $this->getMessage() . ' Code: ' .
            $this->getCode() . '[' . implode(' ', $this->data) . ']';
    }
}