<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 16/07/13
 * Time: 12:10 PM
 *
 */

namespace Importer\Builder\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class BuilderCreationException
 *
 * Exception for errors when creating a Builder.
 *
 * @package Importer\Builder\Exception
 */
class BuilderCreationException extends TranslatedImporterException
{

}