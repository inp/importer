<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 16/07/13
 * Time: 11:52 AM
 *
 */

namespace Importer\Builder\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class BuilderProductPartCouldNotBeBuiltException
 *
 * Exception when a product part cannot be built.
 *
 * @package Importer\Builder\Exception
 */
class BuilderProductPartCouldNotBeBuiltException extends TranslatedImporterException
{

}