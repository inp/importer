<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 16/04/13
 * Time: 09:23 AM
 *
 */

namespace Importer\Builder;

/**
 * Class DirectorInterface
 *
 * Common Interface for Directors using Builders.
 * @package Builder
 */
interface DirectorInterface
{
    /**
     * @param mixed $params Set of (optional) parameters used to construct the object.
     * @return mixed The resulting product.
     */
    public function construct($params = null);

    public function finishObject();

    public function startNewObject();
}