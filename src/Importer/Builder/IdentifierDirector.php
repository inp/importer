<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 25/04/13
 * Time: 10:40 AM
 *
 */

namespace Importer\Builder;

use Exception;
use Importer\Builder\Exception\ObjectNotBuiltException;

/**
 * Class IdentifierDirector
 *
 * A Director for Building the Identifier object.
 * @package Builder
 */
class IdentifierDirector implements DirectorInterface
{
    protected $attributeToMethod = array(

        'mapping' => 'buildMapping',
        'format' => 'buildFormatter',
        'identifier' => 'buildIdentifier',
        'parameter' => 'buildParameter',
        'type' => 'buildBuilder',
        'finder' => 'buildFinder',
        'dataType' => 'buildDataType',
        'columnBuilder' => 'buildColumnBuilder',
        'finder_params_required' => 'buildFinderParamsRequired',
        'skip_if_empty_value' => 'buildSkipIfEmptyValue',
        'skip' => 'buildSkip',
        'originalHeader' => 'buildOriginalHeader'
    );

    public function __construct()
    {
        $this->builder = new IdentifierBuilder();
    }

    /**
     * @param mixed $params Set of (optional) parameters used to construct the object.
     * @return mixed The resulting product.
     * @throws ObjectNotBuiltException
     */
    public function construct($params = null)
    {

        $id = null;
        try {
            $id = key($params);
            $this->builder->create($id);

            // The $attributeToMethod map helps associating an attribute to a builder method.
            foreach ($params[$id] as $param => $value) {

                if (array_key_exists($param, $this->attributeToMethod)) {

                    $build = $this->attributeToMethod[$param];
                    $this->builder->$build($value); // The space (' ')between -> and $build IS necessary
                }
            }

            return $this->builder->get();
        } catch (Exception $e) {

            throw new ObjectNotBuiltException('Error while trying to build the identifier %id%.', E_WARNING, $e, $id, array('%id%' => $id));
        }

    }

    public function finishObject()
    {
        // TODO: Implement finishObject() method.
    }

    public function startNewObject()
    {
        // TODO: Implement startNewObject() method.
    }
}