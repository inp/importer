<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 25/04/13
 * Time: 02:52 PM
 *
 */

namespace Importer\Builder;

use Exception;
use Importer\Builder\Exception\ObjectNotBuiltException;
use Importer\Finder\FinderInterface;
use Importer\Mapper\IdentifierInterface;

/**
 * Class ServiceBuilder
 *
 * A Builder for constructing a Service Object trying to use a Finder.
 * @package Builder
 */
class ServiceBuilder extends IdentifierBasedModelBuilder
{

    const SERVICE = 'Service',
        QUERY = 'Query',
        SERVICE_OR_IGNORE = 'ServiceOrIgnore',
        QUERY_OR_IGNORE = 'QueryOrIgnore';

    /**
     * @var FinderInterface The Finder object used to retrieve the instance
     */
    protected $finder;

    /**
     * @var array
     */
    protected $finderParams;

    /**
     * @var string Reference type Service|Query|ServiceOrIgnore|QueryOrIgnore
     */
    protected $type;

    /**
     * Creates a new instance of the product to be built.
     * @params array $param The parameter list for the finder.
     * @param IdentifierInterface $id
     * @return mixed
     * @throws ObjectNotBuiltException
     */
    public function create($id = null)
    {
        $this->id = $id;
        $foundObjects = $this->finder->findElements($this->finderParams);

        if (!empty($foundObjects)) {

            $this->service = $foundObjects[0];
        } else {

            // Any "ignore" type returns the "empty" value
            if ($this->type == self::QUERY_OR_IGNORE || $this->type == self::SERVICE_OR_IGNORE) {

                $this->service = $foundObjects;
                return;
            }

            $values = [];
            foreach ($this->finderParams as $param) {

                if (is_object($param))
                    $values[] = method_exists($param, '__toString') ? '' . $param : get_class($param);
                elseif (is_scalar($param))
                    $values[] = $param;
                else
                    $values[] = gettype($param);
            }

            $placeholders = [
                '%identifier%' => $this->id && $this->id->hasAttribute('original_header') ? $this->id->getAttribute('original_header') : 'service',
                '%mapping%' => $this->id && $this->id->hasAttribute('mapping') ? $this->id->getAttribute('mapping') : '',
                '%values%' => $this->id && $this->id->hasAttribute('outer_value') ? $this->id->getAttribute('outer_value') : join(',', $values)
            ];

            throw new ObjectNotBuiltException('The Finder could not find any %identifier% with %mapping% %values%.',
                E_ERROR, null, null, $placeholders);
        }
    }

    public function setFinder($finder)
    {
        $this->finder = $finder;
    }

    public function setFinderParams($params)
    {
        $this->finderParams = $params;
    }

    /**
     * Sets the query type.
     * @param $type string The Query type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}