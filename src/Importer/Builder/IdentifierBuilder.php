<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 25/04/13
 * Time: 10:50 AM
 *
 */

namespace Importer\Builder;

use Exception;
use Importer\Mapper\BasicIdentifier;

/**
 * Class IdentifierBuilder
 *
 * A builder  for constructing Identifier objects.
 * @package Builder
 */
class IdentifierBuilder implements BuilderInterface
{
    /**
     * @var BasicIdentifier the identifier to be built
     */
    protected $product;

    public function __construct()
    {
    }

    /**
     * Creates a new instance of the product to be built.
     * @param mixed $id Extra parameters needed for creating the new instance (if needed).
     */
    public function create($id = null)
    {
        $this->product = new BasicIdentifier($id);
    }

    /**
     * Gets the "finished" product.
     * @return mixed The product being built.
     */
    public function get()
    {
        return $this->product;
    }

    public function buildPart($part, $value)
    {
        throw new Exception('Unimplemented method.');
    }

    public function buildFormatter($formatterName)
    {
        $this->product->setAttribute('formatter', $formatterName);
    }

    public function buildMapping($mapping)
    {
        $this->product->setAttribute('mapping', $mapping);
    }

    public function buildIdentifier($identifier)
    {
        $this->product->setAttribute('identifier', sprintf($identifier, $this->product->getIdentifier()));
    }

    public function buildParameter($parameter)
    {
        $this->product->setAttribute('parameter', $parameter);
    }

    public function buildFinder($finder)
    {
        $this->product->setAttribute('finder', $finder);
    }

    public function buildBuilder($type)
    {
        $this->product->setAttribute('builder', $type);
    }

    public function buildFinderParamsRequired($val)
    {
        $this->product->setAttribute('finder_params_required', $val);
    }

    public function buildSkipIfEmptyValue($val)
    {
        $this->product->setAttribute('skip_if_empty_value', $val);
    }

    public function buildSkip($val)
    {
        $this->product->setAttribute('skip', $val);
    }

    public function buildColumnBuilder($columnBuilder)
    {

        $columnBuilderIdentifierBuilder = new IdentifierBuilder();
        $columnBuilderIdentifierBuilder->create('column_builder');
        $columnBuilderIdentifierBuilder->buildBuilder($columnBuilder['type']);
        $columnBuilderIdentifierBuilder->buildFinder($columnBuilder['finder']);

        $columnBuilderIdentifier = $columnBuilderIdentifierBuilder->get();

        $this->product->setAttribute($columnBuilderIdentifier->getIdentifier(), $columnBuilderIdentifier);
    }

    public function buildDataType($dataType)
    {
        $this->product->setAttribute('data_type', $dataType);
    }

    public function buildOriginalHeader($origH)
    {
        $this->product->setAttribute('original_header', $origH);
    }
}