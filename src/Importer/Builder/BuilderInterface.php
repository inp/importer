<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 15/04/13
 * Time: 10:33 AM
 *
 */

namespace Importer\Builder;


/**
 * Class BuilderInterface
 *
 * Common interface for Builder classes, like those ones taking a set of data and properties and "building" certain
 * model representation for later persistence (maybe).
 * @package Builder
 */
interface BuilderInterface
{
    /**
     * Creates a new instance of the product to be built.
     * @param mixed $params Extra parameters needed for creating the new instance (if needed).
     */
    public function create($params = null);

    /**
     * Gets the "finished" product.
     * @return mixed The product being built.
     */
    public function get();

    /**
     * Method for building one part from the product.
     * @param $part mixed The part to be built.
     * @param $value mixed The data used to build that part.
     * @return mixed
     */
    public function buildPart($part, $value);
}