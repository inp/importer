<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 30/04/13
 * Time: 10:16 AM
 *
 */

namespace Importer\Builder;

use Exception;
use Importer\Builder\Exception\BuilderServiceContainerException;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class ContainerBasedBuilder
 *
 * A Builder Base class based on a container to handle Dependencies (Each subclass is responsible of implementing an
 * interface for each ContainerBasedBuilder type).
 * @package Builder
 */
abstract class ContainerBasedBuilder implements BuilderInterface
{
    /**
     * @var ContainerBuilder DI Container
     */
    protected $container;

    /**
     * @var mixed The service to be built over.
     */
    protected $service;


    /**
     * Creates a new reference (or instance, depending of the service's scope) of the product to be built.
     * @param mixed $id The Id of the service to be built.
     */
    public function create($id = null)
    {
        try {
            $this->service = $this->container->get($id);

            if (!isset($this->service)) {

                throw new Exception('The service could not be found.');
            }

        } catch (Exception $e) {

            throw new BuilderServiceContainerException('The service under id %id% could not be created.', $e->getCode(
            ), $e, $id, ['%id%' => $id]);
        }

    }

    /**
     * Gets the service instance.
     * @return mixed The service being built.
     */
    public function get()
    {
        return $this->service;
    }

    /**
     * Sets the current DI container.
     * @param $container ContainerBuilder
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

}