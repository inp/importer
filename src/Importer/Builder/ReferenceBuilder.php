<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 30/04/13
 * Time: 10:52 AM
 *
 */

namespace Importer\Builder;

use Exception;
use Importer\Builder\Exception\ObjectNotBuiltException;
use Importer\Finder\FinderInterface;

/**
 * Class ReferenceBuilder
 *
 * A Builder for constructing objects retrieved from other source (class with
 * certain method). Or using the DI container.
 * @package Builder
 */
class ReferenceBuilder extends IdentifierBasedModelBuilder
{

    const SOFT_REFERENCE = 'SoftReference';
    const QUERY_OR_BUILD = 'QueryOrBuild';
    const HARD_REFERENCE = 'HardReference';
    const BUILD = 'Build';

    /**
     * @var FinderInterface The Finder object used to retrieve the instance
     */
    protected $finder;

    /**
     * @var string Reference type SoftReference|HardReference
     */
    protected $type;

    /**
     * @var array A list of parameters for the Finder.
     */
    protected $finderParams;

    /**
     * Sets the reference for certain object found based on the parameters sent
     * to the Finder object. If the type is a SoftReference, it will try to get
     * a new product from the DI container in case it can't find the required
     * service. In case of a HardReference it will ALWAYS try to grab it from
     * the Container without using the Finder. It will assume there is only ONE
     * returned instance.
     *
     * @param null $id
     * @throws ObjectNotBuiltException
     */
    public function create($id = null)
    {
        try {
            $this->id = $id;
            if ($this->type === ReferenceBuilder::SOFT_REFERENCE ||
                $this->type === ReferenceBuilder::QUERY_OR_BUILD
            ) {

                $foundObjects = $this->finder->findElements($this->finderParams);

                if (!empty($foundObjects)) {

                    $this->service = $foundObjects[0];
                    return;
                }
            }

            parent::create($id);
        } catch (Exception $e) {

            throw new ObjectNotBuiltException('Object could not be created by Builder type %type%.', $e->getCode(), $e,
                $this->type, array('%type%' => $this->type));
        }
    }

    /**
     * Sets the reference type.
     * @param $type string The Reference type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param $finder FinderInterface a Method Finder for locating objects via a list of parameters.
     */
    public function setFinder($finder)
    {
        $this->finder = $finder;
    }

    public function setFinderParams($params)
    {
        $this->finderParams = $params;
    }
}