<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 29/04/13
 * Time: 03:42 PM
 *
 */

namespace Importer\Builder;

use Exception;
use Importer\Builder\Exception\BuilderCreationException;
use Importer\Builder\Exception\BuilderUndefinedFinderParameterException;
use Importer\Builder\Exception\ModelConstructionDirectorObjectNotBuiltException;
use Importer\Factory\InstanceTypeBuilderFactory;
use ICanBoogie\Inflector;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\PropertyAccess\PropertyAccess;

class ModelConstructionDirector implements DirectorInterface
{

    /**
     * @var ContainerBuilder
     */
    protected $container;

    public function __construct()
    {
        $this->modelTable = [];
    }

    /**
     * Method for replacing the finder parameters from the id to the expected parameters used by the Finder object.
     * @param $finderParams array The original finder parameter array acting as a placeholder.
     * @param $data mixed The data to be stored or a potential key.
     * @param $title mixed The header title, another potential key.
     * @param null $columnValue
     * @return array The new injected finder parameters ready to be used by the finder method.
     * @throws BuilderUndefinedFinderParameterException
     */
    protected function createFinderParams($finderParams, $data = null, $title = null, $columnValue = null)
    {

        $newParams =[];
        $t = $finderParams ? count($finderParams) : 0;

        try {
            for ($i = 0; $i < $t; ++$i) {

                switch ($finderParams[$i]) {

                    case '$column$':
                        // This placeholder means
                        $newParams[$i] = $columnValue;
                        break;

                    case '$title$': // This placeholder means, 'use the header column title'
                        $newParams[$i] = $title;
                        break;

                    case '$data$': // This placeholder means, 'use the provided value.'
                        $newParams[$i] = $data;
                        break;

                    default:

                        // Possibly a service...
                        if ($finderParams[$i]{0} == '@') {

                            $serviceId = substr($finderParams[$i], 1);

                            // If we find a "." on the param finder, it tries to
                            // use the property accessor. A SERVICE FINDER PARAM
                            // SHOULD NOT HAVE A "."
                            if (strpos($finderParams[$i], '.') !== false) {

                                $pieces = explode($serviceId, '.', 2);
                                $service = $this->container->get($pieces[0]);

                                $accessor = PropertyAccess::createPropertyAccessor();
                                $newParams[$i] = $accessor->getValue($service, $pieces[1]);
                            } else
                                $newParams[$i] = $this->container->get($serviceId);

                        } elseif ($finderParams[$i]{0} == '%' && $finderParams[$i]{strlen(
                                $finderParams[$i]
                            ) - 1} == '%'
                        ) {

                            // Posibly a parameter...
                            $len = strlen($finderParams[$i]);
                            $newParams[$i] = $this->container->getParameter(
                                substr($finderParams[$i], 1, $len - 2 <= 0 ? 0 : $len - 2)
                            );
                        } else {

                            // "Raw" value.
                            $newParams[$i] = $finderParams[$i];
                        }

                }
            }

            return $newParams;
        } catch (Exception $e) {

            throw new BuilderUndefinedFinderParameterException('Error while creating the Finder parameters : %list%.', $e->getCode(), $e, $finderParams, array('%list%' => implode(' ',
                $finderParams)));
        }
    }

    /**
     * Method for help creating Exceptions
     * @param $msg
     * @param int $code
     * @param null $prev
     * @param null $identifiers
     * @param null $data
     * @param null $title
     * @param null $placeHolders
     * @return ModelConstructionDirectorObjectNotBuiltException
     */
    protected function getModelConstructionDirectorException($msg,
                                                             $code = 0,
                                                             $prev = null,
                                                             $identifiers = null,
                                                             $data = null,
                                                             $title = null,
                                                             $placeHolders = null)
    {
        $currentCreationException = new ModelConstructionDirectorObjectNotBuiltException($msg, $code, $prev, null, $placeHolders);
        $currentCreationException->setCandidateIdentifiers($identifiers);
        $currentCreationException->setData($data);
        $currentCreationException->setTitle($title);

        return $currentCreationException;
    }

    /**
     * @param mixed $params Set of (optional) parameters used to construct the object.
     * @return mixed The resulting product.
     * @throws ModelConstructionDirectorObjectNotBuiltException
     */
    public function construct($params = null)
    {
        $data = $params['data'];
        $title = $params['title'];
        $builder = null;
        $currentCreationException = null;

        // It may be more than one identifier for a given field.
        foreach ($params['identifier'] as $identifier) {

            // If this attribute is true ignore this parameter set
            if ($identifier->getAttribute('skip'))
                return null;

            // If this attribute exists and the value is empty, skip the process
            // for this set of parameters
            if ($identifier->getAttribute('skip_if_empty_value')) {

                $data2 = is_string($data) ? trim($data) : $data;
                if (empty($data2))
                    return null;
            }

            try {

                $builder = $this->getBuilder($identifier, $data, $title);

                if (empty($builder))
                    return null;

            } catch (Exception $e) {

                $currentCreationException = $this->getModelConstructionDirectorException('Error while setting the Builder.',
                    $e->getCode(),
                    $e,
                    $params['identifier'],
                    $data,
                    $title);

                continue;
            }

            try {

                $this->buildProduct($builder, $identifier);

                // If there is no product return null then
                $pr = $this->getProduct($builder);
                if (empty($pr))
                    return null;

            } catch (Exception $e) {

                $currentCreationException = $this->getModelConstructionDirectorException('Error while builder was trying to create the object.',
                    $e->getCode(),
                    $e,
                    $params['identifier'],
                    $data,
                    $title);

                continue;
            }

            try {

                // If there is not a mapping don't write
                if ($identifier->hasAttribute('mapping'))
                    $this->buildData($builder, $data);

                // If this has the parameter attribute, it should be stored as a
                // parameter on the cointainer
                if ($identifier->hasAttribute('parameter')) {

                    $this->container->setParameter($identifier->getAttribute('parameter'), $this->getProduct($builder));
                    return null;
                }

                // If there is no identifier, don't update the container
                if ($identifier->hasAttribute('identifier'))
                    $this->updateContainer($builder);

                return [$identifier->getAttribute('identifier'), $this->getProduct($builder)];

            } catch (Exception $e) {

                throw $this->getModelConstructionDirectorException('The %servicio% object model could not be created : The object could not be built.',
                    $e->getCode(),
                    $e,
                    $params['identifier'],
                    $data,
                    $title,
                    ['%servicio%' => $identifier->getAttribute('identifier')]);
            }
        }

        // If no Identifier could be used to create the object, throw a new Exception with the information
        throw $this->getModelConstructionDirectorException('The %servicio% object model could not be created : No identifier could be used.',
            E_ERROR,
            $currentCreationException,
            $params['identifier'],
            $data,
            $title,
            ['%servicio%' => $title]);
    }

    /**
     * Method for creating the required Builder
     * @param $params mixed Parameter set
     * @param null $data extra data
     * @param null $title optional
     * @return ReferenceBuilder|ServiceBuilder
     * @throws BuilderCreationException
     * @throws BuilderUndefinedFinderParameterException
     * @throws Exception
     */
    protected function getBuilder($params, $data = null, $title = null)
    {

        $factoryParams = [
            'id' => $params,
            'container' => $this->container,
            'accessor' => PropertyAccess::createPropertyAccessor()
        ];

        // If this attribute is set, and it's true, the finder services must be defined in the service container.
        if ($params->hasAttribute('finder_params_required')) {

            if ($params->getAttribute('finder_params_required') == true) {

                $finderParams = $params->getAttribute('finder')[2];
                $finderParams[] = $params->getAttribute('finder')[0];

                foreach ($finderParams as $finderParam) {

                    if (strpos($finderParam, '@') === 0) {

                        if (!$this->container->has(substr($finderParam, 1))) {

                            $inflector = Inflector::get();
                            throw new BuilderCreationException('A %service% is required for %id%.', 0, null, null, ['%service%' => $inflector->humanize(substr($finderParam, 1)), '%id%' => $inflector->humanize($params->getAttribute('identifier'))]);
                        }

                    }

                }
            }
        }

        $builder = InstanceTypeBuilderFactory::create($factoryParams); // Main Builder

        // First, we need to look for the object requested by the columnBuilder
        $columnBuilder = $builder->getColumnBuilder();
        $columnValue = null;
        if (isset($columnBuilder)) {

            $columnBuilderFinderParams = $this->createFinderParams(
                $params->getAttribute('column_builder')->getAttribute('finder')[2],
                $data,
                $title
            );

            $columnBuilder->setFinderParams($columnBuilderFinderParams);
            $params->setAttribute('outer_value', $data);
            $columnBuilder->create($params);
            $columnValue = $columnBuilder->get();

            // If no value from columnBuilder it does not make much sense to
            // try to use a builder
            if (empty($columnValue))
                return null;
        }

        $finderParams = $this->createFinderParams(
            $params->getAttribute('finder')[2],
            $data,
            $title,
            $columnValue
        );
        $builder->setFinderParams($finderParams);

        return $builder;
    }

    /**
     * Method for creating a product with the provided Builder.
     * @param $builder BuilderInterface
     * @param null $identifier identifier with the parameters to build the product
     */
    protected function buildProduct($builder, $identifier = null)
    {
        $builder->create($identifier);
    }

    /**
     * Method for building data into the product.
     * @param $builder BuilderInterface used to build data into the product.
     * @param null $data Data to be written.
     */
    protected function buildData($builder, $data = null)
    {

        $builder->buildData($data);
    }

    /**
     * Method for updating the container related to the Builder with the product definition.
     * @param $builder BuilderInterface
     */
    protected function updateContainer($builder, $data = null)
    {

        $builder->buildToContainer(null);
    }

    /**
     * Method for returning the finished product
     * @param $builder BuilderInterface
     * @return mixed
     */
    protected function getProduct($builder, $data = null)
    {

        return $builder->get();
    }

    /**
     * @param Container $container
     */
    public function setContainer(Container $container)
    {
        $this->container = $container;
    }

    /**
     * Assuming we are using this Director over and over again to build certain object based on a Identifier, maybe
     * @return object The service marked as expected
     */
    public function getExpectedService()
    {
        return $this->container->get($this->container->getParameter('expected_service'));
    }

    public function finishObject()
    {
        if ($this->container->hasParameter('row_services')) {

            $rowServicesIds = $this->container->getParameter('row_services');
            foreach ($rowServicesIds as $rowServiceId) {

                $definition = $this->container->getDefinition($rowServiceId);
                $definition->setSynthetic(false);
                $this->container->set($rowServiceId, null);
                $this->container->setDefinition($rowServiceId, $definition);
            }
        }
    }

    public function startNewObject()
    {
    }
}