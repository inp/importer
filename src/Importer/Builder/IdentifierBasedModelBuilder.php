<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 3/05/13
 * Time: 09:24 AM
 *
 */

namespace Importer\Builder;

use Exception;
use Importer\Builder\Exception\BuilderObjectDataCouldNotBeAssignedException;
use Importer\Builder\Exception\BuilderServiceContainerException;
use Importer\Mapper\IdentifierInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\PropertyAccess\PropertyAccessor;

/**
 * Class IdentifierBasedModelBuilder
 *
 * A class for Building model objects based on a Container, using the information from a identifier.
 * @package Builder
 */
class IdentifierBasedModelBuilder extends ContainerBasedBuilder
{
    /**
     * @var PropertyAccessor A delegate accessor object (PropertyAccess) for assigning a model's attribute.
     */
    protected $dataAccessor;

    /**
     * @var IdentifierInterface The identifier object
     */
    protected $id;

    /**
     * @var BuilderInterface The column Builder
     */

    protected $columnBuilder;

    /**
     * A setter for the delegate data accessor.
     * @param $db mixed The delegate data accesor.
     */
    public function setDataAccessor($da)
    {
        $this->dataAccessor = $da;
    }

    /**
     * A setter for the column Builder
     * @param $cb BuilderInterface The columnBuilder
     */
    public function setColumnBuilder($cb)
    {
        $this->columnBuilder = $cb;
    }

    public function buildPart($part, $value)
    {
        throw new Exception('Unimplemented method.');
    }

    /**
     *
     * @param IdentifierInterface $id The identifier object
     */
    public function create($id = null)
    {

        $this->id = $id;
        parent::create($this->id->getAttribute('identifier'));
    }

    /**
     * Adds the Built service to the container. WARNING: It will always store the under the 'Container Scope', so the
     * same reference will be shared each time this service si requested. For new instances you could "clone" the object.
     * @param $id string The service id (optional, if not provided, it will use the current $id's idntifier attribute)
     */
    public function buildToContainer($id = null)
    {
        try {

            if (!isset($id))
                $id = $this->id->getAttribute('identifier');

            $definition = $this->container->getDefinition($id);
            $definition->setSynthetic(true);
            $this->container->set($id, $this->service);
            $this->container->setDefinition($id, $definition);

        } catch (Exception $e) {

            throw new BuilderServiceContainerException('The built service could not be registered in the service container.', $e->getCode(), $e);
        }
    }

    /**
     * An utility method for applying a formatter service to data.
     * @param $data mixed The data to be formatted
     * @return mixed The formatted data
     */
    protected function formatData($data)
    {

        $formatter = $this->container->get($this->id->getAttribute('formatter'));
        return $formatter->format($data);
    }

    /**
     * Sets the value for the provided path.
     * @param $data mixed The data to be assigned to the path.
     * @throws \Exception
     */
    public function buildData($data)
    {
        try {

            // If there is a formatter tag apply the format
            if ($this->id->hasAttribute('formatter')) {

                $data = $this->formatData($data);
            }

            $this->dataAccessor->setValue(
                $this->service,
                $this->id->getAttribute('mapping'),
                $data
            );
        } catch (Exception $e) {

            throw new BuilderObjectDataCouldNotBeAssignedException('Error setting data to the service.', $e->getCode(), $e);
        }
    }

    /**
     * @return BuilderInterface The column Builder.
     */
    public function getColumnBuilder()
    {
        return $this->columnBuilder;
    }
}