<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 11:36 AM
 *
 */

namespace Importer\Importer\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class ImporterParseException
 *
 * Exception for the Importer's parse method.
 *
 * @package Importer\Importer\Exception
 */
class ImporterParseException extends TranslatedImporterException
{

}