<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 11:30 AM
 *
 */

namespace Importer\Importer\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class ImporterProcessException
 *
 * Exception for the Importer's process method.
 *
 * @package Importer\Importer\Exception
 */
class ImporterProcessException extends TranslatedImporterException
{

}