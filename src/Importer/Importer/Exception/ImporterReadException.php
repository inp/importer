<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 15/07/13
 * Time: 11:18 AM
 *
 */

namespace Importer\Importer\Exception;

use Importer\Exception\TranslatedImporterException;

/**
 * Class ImporterReadException
 *
 * Exception for the Importer's read method.
 *
 * @package Importer\Importer\Exception
 */
class ImporterReadException extends TranslatedImporterException
{

}