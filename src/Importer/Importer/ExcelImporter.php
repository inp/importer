<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian
 * Date: 5/04/13
 * Time: 11:30 AM
 *
 */

namespace Importer\Importer;

use Exception;
use Importer\Processor\PostParseModelProcessor;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Importer\Factory\ServiceContainerFactory;
use Importer\Importer\AbstractImporter;
use Importer\Importer\Exception\ImporterParseException;
use Importer\Importer\Exception\ImporterProcessException;
use Importer\Importer\Exception\ImporterReadException;
use Importer\Loader\LoaderInterface;
use PHPExcel_Worksheet;
use Importer\Parser\ParserInterface;
use Importer\Processor\ProcessorInterface;
use Importer\Util\ExcelImporterUtil;

/**
 * Class ExcelImporter
 *
 * An Importer for handling Excel files based on PHPExcel (http://www.codeplex.com/PHPExcel).
 *
 * @package Importer
 */
class ExcelImporter extends AbstractImporter
{
    protected $sheetErrors = null;
    protected $phpExcelObj = null;
    protected $sheets = null;
    /** @var PostParseModelProcessor|null */
    protected $postParseProcessor = null;

    /**
     * @param LoaderInterface $loader
     * @param ProcessorInterface $processor
     * @param ParserInterface $parser
     * @param PostParseModelProcessor $postProcessor
     */
    function __construct(
        LoaderInterface $loader,
        ProcessorInterface $processor,
        ParserInterface $parser,
        $postProcessor = null
    )
    {

        $this->loader = $loader;
        $this->processor = $processor;
        $this->parser = $parser;
        $this->postParseProcessor = $postProcessor;
    }

    /**
     * Reads the requested file content and stores it into a resource object.
     * @param string $filename Name of the file to be imported, for more information about valid file types please
     * consult PHPExcel documentation.
     * @throws ImporterReadException
     * @return mixed|void
     */
    public function read($filename)
    {
        try {

            parent::read($filename);
            $this->phpExcelObj = $this->loader->getResource();

        } catch (\Exception $e) {

            throw new ImporterReadException('Error loading the file.', $e->getCode(), $e);
        }
    }

    /**
     * This method process the information contained in the spreadsheets. Later it tries to find a set of identifiers
     * based on this Importer's criteria. And finally, it stores the "clean" sheet reference, the new data range and the
     * field names under the keys 'sheet', 'range' and 'fields' respectively.
     */
    public function process()
    {
        // Now attempts to process every sheet, finding its initial dataRange, removing "blanks", finding the field
        // names and registering a set with the previous data.
        try {
            $this->sheets = [];
            $sheetIter = $this->getPHPExcelObject()->getWorkSheetIterator();

            foreach ($sheetIter as $sheet) {

                $this->sheets[] = $this->processor->process($sheet);
            }

        } catch (Exception $e) {

            throw new ImporterProcessException('Error while trying to process the document.', $e->getCode(), $e);
        }

    }

    /**
     * This method traverses the set of processed sheets to map its identifiers to properties, read the information
     * from each row, apply a desired format to the retrieved data and prepare the data set to be saved.
     */
    public function parse()
    {
        try {

            // The resulting Model will be a three dimensional array m(i, j, k) where i is the worksheet index, j is the
            // row index and k is the column index.
            $this->model = [];
            $this->errors = [];
            foreach ($this->sheets as $sheet) {

                $model = $this->parser->parse($sheet);
                if ($model)
                    $this->model[] = $model;

                // Pushing the error lines container for this sheet.
                $errors = $this->parser->getErrors();
                if (!empty($errors))
                    $this->errors[] = $errors;
            }
        } catch (Exception $e) {

            throw new ImporterParseException('Error while trying to parse the sheet data.', $e->getCode(), $e);
        }

        if ($this->postParseProcessor != null) {
            if (empty($errors) || $this->postParseProcessor->isProcessWithErrors())
                // Execute post-parse method only if there aren't any errors.
                $this->postParse();
        }
    }

    /**
     * Function to be executed after every worksheet has been parsed and no
     * errors where caught.
     */
    protected function postParse()
    {

        $accessor = PropertyAccess::createPropertyAccessor();
        $parts = explode('.', $this->postParseProcessor->getBucketIdPath());
        $groupPath = "[{$parts[0]}]." . str_replace($parts[0] . '.', '', $this->postParseProcessor->getBucketIdPath());
        $parts = explode('.', $this->postParseProcessor->getContentIdPath());
        $contentPath = "[{$parts[0]}]." . str_replace($parts[0] . '.', '', $this->postParseProcessor->getContentIdPath());

        // Made sheet by sheet
        foreach ($this->model as $sheetModel) {

            $bucket = [];
            foreach ($sheetModel as $rowModel) {

                try {

                    // Try to find for each row array the path of the grouping value and the
                    // content value
                    $groupVal = $accessor->getValue($rowModel, $groupPath);
                    if ($groupVal == null) {

                        continue;
                    }

                    if (!array_key_exists($groupVal, $bucket)) {

                        $bucket[$groupVal] = [];
                    }

                    $contentVal = $accessor->getValue($rowModel, $contentPath);
                    if ($contentVal == null) {

                        continue;
                    }

                    $bucket[$groupVal][] = $contentVal;
                } catch (Exception $e) {

                    throw new ImporterPostParseException('Error while executing the post parse method.', $e->getCode(), $e);
                }
            }

            // For each sheet execute the Post Processor with the grouped values
            $this->postParseProcessor->process($bucket);
        }
    }

    /**
     * Returns the PHPExcel instance used to handle the Document's information.
     * @return mixed
     */
    public function getPHPExcelObject()
    {
        return $this->phpExcelObj;
    }
}