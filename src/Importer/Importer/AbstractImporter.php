<?php
/**
 * INP All rights reserved.
 * User: Juan Sebastian Robles Jimenez
 * Date: 5/04/13
 * Time: 11:16 AM
 *
 */

namespace Importer\Importer;

use Importer\Loader\LoaderInterface;
use Importer\Parser\ParserInterface;
use Importer\Processor\ProcessorInterface;

/**
 * Class AbstractImporter
 *
 * Common Importer Interface
 * @package Importer
 *
 */
abstract class AbstractImporter
{
    /**
     * @var LoaderInterface The resource Loader
     */
    protected $loader;

    /**
     * @var ProcessorInterface The resource Processor
     */
    protected $processor;

    /**
     * @var ParserInterface The resource Parser.
     */
    protected $parser;

    /**
     * @var ProcessorInterface An object to handle a created model
     */
    protected $postParseProcessor;

    protected $model;

    /**
     * @var mixed Container por possible errors.
     */
    protected $errors;

    /**
     * @param string $filename Name of the file to import.
     * @return mixed
     */
    public function read($filename)
    {

        $this->loader->load($filename);
    }

    abstract public function process();

    abstract public function parse();

    public function getModel()
    {
        return $this->model;
    }

    public function getErrors()
    {
        return $this->errors;
    }
}