<?php
/**
 * INP All rights reserved.
 * Date: 25/06/20
 * Time: 01:26 PM
 *
 */

namespace Importer\Extension;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

/**
 * Class RowScopeExtension
 *
 * This extension, searches for the list 'row_scope_services' in every
 * configuration passed to load() (when the container is compiled) and
 * "deletes" each service referred by id on the list. The compiler pass
 * should re-create the service.
 *
 * It is assumed that these services are "container scoped".
 *
 * @package Importer\Extension
 */
class RowScopeExtension implements ExtensionInterface
{

    public function load(array $configs, ContainerBuilder $container)
    {

        foreach ($configs as $rowServices) {

            foreach ($rowServices as $rowService) {

                // This should make the container create the marked service
                // "from start"
                $container->set($rowService, null);
            }
        }
    }

    public function getNamespace()
    {
        return 'http://www.inp.co/rowscopeservices/schema/';
    }

    public function getXsdValidationBasePath()
    {
        return false;
    }

    public function getAlias()
    {
        return 'row_scope_services';
    }
}